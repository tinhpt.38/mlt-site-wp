<?php
/*
Plugin Name: Sitka Core
Plugin URI: http://solopine.com
Description: Sitka Core Plugin
Author: Solo Pine
Version: 1.2.1
Author URI: http://solopine.com
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function sitka_core_fields_init() {
	// load language files
	load_plugin_textdomain( 'sitka-core', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
}
add_action( 'init', 'sitka_core_fields_init' );

// TGM
include( dirname(__FILE__) . '/sitka-core-tgm.php');

// Customizer
include( dirname(__FILE__) . '/admin/sitka_core_customizer_setup.php');

// Social Share
include( dirname(__FILE__) . '/social/social-share.php');

// Social Icons
include( dirname(__FILE__) . '/social/social-icons.php');

// Author Social Icons
include( dirname(__FILE__) . '/social/author-social-icons.php');

// Widgets
include( dirname(__FILE__) . '/widgets/social_widget.php');
include( dirname(__FILE__) . '/widgets/promo_widget.php');
include( dirname(__FILE__) . '/widgets/about_widget.php');
include( dirname(__FILE__) . '/widgets/post_widget.php');
include( dirname(__FILE__) . '/widgets/facebook_widget.php');