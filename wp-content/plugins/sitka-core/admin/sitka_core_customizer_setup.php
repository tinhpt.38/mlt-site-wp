<?php

// Check if Kirki is installed
if ( class_exists( 'Kirki' ) ) {

//////////////////////////////////////////////////////////////////
// Customizer - Kirki Configuration
//////////////////////////////////////////////////////////////////
Kirki::add_config( 'sitka_core_config', array(
	'capability'    => 'edit_theme_options',
	'option_type'   => 'theme_mod',
) );

//////////////////////////////////////////////////////////////////
// Customizer - Add Settings
//////////////////////////////////////////////////////////////////
if ( class_exists( 'Kirki' ) ) {
 	
	// Include all settings
	include( plugin_dir_path( __FILE__ ) . '/customizer_core_social.php' );
	
	
}

}