<?php
// Add Section
Kirki::add_section( 'sitka_core_section_social_share', array(
    'title'          => esc_html__( 'Social Share Buttons', 'sitka-core' ),
    'priority'       => 100,
	'panel'			 => 'sitka_core_panel_social'
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_core_social_share_layout',
	'label'       => esc_html__( 'Social Share Buttons Layout', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_share',
	'default'     => 'style1',
	'priority'    => 2,
	'choices'     => array(
		'style1'   => esc_html__( 'Style 1', 'sitka-core' ),
		'style2'   => esc_html__( 'Style 2', 'sitka-core' ),
		'style3'   => esc_html__( 'Style 3', 'sitka-core' ),
	),
) );

Kirki::add_field( 'sitka_core_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_core_social_share_style1_color',
    'label'       => esc_html__( 'Social Share Buttons Colors', 'sitka-core' ),
    'section'     => 'sitka_core_section_social_share',
    'priority'    => 5,
    'choices'     => array(
        'icon'    => esc_html__( 'Icon Color', 'sitka-core' ),
        'bg'   => esc_html__( 'Background', 'sitka-core' ),
		'border'   => esc_html__( 'Border', 'sitka-core' ),
		'icon_hover'    => esc_html__( 'Icon Color Hover', 'sitka-core' ),
        'bg_hover'   => esc_html__( 'Background Hover', 'sitka-core' ),
		'border_hover'   => esc_html__( 'Border Hover', 'sitka-core' ),
    ),
    'default'     => array(
        'icon'    => '#f78a74',
        'bg'   => '#ffffff',
		'border'    => '#f78a74',
        'icon_hover'   => '#ffffff',
		'bg_hover'    => '#f78a74',
        'border_hover'   => '#f6836c',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'icon',
		  'element'       => '.share-style1 .share-button',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'bg',
		  'element'       => '.share-style1 .share-button',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'border',
		  'element'       => '.share-style1 .share-button',
		  'property'      => 'border-color',
		),
		array(
		  'choice'		  => 'icon_hover',
		  'element'       => '.share-style1 .share-button:hover',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'bg_hover',
		  'element'       => '.share-style1 .share-button:hover',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'border_hover',
		  'element'       => '.share-style1 .share-button:hover',
		  'property'      => 'border-color',
		),
	),
	'required' => array(
        array(
            'setting'  => 'sitka_core_social_share_layout',
            'value'    => 'style1',
            'operator' => '=='
        )
    ),
	'transport' => 'auto'
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_share_twitter_via',
	'label'    => esc_html__( 'Add "Via" to Twitter Share', 'sitka-core' ),
	'description'  => esc_html__( 'Enter your Twitter username', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_share',
	'priority' => 28,
	'default'     => '',
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_share_facebook',
	'label'       => esc_html__( 'Display Facebook Share Button', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_share',
	'default'     => '1',
	'priority'    => 30,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_share_twitter',
	'label'       => esc_html__( 'Display Twitter Share Button', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_share',
	'default'     => '1',
	'priority'    => 35,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_share_pinterest',
	'label'       => esc_html__( 'Display Pinterest Share Button', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_share',
	'default'     => '1',
	'priority'    => 40,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_share_linkedin',
	'label'       => esc_html__( 'Display LinkedIn Share Button', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_share',
	'default'     => '1',
	'priority'    => 43,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_share_email',
	'label'       => esc_html__( 'Display Email Share Button', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_share',
	'default'     => '1',
	'priority'    => 45,
) );