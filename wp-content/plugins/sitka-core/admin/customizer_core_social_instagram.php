<?php
// Add Section
Kirki::add_section( 'sitka_core_section_social_instagram', array(
    'title'          => esc_html__( 'Instagram Sections', 'sitka-core' ),
    'priority'       => 102,
	'panel'			 => 'sitka_core_panel_social',
) );

Kirki::add_field( 'sitka_core_config', array(
  'settings' => 'sitka_core_footer_insta_describ',
  'section'  => 'sitka_core_section_social_instagram',
  'type'     => 'custom',
  'priority'    => 1,
  'default'    => wp_kses( __( 'Please ensure you have installed/activated the "Smash Balloon Instagram Feed" plugin and connected your Instagram account via WP Dashboard > Instagram Feed. Check out the <a target="_blank" href="https://sitkatheme.com/documentation/#social_instagram">documentation</a> for configuration instructions.', 'sitka-core' ), array(
    'a' => array(
      'target' => array(),
      'href' => array(),
    ),
  ) ),
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_footer_insta_enable',
	'label'       => esc_html__( 'Show Footer Instagram Section?', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_instagram',
	'default'     => '0',
	'priority'    => 2,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_core_footer_insta_number',
	'label'       => esc_html__( 'Number of images', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_instagram',
	'default'     => 8,
	'choices'     => array(
		'min'  => 5,
		'max'  => 10,
		'step' => 1,
	),
	'priority'    => 3,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_core_footer_insta_enable',
            'value'    => '1',
            'operator' => '=='
        )
    )
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_footer_insta_subtitle',
	'label'    => esc_html__( 'Instagram Footer Subtitle', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_instagram',
	'priority' => 12,
	'default'     => esc_html__( 'Find me on Instagram', 'sitka-core' ),
	'active_callback' => array(
        array(
            'setting'  => 'sitka_core_footer_insta_enable',
            'value'    => '1',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_footer_insta_title',
	'label'    => esc_html__( 'Instagram Footer Title', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_instagram',
	'priority' => 14,
	'default'     => esc_html__( 'Solo Pine', 'sitka-core' ),
	'active_callback' => array(
        array(
            'setting'  => 'sitka_core_footer_insta_enable',
            'value'    => '1',
            'operator' => '=='
        )
    )
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_header_insta_enable',
	'label'       => esc_html__( 'Show Header Instagram Section?', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_instagram',
	'default'     => '0',
	'priority'    => 26,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_core_header_insta_number',
	'label'       => esc_html__( 'Number of images', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_instagram',
	'default'     => 10,
	'choices'     => array(
		'min'  => 5,
		'max'  => 10,
		'step' => 1,
	),
	'priority'    => 29,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_core_header_insta_enable',
            'value'    => '1',
            'operator' => '=='
        )
    )
) );