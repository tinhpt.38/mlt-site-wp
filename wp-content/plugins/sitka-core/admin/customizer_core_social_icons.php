<?php
// Add Section
Kirki::add_section( 'sitka_core_section_social_icons', array(
    'title'          => esc_html__( 'Social Profile Icons', 'sitka-core' ),
    'priority'       => 102,
	'panel'			 => 'sitka_core_panel_social',
	'description'    => esc_html__( 'To add social icons that link to your social media accounts, enter the full URL (including https://) to your related profile pages.', 'sitka-core' ),
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_facebook',
	'label'    => esc_html__( 'Facebook URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 10,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_facebook_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 15,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_facebook_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 15,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_twitter',
	'label'    => esc_html__( 'Twitter URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 20,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_twitter_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 25,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_twitter_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 30,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_ig',
	'label'    => esc_html__( 'Instagram URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 35,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_ig_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 40,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_ig_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 45,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_pinterest',
	'label'    => esc_html__( 'Pinterest URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 50,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_pinterest_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 55,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_pinterest_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 60,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_bloglovin',
	'label'    => esc_html__( 'Bloglovin URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 65,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_bloglovin_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 70,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_bloglovin_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 75,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_youtube',
	'label'    => esc_html__( 'Youtube URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 80,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_youtube_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 85,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_youtube_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 90,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_linkedin',
	'label'    => esc_html__( 'LinkedIn URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 95,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_linkedin_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 100,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_linkedin_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 105,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_linkedin',
	'label'    => esc_html__( 'LinkedIn URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 95,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_linkedin_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 100,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_linkedin_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 105,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_snapchat',
	'label'    => esc_html__( 'Snapchat URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 110,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_snapchat_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 115,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_snapchat_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 120,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_tumblr',
	'label'    => esc_html__( 'Tumblr URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 125,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_tumblr_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 130,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_tumblr_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 135,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_soundcloud',
	'label'    => esc_html__( 'Soundcloud URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 140,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_soundcloud_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 145,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_soundcloud_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 150,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_vimeo',
	'label'    => esc_html__( 'Vimeo URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 155,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_vimeo_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 160,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_vimeo_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 165,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_dribbble',
	'label'    => esc_html__( 'Dribbble URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 170,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_dribbble_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 175,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_dribbble_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 180,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_twitch',
	'label'    => esc_html__( 'Twitch URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 185,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_twitch_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 190,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_twitch_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 195,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_medium',
	'label'    => esc_html__( 'Medium URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 200,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_medium_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 205,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_medium_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 210,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_etsy',
	'label'    => esc_html__( 'Etsy URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 215,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_etsy_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 220,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_etsy_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 225,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_flickr',
	'label'    => esc_html__( 'Flickr URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 230,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_flickr_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 235,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_flickr_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 240,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_behance',
	'label'    => esc_html__( 'Behance URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 245,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_behance_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 250,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_behance_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 255,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_reddit',
	'label'    => esc_html__( 'Reddit URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 260,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_reddit_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 265,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_reddit_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 270,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_spotify',
	'label'    => esc_html__( 'Spotify URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 275,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_spotify_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 280,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_spotify_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 285,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_vk',
	'label'    => esc_html__( 'VK URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 290,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_vk_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 295,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_vk_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 300,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_weibo',
	'label'    => esc_html__( 'Weibo URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 305,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_weibo_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 310,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_weibo_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 315,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_rss',
	'label'    => esc_html__( 'RSS URL', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 320,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_rss_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 325,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_rss_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 330,
) );

Kirki::add_field( 'sitka_core_config', array(
	'type'     => 'text',
	'settings' => 'sitka_core_social_ic_email',
	'label'    => esc_html__( 'Email Address', 'sitka-core' ),
	'section'  => 'sitka_core_section_social_icons',
	'priority' => 340,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_email_header',
	'label'       => esc_html__( 'Display in Header', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 350,
) );
Kirki::add_field( 'sitka_core_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_core_social_ic_email_footer',
	'label'       => esc_html__( 'Display in Footer', 'sitka-core' ),
	'section'     => 'sitka_core_section_social_icons',
	'default'     => '1',
	'priority'    => 360,
) );