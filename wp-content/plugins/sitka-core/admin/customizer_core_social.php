<?php

Kirki::add_panel( 'sitka_core_panel_social', array(
    'priority'    => 38,
    'title'       => esc_attr__( 'Social Media Settings', 'sitka-core' ),
) );

include( plugin_dir_path( __FILE__ ) . '/customizer_core_social_share.php' );
include( plugin_dir_path( __FILE__ ) . '/customizer_core_social_icons.php' );
include( plugin_dir_path( __FILE__ ) . '/customizer_core_social_instagram.php' );