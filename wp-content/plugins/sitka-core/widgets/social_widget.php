<?php
/**
 * Social Widget
 */

add_action( 'widgets_init', 'sitka_social_load_widget' );

function sitka_social_load_widget() {
	register_widget( 'sitka_social_widget' );
}

class sitka_social_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'sitka_social_widget', 'description' => esc_html__('A widget that displays your social icons', 'sitka-core') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'sitka_social_widget' );

		/* Create the widget. */
		parent::__construct( 'sitka_social_widget', esc_html__('Sitka: Social Icons', 'sitka-core'), $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$columns = $instance['columns'];
		$facebook = $instance['facebook'];
		$twitter = $instance['twitter'];
		$instagram = $instance['instagram'];
		$bloglovin = $instance['bloglovin'];
		$youtube = $instance['youtube'];
		$tumblr = $instance['tumblr'];
		$pinterest = $instance['pinterest'];
		$dribbble = $instance['dribbble'];
		$soundcloud = $instance['soundcloud'];
		$vimeo = $instance['vimeo'];
		$linkedin = $instance['linkedin'];
		$snapchat = $instance['snapchat'];
		$rss = $instance['rss'];
		$twitch = $instance['twitch'];
		$medium = $instance['medium'];
		$flickr = $instance['flickr'];
		$etsy = $instance['etsy'];
		$behance = $instance['behance'];
		$reddit = $instance['reddit'];
		$spotify = $instance['spotify'];
		$vk = $instance['vk'];
		$weibo = $instance['weibo'];
		$email = $instance['email'];
		
		if(!$title) {
			$args['before_widget'] = str_replace('class="', 'class="widget-no-title ', $args['before_widget']);
		}
		
		echo wp_kses_post( $args['before_widget'] );

		if ( $title ) {
			echo wp_kses_post( $args['before_title'] . $title . $args['after_title'] );
		}
		?>
			
			<div class="social-widget <?php if(!$title) : ?>no-title<?php endif; ?> <?php echo esc_attr($columns); ?>">

				<?php if($facebook) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_facebook')); ?>" class="facebook" target="_blank"><i class="fa fa-facebook"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Facebook', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($twitter) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_twitter')); ?>" class="twitter" target="_blank"><i class="fa fa-twitter"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Twitter', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($instagram) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_ig')); ?>" class="instagram" target="_blank"><i class="fa fa-instagram"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Instagram', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($pinterest) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_pinterest')); ?>" class="pinterest" target="_blank"><i class="fa fa-pinterest"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Pinterest', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($bloglovin) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_bloglovin')); ?>" class="bloglovin" target="_blank"><i class="fa fa-heart"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Bloglovin', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($youtube) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_youtube')); ?>" class="youtube" target="_blank"><i class="fa fa-youtube-play"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Youtube', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($linkedin) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_linkedin')); ?>" class="linkedin" target="_blank"><i class="fa fa-linkedin"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'LinkedIn', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($snapchat) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_snapchat')); ?>" class="snapchat" target="_blank"><i class="fa fa-snapchat-ghost"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Snapchat', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($tumblr) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_tumblr')); ?>" class="tumblr" target="_blank"><i class="fa fa-tumblr"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Tumblr', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($soundcloud) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_soundcloud')); ?>" class="soundcloud" target="_blank"><i class="fa fa-soundcloud"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Soundcloud', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($vimeo) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_vimeo')); ?>" class="vimeo" target="_blank"><i class="fa fa-vimeo"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Vimeo', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($dribbble) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_dribbble')); ?>" class="dribbble" target="_blank"><i class="fa fa-dribbble"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Dribbble', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($twitch) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_twitch')); ?>" class="twitch" target="_blank"><i class="fa fa-twitch"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Twitch', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($medium) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_medium')); ?>" class="medium" target="_blank"><i class="fa fa-medium"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Medium', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($etsy) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_etsy')); ?>" class="etsy" target="_blank"><i class="fa fa-etsy"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Etsy', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($flickr) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_flickr')); ?>" class="flickr" target="_blank"><i class="fa fa-flickr"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Flickr', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($behance) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_behance')); ?>" class="behance" target="_blank"><i class="fa fa-behance"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Behance', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($reddit) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_reddit')); ?>" class="reddit" target="_blank"><i class="fa fa-reddit"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Reddit', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($spotify) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_spotify')); ?>" class="spotify" target="_blank"><i class="fa fa-spotify"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Spotify', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($vk) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_vk')); ?>" class="vk" target="_blank"><i class="fa fa-vk"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Vk', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($weibo) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_weibo')); ?>" class="weibo" target="_blank"><i class="fa fa-weibo"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Weibo', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($rss) : ?><a href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_rss')); ?>" class="rss" target="_blank"><i class="fa fa-rss"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'RSS', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				<?php if($email) : ?><a href="mailto:<?php echo get_theme_mod('sitka_core_social_ic_email'); ?>" class="email" target="_blank"><i class="fa fa-envelope-o"></i><?php if($columns == 'column1') : ?> <span><?php esc_html_e( 'Email', 'sitka-core' ); ?></span><?php endif; ?></a><?php endif; ?>
				
			</div>
			
		<?php

		/* After widget. */
		echo wp_kses_post( $args['after_widget'] );
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['columns'] = $new_instance['columns'];
		$instance['facebook'] = strip_tags( $new_instance['facebook'] );
		$instance['twitter'] = strip_tags( $new_instance['twitter'] );
		$instance['instagram'] = strip_tags( $new_instance['instagram'] );
		$instance['bloglovin'] = strip_tags( $new_instance['bloglovin'] );
		$instance['youtube'] = strip_tags( $new_instance['youtube'] );
		$instance['tumblr'] = strip_tags( $new_instance['tumblr'] );
		$instance['pinterest'] = strip_tags( $new_instance['pinterest'] );
		$instance['dribbble'] = strip_tags( $new_instance['dribbble'] );
		$instance['soundcloud'] = strip_tags( $new_instance['soundcloud'] );
		$instance['vimeo'] = strip_tags( $new_instance['vimeo'] );
		$instance['linkedin'] = strip_tags( $new_instance['linkedin'] );
		$instance['snapchat'] = strip_tags( $new_instance['snapchat'] );
		$instance['rss'] = strip_tags( $new_instance['rss'] );		
		$instance['twitch'] = strip_tags( $new_instance['twitch'] );
		$instance['medium'] = strip_tags( $new_instance['medium'] );
		$instance['flickr'] = strip_tags( $new_instance['flickr'] );
		$instance['etsy'] = strip_tags( $new_instance['etsy'] );
		$instance['behance'] = strip_tags( $new_instance['behance'] );
		$instance['reddit'] = strip_tags( $new_instance['reddit'] );
		$instance['spotify'] = strip_tags( $new_instance['spotify'] );
		$instance['vk'] = strip_tags( $new_instance['vk'] );
		$instance['weibo'] = strip_tags( $new_instance['weibo'] );
		$instance['email'] = strip_tags( $new_instance['email'] );
		
		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Subscribe & Follow', 'facebook' => 'on', 'twitter' => 'on', 'instagram' => 'on', 'columns' => 'column3', 'bloglovin' => '', 'youtube' => '', 'tumblr' => '', 'pinterest' => '',
						   'dribbble' => '', 'soundcloud' => '', 'vimeo' => '', 'linkedin' => '', 'snapchat' => '', 'rss' => '', 'twitch' => '', 'medium' => '', 'flickr' => '', 'etsy' => '', 'behance' => '',
						   'reddit' => '', 'spotify' => '', 'vk' => '', 'weibo' => '', 'email' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e( 'Title', 'sitka-core' ); ?>:</label>
			<input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:90%;" />
		</p>
		
		<!-- Layout -->
		<p>
		<label for="<?php echo esc_attr($this->get_field_id('columns')); ?>"><?php esc_html_e( 'Layout', 'sitka-core' ); ?>:</label> 
		<select id="<?php echo esc_attr($this->get_field_id('columns')); ?>" name="<?php echo esc_attr($this->get_field_name('columns')); ?>" class="widefat categories" style="width:100%;">
			<option value='column1' <?php if ('column1' == $instance['columns']) echo 'selected="selected"'; ?>><?php esc_html_e( '1 Column (Includes Social Media Name)', 'sitka-core' ); ?></option>
			<option value='column2' <?php if ('column2' == $instance['columns']) echo 'selected="selected"'; ?>><?php esc_html_e( '2 Columns', 'sitka-core' ); ?></option>
			<option value='column3' <?php if ('column3' == $instance['columns']) echo 'selected="selected"'; ?>><?php esc_html_e( '3 Columns', 'sitka-core' ); ?></option>
			<option value='column4' <?php if ('column4' == $instance['columns']) echo 'selected="selected"'; ?>><?php esc_html_e( '4 Columns', 'sitka-core' ); ?></option>
			<option value='column5' <?php if ('column5' == $instance['columns']) echo 'selected="selected"'; ?>><?php esc_html_e( '5 Columns', 'sitka-core' ); ?></option>
			<option value='rounded' <?php if ('rounded' == $instance['columns']) echo 'selected="selected"'; ?>><?php esc_html_e( 'Rounded Icons', 'sitka-core' ); ?></option>
		</select>
		<small><?php esc_html_e( 'For example, if you have checked 6 social icons, 3 columns would fit the best.', 'sitka-core' ); ?></small>
		</p>
		
		<p><?php esc_html_e( 'Note: Set your social links in the Customizer', 'sitka-core' ); ?></p>
		
		<div class="social-widget-wrap">
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'facebook' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'facebook' )); ?>" <?php checked( (bool) $instance['facebook'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'facebook' )); ?>"><?php esc_html_e( 'Show Facebook', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'twitter' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitter' )); ?>" <?php checked( (bool) $instance['twitter'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'twitter' )); ?>"><?php esc_html_e( 'Show Twitter', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'instagram' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'instagram' )); ?>" <?php checked( (bool) $instance['instagram'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'instagram' )); ?>"><?php esc_html_e( 'Show Instagram', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'pinterest' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'pinterest' )); ?>" <?php checked( (bool) $instance['pinterest'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'pinterest' )); ?>"><?php esc_html_e( 'Show Pinterest', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'bloglovin' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'bloglovin' )); ?>" <?php checked( (bool) $instance['bloglovin'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'bloglovin' )); ?>"><?php esc_html_e( 'Show Bloglovin', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'youtube' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'youtube' )); ?>" <?php checked( (bool) $instance['youtube'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'youtube' )); ?>"><?php esc_html_e( 'Show Youtube', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'linkedin' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'linkedin' )); ?>" <?php checked( (bool) $instance['linkedin'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'linkedin' )); ?>"><?php esc_html_e( 'Show Linkedin', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'snapchat' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'snapchat' )); ?>" <?php checked( (bool) $instance['snapchat'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'snapchat' )); ?>"><?php esc_html_e( 'Show Snapchat', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'tumblr' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'tumblr' )); ?>" <?php checked( (bool) $instance['tumblr'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'tumblr' )); ?>"><?php esc_html_e( 'Show Tumblr', 'sitka-core' ); ?></label>
		</p>

		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'dribbble' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'dribbble' )); ?>" <?php checked( (bool) $instance['dribbble'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'dribbble' )); ?>"><?php esc_html_e( 'Show Dribbble', 'sitka-core' ); ?></label>
		</p>

		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'soundcloud' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'soundcloud' )); ?>" <?php checked( (bool) $instance['soundcloud'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'soundcloud' )); ?>"><?php esc_html_e( 'Show Soundcloud', 'sitka-core' ); ?></label>
		</p>

		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'vimeo' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'vimeo' )); ?>" <?php checked( (bool) $instance['vimeo'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'vimeo' )); ?>"><?php esc_html_e( 'Show Vimeo', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'twitch' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitch' )); ?>" <?php checked( (bool) $instance['twitch'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'twitch' )); ?>"><?php esc_html_e( 'Show Twitch', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'medium' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'medium' )); ?>" <?php checked( (bool) $instance['medium'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'medium' )); ?>"><?php esc_html_e( 'Show Medium', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'flickr' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'flickr' )); ?>" <?php checked( (bool) $instance['flickr'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'flickr' )); ?>"><?php esc_html_e( 'Show Flickr', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'etsy' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'etsy' )); ?>" <?php checked( (bool) $instance['etsy'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'etsy' )); ?>"><?php esc_html_e( 'Show Etsy', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'behance' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'behance' )); ?>" <?php checked( (bool) $instance['behance'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'behance' )); ?>"><?php esc_html_e( 'Show Behance', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'reddit' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'reddit' )); ?>" <?php checked( (bool) $instance['reddit'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'reddit' )); ?>"><?php esc_html_e( 'Show Reddit', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'spotify' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'spotify' )); ?>" <?php checked( (bool) $instance['spotify'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'spotify' )); ?>"><?php esc_html_e( 'Show Spotify', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'vk' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'vk' )); ?>" <?php checked( (bool) $instance['vk'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'vk' )); ?>"><?php esc_html_e( 'Show Vk', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'weibo' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'weibo' )); ?>" <?php checked( (bool) $instance['weibo'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'weibo' )); ?>"><?php esc_html_e( 'Show Weibo', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'rss' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'rss' )); ?>" <?php checked( (bool) $instance['rss'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'rss' )); ?>"><?php esc_html_e( 'Show RSS', 'sitka-core' ); ?></label>
		</p>
		
		<p>
			<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'email' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'email' )); ?>" <?php checked( (bool) $instance['email'], true ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'email' )); ?>"><?php esc_html_e( 'Show Email', 'sitka-core' ); ?></label>
		</p>
		
		</div>


	<?php
	}
}

?>