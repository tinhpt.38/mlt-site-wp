<?php
/*
* Social share title fix
*/
function sitka_social_title( $title ) {
    $title = html_entity_decode( $title );
    $title = urlencode( $title );
    return $title;
}

/*
* Social Share Buttons
*/
function sitka_get_social_share($position) { 
	
	$social_layout = get_theme_mod('sitka_core_social_share_layout', 'style1');
	
	$facebook = get_theme_mod('sitka_core_social_share_facebook', true);
	$twitter = get_theme_mod('sitka_core_social_share_twitter', true);
	$pinterest = get_theme_mod('sitka_core_social_share_pinterest', true);
	$email = get_theme_mod('sitka_core_social_share_email', true);

	// Get Featured image for pinterest
	$pin_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>
	<?php if($facebook || $twitter || $pinterest || $email) : ?>
	
	<div class="post-<?php echo esc_attr($position); ?>-share-wrap desktop">
		<div class="post-<?php echo esc_attr($position); ?>-share share-<?php echo esc_attr($social_layout); ?>">
		
		<?php if(get_theme_mod('sitka_core_social_share_facebook', true)) : ?>
			<a class="share-button facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
				<i class="fa fa-facebook"></i>
			</a>
		<?php endif; ?>
		
		<?php if(get_theme_mod('sitka_core_social_share_twitter', true)) : ?>
			<a class="share-button twitter" target="_blank" href="https://twitter.com/intent/tweet?text=Check%20out%20this%20article:%20<?php print sitka_social_title( get_the_title() ); ?>&url=<?php echo urlencode(the_permalink()); ?><?php if(get_theme_mod('sitka_core_social_share_twitter_via')) : ?>&via=<?php echo esc_html(get_theme_mod('sitka_core_social_share_twitter_via')); ?><?php endif; ?>">
				<i class="fa fa-twitter"></i>
			</a>
		<?php endif; ?>
		
		<?php if(get_theme_mod('sitka_core_social_share_pinterest', true)) : ?>
			<a class="share-button pinterest" data-pin-do="none" target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode(the_permalink()); ?>&media=<?php echo esc_url($pin_image); ?>&description=<?php print sitka_social_title( get_the_title() ); ?>">
				<i class="fa fa-pinterest"></i>
			</a>
		<?php endif; ?>
		
		<?php if(get_theme_mod('sitka_core_social_share_linkedin', true)) : ?>
		<a target="_blank" class="share-button linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(the_permalink()); ?>&title=<?php print sitka_social_title( get_the_title() ); ?>">
			<i class="fa fa-linkedin"></i>
		</a>
		<?php endif; ?>
		
		<?php if(get_theme_mod('sitka_core_social_share_email', true)) : ?>
		<a target="_blank" class="share-button email" href="mailto:?subject=I%20wanted%20to%20share%20this%20article%20with%20you&amp;body=<?php the_permalink(); ?>">
		  <i class="fa fa-envelope-o"></i>
		  
		</a>
		<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>
	
<?php }

/*
* Mobile Social Share Buttons
*/
function sitka_sitka_get_mobile_social_share() { 

$social_layout = get_theme_mod('sitka_core_social_share_layout', 'style1');

?>

	<div class="post-header-share-wrap mobile">
		<div class="post-header-share share-<?php echo esc_attr($social_layout); ?>">
		
			<a href="#" class="share-button share-toggle"><i class="fa fa-share-alt"></i></a>
				
				<div class="show-share">
					
					<?php if(get_theme_mod('sitka_core_social_share_facebook', true)) : ?>
						<a class="share-button facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
							<i class="fa fa-facebook"></i>
						</a>
					<?php endif; ?>
					
					<?php if(get_theme_mod('sitka_core_social_share_twitter', true)) : ?>
						<a class="share-button twitter" target="_blank" href="https://twitter.com/intent/tweet?text=Check%20out%20this%20article:%20<?php print sitka_social_title( get_the_title() ); ?>&url=<?php echo urlencode(the_permalink()); ?><?php if(get_theme_mod('sitka_core_social_share_twitter_via')) : ?>&via=<?php echo esc_html(get_theme_mod('sitka_core_social_share_twitter_via')); ?><?php endif; ?>">
							<i class="fa fa-twitter"></i>
						</a>
					<?php endif; ?>
					
					<?php if(get_theme_mod('sitka_core_social_share_pinterest', true)) : ?>
						<a class="share-button pinterest" data-pin-do="none" target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode(the_permalink()); ?>&media=<?php echo esc_url($pin_image); ?>&description=<?php print sitka_social_title( get_the_title() ); ?>">
							<i class="fa fa-pinterest"></i>
						</a>
					<?php endif; ?>
					
					<?php if(get_theme_mod('sitka_core_social_share_linkedin', true)) : ?>
					<a class="share-button linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(the_permalink()); ?>&title=<?php print sitka_social_title( get_the_title() ); ?>">
						<i class="fa fa-linkedin"></i>
					</a>
					<?php endif; ?>
					
					<?php if(get_theme_mod('sitka_core_social_share_email', true)) : ?>
					<a class="share-button email" href="mailto:?subject=<?php esc_html_e( 'I wanted to share this article with you', 'sitka-core' ); ?>&amp;body=<?php the_permalink(); ?>">
					  <i class="fa fa-envelope-o"></i>
					  
					</a>
					<?php endif; ?>
					
				</div>
		
		
		</div>
	</div>

<?php }