<?php
/*
* Author Social Icons
* Adding extra social icons to the author profile
*/
if(!function_exists('sitka_contactmethods')) {
function sitka_contactmethods( $contactmethods ) {

	$contactmethods['twitter']   = esc_html__('Twitter URL', 'sitka-core');
	$contactmethods['facebook']  = esc_html__('Facebook URL', 'sitka-core');
	$contactmethods['instagram'] = esc_html__('Instagram URL', 'sitka-core');
	$contactmethods['tumblr']    = esc_html__('Tumblr URL', 'sitka-core');
	$contactmethods['youtube'] = esc_html__('Youtube URL', 'sitka-core');
	$contactmethods['pinterest'] = esc_html__('Pinterest URL', 'sitka-core');
	$contactmethods['linkedin'] = esc_html__('LinkedIn URL', 'sitka-core');
	$contactmethods['dribbble'] = esc_html__('Dribbble URL', 'sitka-core');
	$contactmethods['sitka-email'] = esc_html__('Email', 'sitka-core');

	return $contactmethods;
}
add_filter('user_contactmethods','sitka_contactmethods',10,1);
}

function sitka_core_get_author_icons() { ?>
	
	<?php if(get_the_author_meta('facebook')) : ?><a target="_blank" href="<?php echo esc_html(the_author_meta('facebook')); ?>"><i class="fa fa-facebook"></i></a><?php endif; ?>
	<?php if(get_the_author_meta('twitter')) : ?><a target="_blank" href="<?php echo esc_html(the_author_meta('twitter')); ?>"><i class="fa fa-twitter"></i></a><?php endif; ?>
	<?php if(get_the_author_meta('instagram')) : ?><a target="_blank" href="<?php echo esc_html(the_author_meta('instagram')); ?>"><i class="fa fa-instagram"></i></a><?php endif; ?>
	<?php if(get_the_author_meta('pinterest')) : ?><a target="_blank" href="<?php echo esc_html(the_author_meta('pinterest')); ?>"><i class="fa fa-pinterest"></i></a><?php endif; ?>
	<?php if(get_the_author_meta('tumblr')) : ?><a target="_blank" href="<?php echo esc_html(the_author_meta('tumblr')); ?>"><i class="fa fa-tumblr"></i></a><?php endif; ?>
	<?php if(get_the_author_meta('youtube')) : ?><a target="_blank" href="<?php echo esc_html(the_author_meta('youtube')); ?>"><i class="fa fa-youtube-play"></i></a><?php endif; ?>
	<?php if(get_the_author_meta('dribbble')) : ?><a target="_blank" href="<?php echo esc_html(the_author_meta('dribbble')); ?>"><i class="fa fa-dribbble"></i></a><?php endif; ?>
	<?php if(get_the_author_meta('linkedin')) : ?><a target="_blank" href="<?php echo esc_html(the_author_meta('linkedin')); ?>"><i class="fa fa-linkedin"></i></a><?php endif; ?>
	<?php if(get_the_author_meta('sitka-email')) : ?><a target="_blank" href="mailto:<?php echo esc_html(the_author_meta('sitka-email')); ?>"><i class="fa fa-envelope-o"></i></a><?php endif; ?>
	
<?php }