<?php
/*
* Social Profile Icons
*/
function sitka_get_social_icons($area="header") {
	
	$social_profiles = array(
		array(
			'social_site'	=> 'facebook',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_facebook'),
			'header'		=> get_theme_mod('sitka_core_social_ic_facebook_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_facebook_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'twitter',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_twitter'),
			'header'		=> get_theme_mod('sitka_core_social_ic_twitter_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_twitter_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'instagram',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_ig'),
			'header'		=> get_theme_mod('sitka_core_social_ic_ig_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_ig_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'pinterest',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_pinterest'),
			'header'		=> get_theme_mod('sitka_core_social_ic_pinterest_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_pinterest_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'bloglovin',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_bloglovin'),
			'header'		=> get_theme_mod('sitka_core_social_ic_bloglovin_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_bloglovin_footer', true),
			'icon'			=> 'heart'
		),
		array(
			'social_site'	=> 'youtube',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_youtube'),
			'header'		=> get_theme_mod('sitka_core_social_ic_youtube_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_youtube_footer', true),
			'icon'			=> 'youtube-play'
		),
		array(
			'social_site'	=> 'linkedin',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_linkedin'),
			'header'		=> get_theme_mod('sitka_core_social_ic_linkedin_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_linkedin_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'snapchat',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_snapchat'),
			'header'		=> get_theme_mod('sitka_core_social_ic_snapchat_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_snapchat_footer', true),
			'icon'			=> 'snapchat-ghost'
		),
		array(
			'social_site'	=> 'tumblr',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_tumblr'),
			'header'		=> get_theme_mod('sitka_core_social_ic_tumblr_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_tumblr_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'soundcloud',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_soundcloud'),
			'header'		=> get_theme_mod('sitka_core_social_ic_soundcloud_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_soundcloud_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'vimeo',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_vimeo'),
			'header'		=> get_theme_mod('sitka_core_social_ic_vimeo_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_vimeo_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'dribbble',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_dribbble'),
			'header'		=> get_theme_mod('sitka_core_social_ic_dribbble_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_dribbble_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'twitch',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_twitch'),
			'header'		=> get_theme_mod('sitka_core_social_ic_twitch_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_twitch_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'medium',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_medium'),
			'header'		=> get_theme_mod('sitka_core_social_ic_medium_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_medium_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'etsy',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_etsy'),
			'header'		=> get_theme_mod('sitka_core_social_ic_etsy_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_etsy_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'flickr',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_flickr'),
			'header'		=> get_theme_mod('sitka_core_social_ic_flickr_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_flickr_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'behance',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_behance'),
			'header'		=> get_theme_mod('sitka_core_social_ic_behance_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_behance_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'reddit',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_reddit'),
			'header'		=> get_theme_mod('sitka_core_social_ic_reddit_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_reddit_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'spotify',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_spotify'),
			'header'		=> get_theme_mod('sitka_core_social_ic_spotify_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_spotify_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'vk',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_vk'),
			'header'		=> get_theme_mod('sitka_core_social_ic_vk_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_vk_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'weibo',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_weibo'),
			'header'		=> get_theme_mod('sitka_core_social_ic_weibo_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_weibo_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'rss',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_rss'),
			'header'		=> get_theme_mod('sitka_core_social_ic_rss_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_rss_footer', true),
			'icon'			=> ''
		),
		array(
			'social_site'	=> 'email',
			'profile_url'	=> get_theme_mod('sitka_core_social_ic_email'),
			'header'		=> get_theme_mod('sitka_core_social_ic_email_header', true),
			'footer'		=> get_theme_mod('sitka_core_social_ic_email_footer', true),
			'icon'			=> 'envelope-o'
		),
	);
	
	echo '<div class="'.$area.'-social">';
	foreach($social_profiles as $profile) {
	
		if($profile['profile_url']) {
			if($profile['header'] && $area == 'header' || $profile['footer'] && $area == 'footer') {
				
				if($profile['social_site'] == 'email') {
					echo '<a href="mailto:' . $profile['profile_url'] . '" class="' . $profile['social_site'] . '">';
				} else {
					echo '<a href="' . $profile['profile_url'] . '" class="' . $profile['social_site'] . '" target="_blank">';
				}
				if($profile['icon']) {
					echo '<i class="fa fa-'. $profile['icon'] .'"></i>';
				} else {
					echo '<i class="fa fa-' . $profile['social_site'] . '"></i>';
				}
				echo '</a>';
				
			}
		}
		
	}
	echo '</div>';
	
}