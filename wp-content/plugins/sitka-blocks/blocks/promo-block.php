<?php
/**
 *  Register Promo Box Block
 */
add_action('acf/init', 'sitka_blocks_promo');
function sitka_blocks_promo() {

    // check function exists
    if( function_exists('acf_register_block') ) {

        // register a testimonial block
        acf_register_block(array(
            'name'              => 'sitka-promo',
            'title'             => esc_html__('Sitka: Promo Boxes', 'sitka-blocks'),
            'description'       => esc_html__('A custom block to display promo boxes', 'sitka-blocks'),
            'render_callback'   => 'sitka_blocks_promo_callback',
            'category'          => 'sitka-blocks',
            'icon'              => 'screenoptions',
            'mode'              => 'preview',
			'supports'			=> array(
									'align' => array('wide'),
			),
            'keywords'          => array( 'sitka', 'promo', 'boxes' ),
        ));
    }
}

/**
 * Promo Box Block Callback
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block content (emtpy string).
 * @param   bool $is_preview True during AJAX preview.
 */
function sitka_blocks_promo_callback( $block, $content = '', $is_preview = true ) {
	
    // Create id attribute for specific styling
    $id = 'sitka-promo-' . $block['id'];

    // create align class ("alignwide") from block setting ("wide")
    $align_class = $block['align'] ? 'align' . $block['align'] : '';
	
    ?>
	
	<div id="<?php echo $id; ?>" class="sitka-promo <?php echo esc_attr($align_class); ?>">
		
		<?php if( have_rows('sitka_blocks_promo_repeater') ) : ?>
			
			<div class="promo-wrap promo-grid promo-col-<?php echo esc_attr(get_field('sitka_blocks_promo_columns')); ?> promo-<?php echo esc_attr(get_field('sitka_blocks_promo_box_style')); ?>" style="grid-auto-rows: <?php echo esc_attr(get_field('sitka_blocks_promo_height')); ?>px;">
			
			<?php while ( have_rows('sitka_blocks_promo_repeater') ) : the_row(); ?>
				
				<?php
					$get_title = get_sub_field('sitka_blocks_promo_title');
					$get_subtitle = get_sub_field('sitka_blocks_promo_subtitle');
					$get_image = get_sub_field('sitka_blocks_promo_image');
					$get_url = get_sub_field('sitka_blocks_promo_url');
					$get_target = get_sub_field('sitka_blocks_promo_url_target');
				?>
				
				<div class="promo-item" style="background-image:url(<?php echo esc_url($get_image); ?>);">
					<?php if($get_url) : ?><a <?php if($get_target == true) : ?>target="_blank"<?php endif; ?> href="<?php echo esc_url($get_url); ?>" class="promo-link"></a><?php endif; ?>
					<div class="promo-overlay">
						<div class="promo-inner">
							<?php if($get_subtitle) : ?><span><?php echo esc_html($get_subtitle); ?></span><?php endif; ?>
							<?php if($get_title) : ?><h5><?php echo esc_html($get_title); ?></h5><?php endif; ?>
						</div>
					</div>
					<div class="promo-shadow"></div>
				</div>
				
			<?php endwhile; ?>
				
			</div>
				
		<?php else : ?>
			
			<?php if(is_admin()) : ?>
				
				<div class="block-promo-admin">
					<h5>Promo Box Block</h5>
				</div>
				
			<?php endif; ?>

		<?php endif; ?>
		
	</div>
	
    <?php
}