<?php
/**
 *  Register Post Grid Block
 */
add_action('acf/init', 'sitka_blocks_post_grid');
function sitka_blocks_post_grid() {

    // check function exists
    if( function_exists('acf_register_block') ) {

        // register a testimonial block
        acf_register_block(array(
            'name'              => 'sitka-post-grid',
            'title'             => esc_html__('Sitka: Post Grid', 'sitka-blocks'),
            'description'       => esc_html__('A custom block to display posts in a grid layout', 'sitka-blocks'),
            'render_callback'   => 'sitka_blocks_post_grid_callback',
            'category'          => 'sitka-blocks',
            'icon'              => 'screenoptions',
            'mode'              => 'preview',
			'supports'			=> array(
									'align' => array('wide'),
			),
            'keywords'          => array( 'sitka', 'posts' ),
        ));
    }
}

/**
 *  Post Blog Callback
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block content (emtpy string).
 * @param   bool $is_preview True during AJAX preview.
 */
function sitka_blocks_post_grid_callback( $block, $content = '', $is_preview = true ) {

	// Get block title
	$grid_title = get_field('sitka_blocks_post_grid_title');
	
	// Get block columns
	$grid_cols = get_field('sitka_blocks_post_grid_cols');
	$grid_cols = isset($grid_cols) ? $grid_cols : '3';
	
	// Get block amount of posts
	$grid_amount = get_field('sitka_blocks_post_grid_amount');
	$grid_amount = isset($grid_amount) ? $grid_amount : '3';
	
	// Get block content type
	$grid_type = get_field('sitka_blocks_post_grid_content_type');
	$grid_type = isset($grid_type) ? $grid_type : 'latest';
	
	// Get block sorting
	$grid_sort = get_field('sitka_blocks_post_grid_sort');
	$grid_sort = isset($grid_sort) ? $grid_sort : 'latest';
	
	/* Hide/Show */
	$grid_show_image = get_field('sitka_blocks_post_grid_show_image');
	$grid_show_image = isset($grid_show_image) ? $grid_show_image : true;
	
	$grid_show_cat = get_field('sitka_blocks_post_grid_show_cat');
	$grid_show_cat = isset($grid_show_cat) ? $grid_show_cat : true;
	
	$grid_show_title = get_field('sitka_blocks_post_grid_show_title');
	$grid_show_title = isset($grid_show_title) ? $grid_show_title : true;
	
	$grid_show_excerpt = get_field('sitka_blocks_post_grid_show_excerpt');
	$grid_show_excerpt = isset($grid_show_excerpt) ? $grid_show_excerpt : true;
	
	$grid_show_date = get_field('sitka_blocks_post_grid_show_date');
	$grid_show_date = isset($grid_show_date) ? $grid_show_date : true;
	
	if($grid_type == 'latest') {
		$grid_args = array(
			'posts_per_page' => $grid_amount,
			'ignore_sticky_posts' => 1
		);
	} elseif($grid_type == 'category') {
		$grid_cat = get_field('sitka_blocks_post_grid_content_type_category');
		$grid_args = array(
			'posts_per_page' => $grid_amount,
			'cat' => $grid_cat,
			'ignore_sticky_posts' => 1,
		);
		if($grid_sort == 'alphabetic') {
			$grid_args['order'] = 'ASC';
			$grid_args['orderby'] = 'title';
		} elseif($grid_sort == 'comments') {
			$grid_args['orderby'] = 'comment_count';
		} elseif($grid_sort == 'oldest') {
			$grid_args['order'] = 'ASC';
			$grid_args['orderby'] = 'date';
		}
	} elseif($grid_type == 'tag') {
		$grid_tag = get_field('sitka_blocks_post_grid_content_type_tag');
		$grid_args = array(
			'posts_per_page' => $grid_amount,
			'tag__in' => $grid_tag,
			'ignore_sticky_posts' => 1
		);
		if($grid_sort == 'alphabetic') {
			$grid_args['order'] = 'ASC';
			$grid_args['orderby'] = 'title';
		} elseif($grid_sort == 'comments') {
			$grid_args['orderby'] = 'comment_count';
		} elseif($grid_sort == 'oldest') {
			$grid_args['order'] = 'ASC';
			$grid_args['orderby'] = 'date';
		}
	} elseif($grid_type == 'specific') { 
		$grid_specific = get_field('sitka_blocks_post_grid_content_type_specific');
		$grid_specific_array = explode (",", $grid_specific);  
		$grid_args = array(
			'posts_per_page' => $grid_amount,
			'post_type' => array('post', 'page'),
			'post__in' => $grid_specific_array,
			'ignore_sticky_posts' => 1
		);
	}
	
	// Create grid post query
	if(!is_admin()) {
		$grid_query = new WP_Query( $grid_args );
	}
	

    // Create id attribute for specific styling
    $id = 'sitka-post-grid-' . $block['id'];

    // create align class ("alignwide") from block setting ("wide")
    $align_class = $block['align'] ? 'align' . $block['align'] : '';
	
    ?>
	
	<div id="<?php echo $id; ?>" class="sitka-post-grid <?php echo esc_attr($align_class); ?>">
		
		<?php if($grid_title) : ?>
		<div class="block-heading-wrap">
			<h5 class="block-heading"><?php echo esc_html($grid_title); ?></h5>
		</div>
		<?php endif; ?>

		<?php if(is_admin()) : ?>
		
			<div class="sitka-post-grid-admin-wrap">
				
				<h5><?php esc_html_e( 'Grid Posts Block', 'sitka-blocks' ); ?></h5>
				
				<div class="grid-info">

					<?php
						if($grid_type == 'category') {
							echo 'Category: ';
							foreach($grid_cat as $cat) {
								echo '<span>' . get_cat_name($cat[0]) . '</span>';
							}
						} elseif($grid_type == 'tag') {
							echo 'Tag: ';
							foreach($grid_tag as $tag_id) {
								$tag = get_term( $tag_id, 'post_tag' );
								echo '<span>' . $tag->name . '</span>';
							}
						} elseif($grid_type == 'latest') {
							echo 'Latest Posts';
						} elseif($grid_type == 'specific') {
							echo 'Post/Page IDs: ' . $grid_specific;
						}
					?>
					
				</div>
				
			</div>
		
		<?php else : ?>
		
			<div class="sitka-post-grid-wrap cols-<?php echo esc_attr($grid_cols); ?>">
				
				<?php if($grid_query->have_posts()) : while ($grid_query->have_posts()) : $grid_query->the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" class="sitka-block-grid-item">
						
						<?php if(has_post_thumbnail() && $grid_show_image) : ?>
						<div class="sitka-block-grid-img">				
							<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('sitka-grid-post-thumb'); ?></a>
						</div>
						<?php endif; ?>
						
						<?php if($grid_show_cat) : ?>
						<div class="post-cats">
							<?php the_category(' <span>/</span> '); ?>
						</div>
						<?php endif; ?>
						
						<?php if($grid_show_title) : ?>
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php endif; ?>
						
						<?php 
						if($grid_show_excerpt) {
							if(function_exists('sitka_excerpt')) {
								echo sitka_excerpt(14);
							} else {
								echo get_the_excerpt();
							}
						}
						?>
						
						<?php if($grid_show_date) : ?>
						<span class="sp-date updated published"><?php the_time( get_option('date_format') ); ?></span>
						<?php endif; ?>
					
					</article>
					
				<?php endwhile; wp_reset_postdata(); endif; ?>
				
			</div>
			
		<?php endif; ?>

	</div>
	
    <?php
}