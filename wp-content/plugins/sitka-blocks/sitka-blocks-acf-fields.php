<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5c39127a837b0',
	'title' => 'Post Grid Block',
	'fields' => array(
		array(
			'key' => 'field_5c3989cb03a58',
			'label' => 'General Settings',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 1,
			'multi_expand' => 0,
			'endpoint' => 0,
		),
		array(
			'key' => 'field_5c3912928eb4a',
			'label' => 'Title',
			'name' => 'sitka_blocks_post_grid_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5c391c70aec6a',
			'label' => 'Grid Columns',
			'name' => 'sitka_blocks_post_grid_cols',
			'type' => 'range',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 3,
			'min' => 2,
			'max' => 4,
			'step' => '',
			'prepend' => '',
			'append' => '',
		),
		array(
			'key' => 'field_5c39875247da5',
			'label' => 'Number of Posts',
			'name' => 'sitka_blocks_post_grid_amount',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 3,
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => 1,
			'max' => '',
			'step' => 1,
		),
		array(
			'key' => 'field_5c392dba31b5f',
			'label' => 'Show Posts By',
			'name' => 'sitka_blocks_post_grid_content_type',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'latest' => 'Latest Posts',
				'category' => 'Categories',
				'tag' => 'Tags',
				'specific' => 'Specific Posts',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => 'latest',
			'layout' => 'vertical',
			'return_format' => 'value',
			'save_other_choice' => 0,
		),
		array(
			'key' => 'field_5c392e6831b60',
			'label' => 'Select Category',
			'name' => 'sitka_blocks_post_grid_content_type_category',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5c392dba31b5f',
						'operator' => '==',
						'value' => 'category',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'category',
			'field_type' => 'multi_select',
			'allow_null' => 0,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
		),
		array(
			'key' => 'field_5c3983ca77ca9',
			'label' => 'Select Tag',
			'name' => 'sitka_blocks_post_grid_content_type_tag',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5c392dba31b5f',
						'operator' => '==',
						'value' => 'tag',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'post_tag',
			'field_type' => 'multi_select',
			'allow_null' => 0,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
		),
		array(
			'key' => 'field_5c39865dafac7',
			'label' => 'Enter Post IDs',
			'name' => 'sitka_blocks_post_grid_content_type_specific',
			'type' => 'text',
			'instructions' => 'Enter post IDs separated by comma (fx: 34, 81, 23)',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5c392dba31b5f',
						'operator' => '==',
						'value' => 'specific',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5c3987e0fb83c',
			'label' => 'Sort By',
			'name' => 'sitka_blocks_post_grid_sort',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5c392dba31b5f',
						'operator' => '!=',
						'value' => 'latest',
					),
					array(
						'field' => 'field_5c392dba31b5f',
						'operator' => '!=',
						'value' => 'specific',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'latest' => 'Latest',
				'oldest' => 'Oldest',
				'alphabetic' => 'Alphabetic (A - Z)',
				'comments' => 'Most Commented',
			),
			'default_value' => array(
				0 => 'latest',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_5c39897d73086',
			'label' => 'Hide/Show Settings',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 0,
			'multi_expand' => 0,
			'endpoint' => 0,
		),
		array(
			'key' => 'field_5c39893e17df3',
			'label' => 'Display Featured Image',
			'name' => 'sitka_blocks_post_grid_show_image',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 1,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array(
			'key' => 'field_5c3d0b8ead03d',
			'label' => 'Display Category',
			'name' => 'sitka_blocks_post_grid_show_cat',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 1,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array(
			'key' => 'field_5c3d0bfbc4792',
			'label' => 'Display Post Title',
			'name' => 'sitka_blocks_post_grid_show_title',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 1,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array(
			'key' => 'field_5c3d0c8af9b16',
			'label' => 'Display Excerpt',
			'name' => 'sitka_blocks_post_grid_show_excerpt',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 1,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array(
			'key' => 'field_5c3d0c95f9b17',
			'label' => 'Display Date',
			'name' => 'sitka_blocks_post_grid_show_date',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 1,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/sitka-post-grid',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5dc4bb3b7bea4',
	'title' => 'Promo Box Block',
	'fields' => array(
		array(
			'key' => 'field_5dc51b940a6df',
			'label' => 'Promo Box Style',
			'name' => 'sitka_blocks_promo_box_style',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'style1' => 'Style 1',
				'style2' => 'Style 2',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => 'style2',
			'layout' => 'vertical',
			'return_format' => 'value',
			'save_other_choice' => 0,
		),
		array(
			'key' => 'field_5dc51c01c48fb',
			'label' => 'Columns',
			'name' => 'sitka_blocks_promo_columns',
			'type' => 'range',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 3,
			'min' => 1,
			'max' => 4,
			'step' => '',
			'prepend' => '',
			'append' => '',
		),
		array(
			'key' => 'field_5dc5d262f5b9b',
			'label' => 'Promo Box Height',
			'name' => 'sitka_blocks_promo_height',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 200,
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => 0,
			'max' => 1000,
			'step' => 1,
		),
		array(
			'key' => 'field_5dc50546c99e3',
			'label' => 'Promo Boxes',
			'name' => 'sitka_blocks_promo_repeater',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => 0,
			'max' => 0,
			'layout' => 'block',
			'button_label' => 'Add Promo Box',
			'sub_fields' => array(
				array(
					'key' => 'field_5dc511c021057',
					'label' => 'Image',
					'name' => 'sitka_blocks_promo_image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'url',
					'preview_size' => 'medium',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array(
					'key' => 'field_5dc511b721056',
					'label' => 'Title',
					'name' => 'sitka_blocks_promo_title',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5dc51c60828fa',
					'label' => 'Subtitle',
					'name' => 'sitka_blocks_promo_subtitle',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5dc51b940a6df',
								'operator' => '==',
								'value' => 'style1',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5dc5122721058',
					'label' => 'URL',
					'name' => 'sitka_blocks_promo_url',
					'type' => 'url',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
				),
				array(
					'key' => 'field_5dc653d0562be',
					'label' => 'Open link in a new tab?',
					'name' => 'sitka_blocks_promo_url_target',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
			),
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/sitka-promo',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;