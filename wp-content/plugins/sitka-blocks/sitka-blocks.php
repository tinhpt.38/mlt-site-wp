<?php
/*
Plugin Name: Sitka Blocks
Plugin URI: http://solopine.com
Description: Adds additional custom Gutenberg blocks
Author: Solo Pine
Version: 1.2.2
Author URI: http://solopine.com
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function sitka_blocks_fields_init() {
	// load language files
	load_plugin_textdomain( 'sitka-blocks', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
}
add_action( 'init', 'sitka_blocks_fields_init' );

// Frontend and editor styles
function sitka_blocks_styles() {
	wp_enqueue_style( 'sitka_blocks_styles', plugin_dir_url( __FILE__ ) . 'css/sitka-blocks-style.css' );
}
add_action( 'enqueue_block_assets', 'sitka_blocks_styles' );

// Editor styles
function sitka_blocks_editor_styles() {
	wp_enqueue_style( 'sitka_blocks_editor_styles', plugin_dir_url( __FILE__ ) . 'css/sitka-blocks-editor-style.css' );
}
add_action( 'enqueue_block_editor_assets', 'sitka_blocks_editor_styles' );

// TGM
include( dirname(__FILE__) . '/sitka-blocks-tgm.php');

// Register Sitka Blocks category
function sitka_blocks_category( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug'	=> 'sitka-blocks',
                'title'	=> esc_html__( 'Sitka Blocks', 'sitka-blocks' ),
            ),
        )
    );
}
add_filter( 'block_categories', 'sitka_blocks_category', 10, 2 );

// Blocks
include( dirname(__FILE__) . '/blocks/post-grid-block.php');
include( dirname(__FILE__) . '/blocks/promo-block.php');
include( dirname(__FILE__) . '/sitka-blocks-acf-fields.php');

// Add Image Sizes
function sitka_blocks_image_sizes() {
	// Post Grid Block
	add_image_size( 'sitka-grid-post-thumb', 600, 440, true );
}
add_action( 'after_setup_theme', 'sitka_blocks_image_sizes' );