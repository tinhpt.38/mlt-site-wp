<?php

// Add Section
Kirki::add_section( 'sitka_section_misc', array(
    'title'          => esc_html__( 'MISC Settings', 'sitka' ),
    'priority'       => 52,
) );

// Add Settings

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_misc_acf',
	'label'       => esc_html__( 'Show ACF Pro in WP Dashboard Menu?', 'sitka' ),
	'description' => esc_html__( 'Enable this if you need to create your own Custom Fields using the Advanced Custom Fields Pro plugin. Only recommended for experienced users.', 'sitka' ),
	'section'     => 'sitka_section_misc',
	'default'     => '0',
	'priority'    => 3,
) );