<?php

// Add Section
Kirki::add_section( 'sitka_section_colors_misc', array(
    'title'          => esc_html__( 'MISC', 'sitka' ),
    'priority'       => 250,
	'panel'			 => 'sitka_panel_colors'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_misc_link',
	'label'       => esc_html__( 'General Link Color', 'sitka' ),
	'section'     => 'sitka_section_colors_misc',
	'default'     => '#f78a74',
	'priority'    => 5,
	'output'    => array(
		array(
			'element'         => 'a',
			'property'        => 'color',
		),
	),
	'transport'	  => 'auto'
) );