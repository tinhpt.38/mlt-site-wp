<?php

Kirki::add_panel( 'sitka_panel_colors', array(
    'priority'    => 45,
    'title'       => esc_attr__( 'Colors & Typography Settings', 'sitka' ),
	'icon' 		  => 'dashicons-admin-appearance'
) );

include( get_template_directory() . '/admin/customizer/customizer_colors_header.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_footer.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_sidebar.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_posts.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_archive.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_featured.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_promo.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_list_grid.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_pagination.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_mobile_menu.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_misc.php');
include( get_template_directory() . '/admin/customizer/customizer_colors_palette.php');