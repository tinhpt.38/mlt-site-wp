<?php

// Add Section
Kirki::add_section( 'sitka_section_footer', array(
    'title'          => esc_html__( 'Footer Settings', 'sitka' ),
    'priority'       => 35,
) );

// Add Settings
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_footer_logo_enable',
	'label'       => esc_html__( 'Display Footer Logo?', 'sitka' ),
	'section'     => 'sitka_section_footer',
	'default'     => '1',
	'priority'    => 1,
) );


Kirki::add_field( 'sitka_config', array(
	'type'        => 'image',
	'settings'    => 'sitka_footer_logo',
	'label'       => esc_html__( 'Upload Footer Logo', 'sitka' ),
	'section'     => 'sitka_section_footer',
	'default'     => get_template_directory_uri() . '/img/footer-logo.png',
	'priority'    => 2,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_footer_logo_max_width',
	'label'       => esc_html__( 'Logo Max width', 'sitka' ),
	'section'     => 'sitka_section_footer',
	'description' => esc_html__( 'Default: 400px', 'sitka' ),
	'default'     => 400,
	'choices'     => array(
		'min'  => 0,
		'max'  => 2000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#footer-logo img',
			'property' => 'max-width',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 3,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'Editor',
	'settings'    => 'sitka_footer_copyright_text',
	'label'       => esc_html__( 'Footer Copyright Text', 'sitka' ),
	'section'     => 'sitka_section_footer',
	'default'     => '<p>'. esc_html__( '(C) 2019 - Solo Pine. All Rights Reserved.', 'sitka' ) .'</p>',
	'priority'    => 4,
) );