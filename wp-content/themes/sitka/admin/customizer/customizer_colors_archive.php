<?php

// Add Section
Kirki::add_section( 'sitka_section_colors_archive', array(
    'title'          => esc_html__( 'Archives', 'sitka' ),
    'priority'       => 220,
	'panel'			 => 'sitka_panel_colors'
) );


/* Widget title */

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_archivebox_bg',
	'label'       => esc_html__( 'Archive Header Background Color', 'sitka' ),
	'section'     => 'sitka_section_colors_archive',
	'default'     => '#f7f7f7',
	'priority'    => 5,
	'output'    => array(
		array(
			'element'         => '.archive-box',
			'property'        => 'background-color',
		),
	),
	'transport'	  => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_archivebox_border',
	'label'       => esc_html__( 'Archive Header Border Color', 'sitka' ),
	'section'     => 'sitka_section_colors_archive',
	'default'     => '#eaeaea',
	'priority'    => 9,
	'output'    => array(
		array(
			'element'         => '.archive-box',
			'property'        => 'border-color',
		),
	),
	'transport'	  => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_archivebox_browsing',
	'label'       => esc_html__( 'Archive Box "Browsing" Text', 'sitka' ),
	'section'     => 'sitka_section_colors_archive',
	'default'     => array(
		'font-size'      => '14px',
		'letter-spacing' => '0',
		'color'          => '#888888',
		'text-transform' => 'none',
	),
	'priority'    => 13,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '.archive-box span',
		),
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_archivebox_title',
	'label'       => esc_html__( 'Archive Box Title Text', 'sitka' ),
	'section'     => 'sitka_section_colors_archive',
	'default'     => array(
		'font-size'      => '32px',
		'letter-spacing' => '4px',
		'color'          => '#000000',
		'text-transform' => 'uppercase',
	),
	'priority'    => 25,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '.archive-box h1',
		),
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_archivebox_description',
	'label'       => esc_html__( 'Archive Box Description Text', 'sitka' ),
	'section'     => 'sitka_section_colors_archive',
	'default'     => array(
		'font-size'      => '14px',
		'line-height'	 => '25px',
		'letter-spacing' => '0',
		'color'          => '#555555',
		'text-transform' => 'none',
	),
	'priority'    => 25,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '.archive-description p',
		),
	),
) );