<?php

// Add Section
Kirki::add_section( 'sitka_section_posts', array(
    'title'          => esc_html__( 'Post Settings', 'sitka' ),
    'priority'       => 29,
) );

// Add Settings

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_post_layout',
	'label'       => esc_html__( 'Default Post Layout', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => 'layout1',
	'priority'    => 2,
	'choices'     => array(
		'layout1'   => get_template_directory_uri() . '/admin/admin-img/style1.png',
		'layout2'   => get_template_directory_uri() . '/admin/admin-img/style2.png',
		'layout3'   => get_template_directory_uri() . '/admin/admin-img/style3.png',
		'layout4'   => get_template_directory_uri() . '/admin/admin-img/style4.png',
		'layout5'   => get_template_directory_uri() . '/admin/admin-img/style5.png',
	),
));

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_post_layout_layout1_title',
	'label'       => esc_html__( 'Default Post (Style 1) Title Position', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => 'above',
	'priority'    => 3,
	'choices'     => array(
		'above'   => esc_html__( 'Above Featured Image', 'sitka' ),
		'below'   => esc_html__( 'Below Featured Image', 'sitka' ),
	),
	'active_callback' => array(
        array(
            'setting'  => 'sitka_post_layout',
            'value'    => 'layout1',
            'operator' => '=='
        )
    )
));

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_video_post_layout',
	'label'       => esc_html__( 'Default Video Post Layout', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => 'layout1',
	'priority'    => 4,
	'choices'     => array(
		'layout1'   => get_template_directory_uri() . '/admin/admin-img/video-style1.png',
		'layout5'   => get_template_directory_uri() . '/admin/admin-img/video-style2.png',
		'video-layout3'   => get_template_directory_uri() . '/admin/admin-img/video-style3.png',
	),
));

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_gallery_post_layout',
	'label'       => esc_html__( 'Default Gallery Post Layout', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => 'layout1',
	'priority'    => 5,
	'choices'     => array(
		'layout1'   => get_template_directory_uri() . '/admin/admin-img/gallery-style1.png',
		'layout5'   => get_template_directory_uri() . '/admin/admin-img/gallery-style2.png',
	),
));

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_post_content_layout',
	'label'       => esc_html__( 'Default Post Content Layout', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => 'isSidebar',
	'priority'    => 6,
	'choices'     => array(
		'isSidebar'   => get_template_directory_uri() . '/admin/admin-img/isSidebar.png',
		'isNarrow isFullwidth'   => get_template_directory_uri() . '/admin/admin-img/isNarrow.png',
		'isFullwidth'   => get_template_directory_uri() . '/admin/admin-img/isFullwidth.png',
	),
));

/* Hide / Show */
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_post_show_image',
	'label'       => esc_html__( 'Featured Image', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => 'display',
	'priority'    => 75,
	'choices'     => array(
		'display'   => esc_html__( 'Display Featured Image', 'sitka' ),
		'no_display'   => esc_html__( 'Hide Featured Image', 'sitka' ),
		'ho_display'   => esc_html__( 'Hide Featured Image only on single post pages', 'sitka' ),
	),
));

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_post_show_post_meta',
	'label'       => esc_html__( 'Post Meta', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => 'single_display',
	'priority'    => 77,
	'choices'     => array(
		'display'   => esc_html__( 'Display Post Meta', 'sitka' ),
		'single_display'   => esc_html__( 'Display Post Meta only on single post pages', 'sitka' ),
		'no_display'   => esc_html__( 'Hide Post Meta', 'sitka' ),
	),
));
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_post_show_footer_social',
	'label'       => esc_html__( 'Post Footer Social Buttons', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => 'display',
	'priority'    => 79,
	'choices'     => array(
		'display'   => esc_html__( 'Display Post Footer Share Buttons', 'sitka' ),
		'single_display'   => esc_html__( 'Display Post Footer Share Buttons only on single post pages', 'sitka' ),
		'no_display'   => esc_html__( 'Hide Post Footer Share Buttons', 'sitka' ),
	),
));

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_category',
	'label'       => esc_html__( 'Display Post Category', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 80,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_title',
	'label'       => esc_html__( 'Display Post Title', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 84,
) );


Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_date',
	'label'       => esc_html__( 'Display Post Date', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 88,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_author',
	'label'       => esc_html__( 'Display Post Author Name', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 92,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_author_avatar',
	'label'       => esc_html__( 'Display Post Author Avatar', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 93,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_meta_share',
	'label'       => esc_html__( 'Display Post Meta Share Buttons', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 97,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_leavereply',
	'label'       => esc_html__( 'Display Comment Box', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 100,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_author_box',
	'label'       => esc_html__( 'Display Author Box', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 112,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_related',
	'label'       => esc_html__( 'Display Related Posts', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 116,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_related_date',
	'label'       => esc_html__( 'Display Related Posts Date', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 117,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_show_pagination',
	'label'       => esc_html__( 'Display Post Pagination', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 118,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_grid_show_cat',
	'label'       => esc_html__( 'Grid Post: Display Category', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 130,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_grid_show_date',
	'label'       => esc_html__( 'Grid Post: Display Date', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 132,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_list_show_cat',
	'label'       => esc_html__( 'List Post: Display Category', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 138,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_list_show_date',
	'label'       => esc_html__( 'List Post: Display Date', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 140,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_post_list_show_author',
	'label'       => esc_html__( 'List Post: Display Author', 'sitka' ),
	'section'     => 'sitka_section_posts',
	'default'     => '1',
	'priority'    => 142,
) );