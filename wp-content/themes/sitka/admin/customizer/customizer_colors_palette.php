<?php

// Add Section
Kirki::add_section( 'sitka_section_colors_palette', array(
    'title'          => esc_html__( 'Gutenberg/Editor Palette', 'sitka' ),
    'priority'       => 255,
	'panel'			 => 'sitka_panel_colors',
	'description'	 => esc_html__('Create your palette that will be used in Gutenberg blocks', 'sitka' ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_colors_palette_default_toggle',
	'label'       => esc_html__( 'Use a Premade Palette?', 'sitka' ),
	'description'	 => esc_html__('If you toggle this off, the default WordPress palette will be used', 'sitka' ),
	'section'     => 'sitka_section_colors_palette',
	'default'     => '1',
	'priority'    => 5,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'palette',
	'settings'    => 'sitka_colors_palette_default',
	'label'       => esc_html__( 'Select Premade Color Palette', 'sitka' ),
	'section'     => 'sitka_section_colors_palette',
	'default'     => 'palette1',
	'priority'    => 10,
	'choices'     => array(
		'palette1' => array(
			'#f78a74',
			'#ffd6cb',
			'#000000',
			'#1f2025',
			'#999',
			'#b5b5b5',
			'#f4f4f4',
			'#ffffff',
		),
		'palette2' => array(
			'#e8f1ef',
			'#3e555a',
			'#000000',
			'#999',
			'#b5b5b5',
			'#f4f4f4',
			'#ffffff',
		),
		'palette3' => array(
			'#d1b099',
			'#f7eee9',
			'#000000',
			'#999',
			'#b5b5b5',
			'#f4f4f4',
			'#ffffff',
		),
		'palette4' => array(
			'#53c3b5',
			'#000000',
			'#999',
			'#b5b5b5',
			'#f4f4f4',
			'#ffffff',
		),
	),
	'active_callback' => array(
        array(
            'setting'  => 'sitka_colors_palette_default_toggle',
            'value'    => '1',
            'operator' => '=='
        )
    )
) );


Kirki::add_field( 'sitka_config', array(
	'type'        => 'repeater',
	'label'       => esc_html__( 'Add Custom Colors to the Palette', 'sitka' ),
	'section'     => 'sitka_section_colors_palette',
	'priority'    => 11,
	'row_label' => array(
		'type'  => 'field',
		'value' => esc_html__('Color', 'sitka' ),
		'field' => 'color_name',
	),
	'button_label' => esc_html__('Add New Color', 'sitka' ),
	'settings'     => 'sitka_colors_palette_custom',
	'default'      => array(
		array(
			'color_name' => esc_html__( 'Custom Color', 'sitka' ),
			'color_slug' => esc_html__( 'custom-color', 'sitka' ),
			'color_code'  => '',
		),
	),
	'fields' => array(
		'color_name' => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Color Name', 'sitka' ),
			'description' => esc_html__( 'This will be the label for your color', 'sitka' ),
			'default'     => '',
		),
		'color_slug' => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Color Slug', 'sitka' ),
			'description' => esc_html__( '*REQUIRED - This will be the color slug', 'sitka' ),
			'default'     => '',
		),
		'color_code' => array(
			'type'        => 'color',
			'label'       => esc_html__( 'Color', 'sitka' ),
			'description' => esc_html__( '*REQUIRED - Select the color', 'sitka' ),
			'default'     => '',
		),
	),
	'active_callback' => array(
        array(
            'setting'  => 'sitka_colors_palette_default_toggle',
            'value'    => '1',
            'operator' => '=='
        )
    )
) );