<?php

// Add Section
Kirki::add_section( 'sitka_section_excerpt', array(
    'title'          => esc_html__( 'Excerpt/Summary Settings', 'sitka' ),
    'priority'       => 41,
	'description'	 => esc_html__( 'An excerpt is a brief intro to your post\'s text. The "Excerpt" option automatically pulls the first # of words chosen from your post. The "Read More Tag" option lets you choose on a post-by-post basis the length of the excerpt by using the "More" block within the post edit screen.', 'sitka' )
) );

// Add settings
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_excerpt_classic_type',
	'label'       => esc_html__( 'Classic Post Summary Type', 'sitka' ),
	'section'     => 'sitka_section_excerpt',
	'default'     => 'excerpt',
	'priority'    => 10,
	'choices'     => array(
		'excerpt'   => esc_html__( 'Excerpt', 'sitka' ),
		'readmore'   => esc_html__( 'Read More Tag', 'sitka' ),
	),
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_excerpt_classic_length',
	'label'       => esc_html__( 'Classic Post Excerpt Length', 'sitka' ),
	'section'     => 'sitka_section_excerpt',
	'default'     => 40,
	'priority'    => 11,
	'choices'     => array(
		'min'  => 1,
		'max'  => 500,
		'step' => 1,
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_excerpt_classic_type',
            'value'    => 'excerpt',
            'operator' => '=='
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_home_grid_excerpt_length',
	'label'       => esc_html__( 'Grid Post Excerpt Length', 'sitka' ),
	'section'     => 'sitka_section_excerpt',
	'default'     => 18,
	'priority'    => 12,
	'choices'     => array(
		'min'  => 1,
		'max'  => 500,
		'step' => 1,
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_home_list_excerpt_length',
	'label'       => esc_html__( 'List Post Excerpt Length', 'sitka' ),
	'section'     => 'sitka_section_excerpt',
	'default'     => 18,
	'priority'    => 16,
	'choices'     => array(
		'min'  => 1,
		'max'  => 500,
		'step' => 1,
	),
) );