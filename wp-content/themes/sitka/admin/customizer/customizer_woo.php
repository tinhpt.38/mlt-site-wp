<?php

// Add Section
Kirki::add_section( 'sitka_section_woo', array(
    'title'          => esc_html__( 'Sitka WooCommerce Settings', 'sitka' ),
    'priority'       => 51,
) );

// Add Settings
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'sitka_woo_layout',
	'label'       => esc_html__( 'WooCommerce Layout', 'sitka' ),
	'section'     => 'sitka_section_woo',
	'default'     => 'isFullwidth',
	'priority'    => 2,
	'choices'     => array(
		'isFullwidth' => esc_html__( 'Full-width', 'sitka' ),
		'isSidebar'   => esc_html__( 'Sidebar', 'sitka' ),
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_woo_cart',
	'label'       => esc_html__( 'Display Shop Cart in Header?', 'sitka' ),
	'section'     => 'sitka_section_woo',
	'default'     => '1',
	'priority'    => 3,
) );

Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_woo_cart_color',
    'label'       => esc_html__( 'Shop Cart Icon Colors', 'sitka' ),
    'section'     => 'sitka_section_woo',
    'priority'    => 6,
    'choices'     => array(
        'cart'    => esc_html__( 'Cart Icon', 'sitka' ),
        'bubble'   => esc_html__( 'Cart Bubble BG', 'sitka' ),
		'bubble_text'   => esc_html__( 'Cart Bubble Number', 'sitka' ),
    ),
    'default'     => array(
        'cart'    => '#000000',
        'bubble'   => '#f78a74',
		'bubble_text'   => '#ffffff',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'cart',
		  'element'       => '.cart-contents:before',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'bubble',
		  'element'       => '.cart-contents .sp-count',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'bubble_text',
		  'element'       => '.cart-contents .sp-count',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );