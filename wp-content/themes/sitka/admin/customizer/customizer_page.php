<?php

// Add Section
Kirki::add_section( 'sitka_section_page', array(
    'title'          => esc_html__( 'Page Settings', 'sitka' ),
    'priority'       => 31,
) );

// Add settings
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_page_layout',
	'label'       => esc_html__( 'Default Page Layout', 'sitka' ),
	'section'     => 'sitka_section_page',
	'default'     => 'layout1',
	'priority'    => 3,
	'choices'     => array(
		'layout1'   => get_template_directory_uri() . '/admin/admin-img/style1.png',
		'layout2'   => get_template_directory_uri() . '/admin/admin-img/page-style2.png',
		'layout3'   => get_template_directory_uri() . '/admin/admin-img/page-style3.png',
	),
));

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_page_content_layout',
	'label'       => esc_html__( 'Default Page Content Layout', 'sitka' ),
	'section'     => 'sitka_section_page',
	'default'     => 'isSidebar',
	'priority'    => 5,
	'choices'     => array(
		'isSidebar'   => get_template_directory_uri() . '/admin/admin-img/isSidebar.png',
		'isNarrow isFullwidth'   => get_template_directory_uri() . '/admin/admin-img/isNarrow.png',
		'isFullwidth'   => get_template_directory_uri() . '/admin/admin-img/isFullwidth.png',
	),
));

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_page_show_title',
	'label'       => esc_html__( 'Display Page Title', 'sitka' ),
	'section'     => 'sitka_section_page',
	'default'     => '1',
	'priority'    => 80,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_page_show_social',
	'label'       => esc_html__( 'Display Page Share Buttons', 'sitka' ),
	'section'     => 'sitka_section_page',
	'default'     => '1',
	'priority'    => 82,
) );