<?php

// Add Section
Kirki::add_section( 'sitka_section_header', array(
    'title'          => esc_html__( 'Header & Logo Settings', 'sitka' ),
    'priority'       => 25,
) );

// Add Settings
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_header_logo_type',
	'label'       => esc_html__( 'Select Logo Type', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => 'logo_image',
	'priority'    => 3,
	'choices'     => array(
		'logo_image'   => esc_html__( 'Image Logo', 'sitka' ),
		'logo_text'   => esc_html__( 'Text Logo', 'sitka' ),
	),
));
Kirki::add_field( 'sitka_config', array(
	'type'        => 'image',
	'settings'    => 'sitka_header_logo_image',
	'label'       => esc_html__( 'Upload Image Logo', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => get_template_directory_uri() . '/img/logo.png',
	'priority'    => 5,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_logo_type',
            'value'    => 'logo_image',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'image',
	'settings'    => 'sitka_header_logo_image_white',
	'label'       => esc_html__( 'Upload Image Logo (White version)', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => get_template_directory_uri() . '/img/logo-white.png',
	'description' => esc_html__( 'Upload a white version of your logo so it stands out on full-screen Featured Areas w/ the "Extended Overlay" option enabled.', 'sitka' ),
	'priority'    => 6,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_logo_type',
            'value'    => 'logo_image',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_header_logo_text',
	'label'       => esc_html__( 'Set Text Logo', 'sitka' ),
	'description' => esc_html__( 'Change the text of your text logo via Customizer > Site Identity > Site Title', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => array(
		'font-family'    => 'Poppins',
		'variant'        => '600',
		'font-size'      => '34px',
		'letter-spacing' => '0px',
		'color'          => '#000000',
		'text-transform' => 'uppercase',
	),
	'priority'    => 8,
	'output'      => array(
		array(
			'element' => '.text-logo, .sticky-wrapper.sticky #header.header-white .text-logo',
		),
	),
	'transport' => 'auto',
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_logo_type',
            'value'    => 'logo_text',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_header_logo_text_mobile',
	'label'       => esc_html__( 'Mobile Text Logo Size', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => array(
		'font-size'      => '24px',
		'letter-spacing' => '0px',
	),
	'priority'    => 9,
	'output'      => array(
		array(
			'element' => '.text-logo, .sticky-wrapper.sticky #header.header-white .text-logo',
			'media_query' => '@media (max-width:768px)'
		),
	),
	'transport' => 'auto',
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_logo_type',
            'value'    => 'logo_text',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_header_logo_text_name',
	'label'    => esc_html__( 'Logo Text', 'sitka' ),
	'section'  => 'sitka_section_header',
	'priority' => 10,
	'default'     => esc_html__( 'Sitka', 'sitka' ),
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_logo_type',
            'value'    => 'logo_text',
            'operator' => '=='
        )
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_header_layout',
	'label'       => esc_html__( 'Select Header Layout', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => 'layout1',
	'priority'    => 12,
	'choices'     => array(
		'layout1'   => esc_html__( 'Header Layout 1', 'sitka' ),
		'layout2'   => esc_html__( 'Header Layout 2', 'sitka' ),
		'layout3'   => esc_html__( 'Header Layout 3', 'sitka' ),
		'layout4'   => esc_html__( 'Header Layout 4', 'sitka' ),
	),
));

Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout1_height',
	'label'       => esc_html__( 'Header Layout 1 Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 90px', 'sitka' ),
	'default'     => 90,
	'choices'     => array(
		'min'  => 30,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#header.layout1',
			'property' => 'height',
			'units' => 'px'
		),
		array(
			'element' => '.feat-area.fullscreen .feat-item',
			'property' => 'height',
			'value_pattern' => 'calc(100vh - $px)'
		),
		array(
			'element' => '.feat-area.split-slider .feat-split',
			'property' => 'height',
			'value_pattern' => 'calc(100vh - $px)'
		),
		array(
			'element' => '.split-header',
			'property' => 'height',
			'value_pattern' => 'calc(100vh - $px)'
		),
		array(
			'element' => '#header.layout1 #nav-wrapper .menu > li > a',
			'property' => 'line-height',
			'units' => 'px'
		),
	),
	'priority'    => 16,
	'transport' => 'refresh',
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout1',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout1_height_sticky',
	'label'       => esc_html__( 'Header Layout 1 Scrolling Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 70px', 'sitka' ),
	'default'     => 70,
	'choices'     => array(
		'min'  => 30,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.sticky-wrapper.sticky #header.layout1',
			'property' => 'height',
			'units' => 'px'
		),
		array(
			'element' => '.sticky-wrapper.sticky #header.layout1 #nav-wrapper .menu > li > a',
			'property' => 'line-height',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 16,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout1',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout1_logo_height',
	'label'       => esc_html__( 'Logo Max Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 45px', 'sitka' ),
	'default'     => 45,
	'choices'     => array(
		'min'  => 0,
		'max'  => 2000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#header.layout1 #logo img',
			'property' => 'max-height',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 18,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout1',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout1_logo_height_sticky',
	'label'       => esc_html__( 'Scrolling Logo Max Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 40px', 'sitka' ),
	'default'     => 40,
	'choices'     => array(
		'min'  => 0,
		'max'  => 2000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.sticky-wrapper.sticky #header.layout1 #logo img',
			'property' => 'max-height',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 20,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout1',
            'operator' => '=='
        )
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout2_height',
	'label'       => esc_html__( 'Header Layout 2 Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 110px', 'sitka' ),
	'default'     => 110,
	'choices'     => array(
		'min'  => 30,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#header.layout2',
			'property' => 'height',
			'units' => 'px'
		),
		array(
			'element' => '.feat-area.fullscreen .feat-item',
			'property' => 'height',
			'value_pattern' => 'calc(100vh - $px)'
		),
		array(
			'element' => '.feat-area.split-slider .feat-split',
			'property' => 'height',
			'value_pattern' => 'calc(100vh - $px)'
		),
		array(
			'element' => '.split-header',
			'property' => 'height',
			'value_pattern' => 'calc(100vh - $px)'
		),
	),
	'priority'    => 24,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout2',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout2_height_sticky',
	'label'       => esc_html__( 'Header Layout 2 Scrolling Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 70px', 'sitka' ),
	'default'     => 70,
	'choices'     => array(
		'min'  => 30,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.sticky-wrapper.sticky #header.layout2',
			'property' => 'height',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 27,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout2',
            'operator' => '=='
        )
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout2_logo_height',
	'label'       => esc_html__( 'Logo Max Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 45px', 'sitka' ),
	'default'     => 45,
	'choices'     => array(
		'min'  => 0,
		'max'  => 2000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#header.layout2 #logo img',
			'property' => 'max-height',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 31,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout2',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout2_logo_height_sticky',
	'label'       => esc_html__( 'Scrolling Logo Max Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 40px', 'sitka' ),
	'default'     => 40,
	'choices'     => array(
		'min'  => 0,
		'max'  => 2000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.sticky-wrapper.sticky #header.layout2 #logo img',
			'property' => 'max-height',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 34,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout2',
            'operator' => '=='
        )
    )
) );

/* Header layout 3 */
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout3_height',
	'label'       => esc_html__( 'Header Layout 3 - Top Bar Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 58px', 'sitka' ),
	'default'     => 58,
	'choices'     => array(
		'min'  => 30,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#top-bar.layout3',
			'property' => 'height',
			'units' => 'px'
		),
		array(
			'element' => '#top-bar.layout3 #nav-wrapper .menu > li > a',
			'property' => 'line-height',
			'units' => 'px'
		),
		array(
			'element' => '#top-bar.layout3 .top-misc',
			'property' => 'line-height',
			'units' => 'px'
		),
	),
	'priority'    => 38,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout3',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout3_logo_width',
	'label'       => esc_html__( 'Logo Max Width', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 1140px', 'sitka' ),
	'default'     => 1140,
	'choices'     => array(
		'min'  => 0,
		'max'  => 1140,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#header.layout3 #logo img, #header.layout4 #logo img',
			'property' => 'max-width',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 55,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_header_layout',
            'value'    => array('layout3','layout4'),
            'operator' => 'contains'
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout3_logo_top_padding',
	'label'       => esc_html__( 'Header Layout 3 - Logo Padding Top', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 55px', 'sitka' ),
	'default'     => 55,
	'choices'     => array(
		'min'  => 0,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#header.layout3 #logo',
			'property' => 'padding-top',
			'units' => 'px'
		),
	),
	'priority'    => 40,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout3',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout3_logo_bottom_padding',
	'label'       => esc_html__( 'Header Layout 3 - Logo Padding Bottom', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 55px', 'sitka' ),
	'default'     => 55,
	'choices'     => array(
		'min'  => 0,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#header.layout3 #logo',
			'property' => 'padding-bottom',
			'units' => 'px'
		),
	),
	'priority'    => 50,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout3',
            'operator' => '=='
        )
    )
) );

/* Header layout 4 */
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'sitka_header_layout4_menu_width',
	'label'       => esc_html__( 'Header Layout 4 - Menu Bar Width', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => 'fullwidth',
	'priority'    => 39,
	'choices'     => array(
		'fullwidth'   => esc_html__( 'Full-width', 'sitka' ),
		'content-width' => esc_html__( 'Content Width', 'sitka' ),
	),
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout4',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout4_height',
	'label'       => esc_html__( 'Header Layout 4 - Menu Bar Height', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 58px', 'sitka' ),
	'default'     => 58,
	'transport' => 'auto',
	'choices'     => array(
		'min'  => 0,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#top-bar.layout4',
			'property' => 'height',
			'units' => 'px'
		),
		array(
			'element' => '#top-bar.layout4 #nav-wrapper .menu > li > a',
			'value_pattern' => 'calc($px - 1px)',
			'property' => 'line-height',
		),
		array(
			'element' => '#top-bar.layout4 .top-misc',
			'property' => 'line-height',
			'units' => 'px'
		),
	),
	'priority'    => 40,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout4',
            'operator' => '=='
        )
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout4_logo_top_padding',
	'label'       => esc_html__( 'Header Layout 4 - Logo Padding Top', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 45px', 'sitka' ),
	'default'     => 45,
	'transport' => 'auto',
	'choices'     => array(
		'min'  => 0,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#header.layout4 #logo',
			'property' => 'padding-top',
			'units' => 'px'
		),
	),
	'priority'    => 42,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout4',
            'operator' => '=='
        )
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_layout4_logo_bottom_padding',
	'label'       => esc_html__( 'Header Layout 4 - Logo Padding Bottom', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 45px', 'sitka' ),
	'default'     => 45,
	'transport' => 'auto',
	'choices'     => array(
		'min'  => 0,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#header.layout4 #logo',
			'property' => 'padding-bottom',
			'units' => 'px'
		),
	),
	'priority'    => 43,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_header_layout',
            'value'    => 'layout4',
            'operator' => '=='
        )
    )
) );

/* Hide / Show */
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_header_show_social',
	'label'       => esc_html__( 'Display Header Social Icons', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => '1',
	'priority'    => 80,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_header_show_search',
	'label'       => esc_html__( 'Display Header Search', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => '1',
	'priority'    => 84,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_header_show_menu_label',
	'label'       => esc_html__( 'Display Burger-Menu Label', 'sitka' ),
	'section'     => 'sitka_section_header',
	'default'     => '1',
	'priority'    => 88,
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_header_menu_label',
	'label'    => esc_html__( 'Burger-Menu Label Text', 'sitka' ),
	'section'  => 'sitka_section_header',
	'default'  => esc_html__( 'Menu', 'sitka' ),
	'priority' => 92,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_header_mobile_logo_max_width',
	'label'       => esc_html__( 'Mobile Logo Max Width', 'sitka' ),
	'section'     => 'sitka_section_header',
	'description' => esc_html__( 'Default: 200px', 'sitka' ),
	'default'     => 200,
	'choices'     => array(
		'min'  => 0,
		'max'  => 480,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '#mobile-menu.mobile-header .normal-logo img',
			'property' => 'max-width',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 100,

) );