<?php

// Add Section
Kirki::add_section( 'sitka_section_colors_pagination', array(
    'title'          => esc_html__( 'Pagination', 'sitka' ),
    'priority'       => 240,
	'panel'			 => 'sitka_panel_colors'
) );

/*Footer */
Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_pagi_active_bg',
    'label'       => esc_html__( 'Active Pagination Number', 'sitka' ),
    'section'     => 'sitka_section_colors_pagination',
    'priority'    => 4,
    'choices'     => array(
        'bg'    => esc_html__( 'Background', 'sitka' ),
        'text'   => esc_html__( 'Text Color', 'sitka' ),
    ),
    'default'     => array(
        'bg'    => '#f78a74',
        'text'   => '#ffffff',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'bg',
		  'element'       => '.sitka-pagination .page-numbers.current',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'text',
		  'element'       => '.sitka-pagination .page-numbers.current',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_pagi_number',
    'label'       => esc_html__( 'Pagination Numbers', 'sitka' ),
    'section'     => 'sitka_section_colors_pagination',
    'priority'    => 8,
    'choices'     => array(
        'text'    => esc_html__( 'Text Color', 'sitka' ),
        'hover'   => esc_html__( 'Text Hover Color', 'sitka' ),
    ),
    'default'     => array(
        'text'    => '#000000',
        'hover'   => '#f78a74',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'text',
		  'element'       => '.sitka-pagination .page-numbers',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'hover',
		  'element'       => '.sitka-pagination a.page-numbers:hover',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );


Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_pagi_nextprev',
	'label'       => esc_html__( 'Pagination Next/Prev Text', 'sitka' ),
	'section'     => 'sitka_section_colors_pagination',
	'default'     => array(
		'font-size'      => '14px',
		'letter-spacing' => '3px',
		'color'			 => '#000000',
		'text-transform' => 'uppercase',
	),
	'priority'    => 12,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '.sitka-pagination .page-numbers.prev, .sitka-pagination .page-numbers.next',
		),
	),
) );