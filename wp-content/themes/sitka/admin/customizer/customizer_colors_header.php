<?php

// Add Section
Kirki::add_section( 'sitka_section_colors_header', array(
    'title'          => esc_html__( 'Header', 'sitka' ),
    'priority'       => 200,
	'panel'			 => 'sitka_panel_colors'
) );

/*Header*/
Kirki::add_field( 'sitka_config', array(
	'type'        => 'background',
	'settings'    => 'sitka_colors_header_bg',
	'label'       => esc_html__( 'Header Background', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'priority' => 5,
	'default'     => array(
		'background-color'      => '#ffffff',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element' => '#header, .sticky-wrapper.sticky #header.header-white, #mobile-menu',
		),
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_header_shadow',
	'label'       => esc_html__( 'Header Box Shadow Color', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'priority' => 10,
	'default'     => 'rgba(0,0,0,.07)',
	'choices'     => array(
		'alpha' => true,
	),
	'output'    => array(
		array(
		  'element'       => '#header:not(.menu-bar-layout), #mobile-menu',
		  'property'      => 'box-shadow',
		  'value_pattern' => '0 0 shadow_size $',
		  'pattern_replace' => array(
				'shadow_size'    => 'sitka_colors_header_shadow_size',
			),
		),
	),
	'transport' => 'refresh'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_colors_header_shadow_size',
	'label'       => esc_html__( 'Header Box Shadow Size', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'description' => esc_html__( 'Default: 28px', 'sitka' ),
	'default'     => 28,
	'choices'     => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'      => array(
		array(
		  'element'       => '#header:not(.menu-bar-layout), #mobile-menu',
		  'property'      => 'box-shadow',
		  'value_pattern' => '0 0 $px shadow_color',
		  'pattern_replace' => array(
				'shadow_color'    => 'sitka_colors_header_shadow',
			),
		),
	),
	'transport' => 'auto',
	'priority'    => 15,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'background',
	'settings'    => 'sitka_colors_topbar_bg',
	'label'       => esc_html__( 'Top Bar/Menu Bar Background', 'sitka' ),
	'description' => esc_html__( 'Applies to Header layout 3 and 4', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'priority' => 16,
	'default'     => array(
		'background-color'      => '#ffffff',
		'background-image'      => '',
		'background-repeat'     => 'repeat',
		'background-position'   => 'center center',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
	),
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element' => '#top-bar',
		),
	),
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_topbar_shadow',
	'label'       => esc_html__( 'Top Bar Box Shadow Color', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'priority' => 17,
	'default'     => 'rgba(0,0,0,.11)',
	'choices'     => array(
		'alpha' => true,
	),
	'output'    => array(
		array(
		  'element'       => '#top-bar',
		  'property'      => 'box-shadow',
		  'value_pattern' => '0 0 topbar_shadow_size $',
		  'pattern_replace' => array(
				'topbar_shadow_size'    => 'sitka_colors_topbar_shadow_size',
			),
		),
	),
	'transport' => 'refresh'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_colors_topbar_shadow_size',
	'label'       => esc_html__( 'Top Bar Box Shadow Size', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'description' => esc_html__( 'Default: 12px', 'sitka' ),
	'default'     => 12,
	'choices'     => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'      => array(
		array(
		  'element'       => '#top-bar',
		  'property'      => 'box-shadow',
		  'value_pattern' => '0 0 $px topbar_shadow_color',
		  'pattern_replace' => array(
				'topbar_shadow_color'    => 'sitka_colors_topbar_shadow',
			),
		),
	),
	'transport' => 'auto',
	'priority'    => 18,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_header_border',
	'label'       => esc_html__( 'Header Layout 3 - Border Color', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'default'     => 'rgba(232,232,232, 1)',
	'choices'     => array(
		'alpha' => true,
	),
	'priority'    => 19,
	'output'    => array(
		array(
			'element'         => '#header.layout3',
			'property'        => 'border-color',
		),
	),
	'transport'	  => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_header4_border',
	'label'       => esc_html__( 'Header Layout 4 - Menu Border Color', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'default'     => '#e8e8e8',
	'choices'     => array(
		'alpha' => true,
	),
	'priority'    => 22,
	'output'    => array(
		array(
			'element'         => '#top-bar.layout4',
			'property'        => 'border-color',
		),
	),
	'transport'	  => 'auto'
) );


Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_header_menu_link',
    'label'       => esc_html__( 'Menu: Link Color', 'sitka' ),
    'section'     => 'sitka_section_colors_header',
    'priority'    => 25,
    'choices'     => array(
        'link'    => esc_html__( 'Color', 'sitka' ),
        'hover'   => esc_html__( 'Hover', 'sitka' ),
		'arrow'   => esc_html__( 'Dropdown Arrow', 'sitka' ),
    ),
    'default'     => array(
        'link'    => '#000000',
        'hover'   => '#999999',
		'arrow'   => '#aaaaaa',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'link',
		  'element'       => '#nav-wrapper .menu li a, .sticky-wrapper.sticky #header.header-white #nav-wrapper .menu li a',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'hover',
		  'element'       => '#nav-wrapper .menu li a:hover, .sticky-wrapper.sticky #header.header-white #nav-wrapper .menu li a:hover',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'arrow',
		  'element'       => '#nav-wrapper .menu li.menu-item-has-children > a:after, .sticky-wrapper.sticky #header.header-white #nav-wrapper .menu li.menu-item-has-children > a:after',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_header_menu_link_text',
	'label'       => esc_html__( 'Menu: Typography', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'default'     => array(
		'font-size'      => '13px',
		'letter-spacing' => '1.5px',
		'text-transform' => 'uppercase',
	),
	'priority'    => 30,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '#nav-wrapper .menu li a',
		),
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_colors_header_menu_link_spacing',
	'label'       => esc_html__( 'Menu Link Spacing', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'description' => esc_html__( 'Default: 30px', 'sitka' ),
	'default'     => 30,
	'priority'    => 35,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'      => array(
		array(
		  'element'       => '#nav-wrapper .menu li',
		  'property'      => 'margin',
		  'value_pattern' => '0 $px',
		),
		array(
		  'element'       => '#top-bar #nav-wrapper .menu > li',
		  'property'      => 'margin',
		  'value_pattern' => '0 $px 0 0',
		),
		array(
		  'element'       => '#top-bar.layout4 #nav-wrapper .menu > li',
		  'property'      => 'margin',
		  'value_pattern' => '0 $px',
		),
	),
) );

Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_header_menu_dropdown',
    'label'       => esc_html__( 'Menu: Dropdown Box', 'sitka' ),
    'section'     => 'sitka_section_colors_header',
    'priority'    => 40,
    'choices'     => array(
		'border_top'	=> esc_html__( 'Top Border Color', 'sitka' ),
        'background'    => esc_html__( 'Background Color', 'sitka' ),
		'background_hover'    => esc_html__( 'Background Hover Color', 'sitka' ),
        'border'  		=> esc_html__( 'Border Color', 'sitka' ),
		'border_hover'  => esc_html__( 'Border Hover Color', 'sitka' ),
		'text'  		=> esc_html__( 'Text Color', 'sitka' ),
		'text_hover'  => esc_html__( 'Text Hover Color', 'sitka' ),
    ),
    'default'     => array(
		'border_top'    => '#f78a74',
        'background'    => '#ffffff',
		'background_hover'    => '#f5f5f5',
        'border'   => '#eeeeee',
		'border_hover'    => '#e2e2e2',
		'text'   => '#000000',
		'text_hover'    => '#444444',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'border_top',
		  'element'       => '#nav-wrapper .menu .sub-menu, #nav-wrapper .menu .children',
		  'property'      => 'border-color',
		),
		array(
		  'choice'		  => 'background',
		  'element'       => '#nav-wrapper .menu .sub-menu, #nav-wrapper .menu .children',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'background_hover',
		  'element'       => '#nav-wrapper ul.menu ul a:hover, #nav-wrapper .menu ul ul a:hover',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'border',
		  'element'       => '#nav-wrapper ul.menu ul a, #nav-wrapper .menu ul ul a',
		  'property'      => 'border-color',
		),
		array(
		  'choice'		  => 'border_hover',
		  'element'       => '#nav-wrapper ul.menu ul a:hover, #nav-wrapper .menu ul ul a:hover',
		  'property'      => 'border-color',
		),
		array(
		  'choice'		  => 'text',
		  'element'       => '#nav-wrapper ul.menu ul a, #nav-wrapper .menu ul ul a',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'text_hover',
		  'element'       => '#nav-wrapper ul.menu ul a:hover, #nav-wrapper .menu ul ul a:hover',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_header_menu_dropdown_text',
	'label'       => esc_html__( 'Menu: Typography', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'default'     => array(
		'font-size'      => '11px',
		'letter-spacing' => '1.5px',
		'text-transform' => 'uppercase',
	),
	'priority'    => 45,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '#nav-wrapper ul.menu ul a, #nav-wrapper .menu ul ul a',
		),
	),
) );

Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_header_social',
    'label'       => esc_html__( 'Header Social Icons', 'sitka' ),
    'section'     => 'sitka_section_colors_header',
    'priority'    => 50,
    'choices'     => array(
        'color'    => esc_html__( 'Color', 'sitka' ),
        'hover'   => esc_html__( 'Hover', 'sitka' ),
    ),
    'default'     => array(
        'color'    => '#000000',
        'hover'   => '#999999',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'color',
		  'element'       => '.header-social a',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'hover',
		  'element'       => '.header-social a:hover',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_header_search',
    'label'       => esc_html__( 'Header Search Icon', 'sitka' ),
    'section'     => 'sitka_section_colors_header',
    'priority'    => 55,
    'choices'     => array(
        'icon'    => esc_html__( 'Search Icon Color', 'sitka' ),
        'icon_hover'   => esc_html__( 'Search Icon Hover Color', 'sitka' ),
		'separator'   => esc_html__( 'Search Separator Line', 'sitka' ),
    ),
    'default'     => array(
        'icon'    => '#000000',
        'icon_hover'   => '#999999',
		'separator'   => '#dddddd',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'icon',
		  'element'       => '.toggle-search-box',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'icon_hover',
		  'element'       => '.toggle-search-box:hover',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'separator',
		  'element'       => '.header-search-wrap',
		  'property'      => 'border-color',
		),
	),
	'transport' => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_colors_header_icon_size',
	'label'       => esc_html__( 'Header Icons Size', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'description' => esc_html__( 'Default: 16px', 'sitka' ),
	'default'     => 16,
	'choices'     => array(
		'min'  => 8,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.header-social a, .toggle-search-box, .cart-contents:before, .cart-contents',
			'property' => 'font-size',
			'units' => 'px'
		),
	),
	'transport' => 'auto',
	'priority'    => 60,
) );

Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_header_search_overlay',
    'label'       => esc_html__( 'Full-screen Search Overlay', 'sitka' ),
    'section'     => 'sitka_section_colors_header',
    'priority'    => 65,
    'choices'     => array(
        'bg'    => esc_html__( 'Search Overlay Background', 'sitka' ),
        'text'   => esc_html__( 'Search Text', 'sitka' ),
		'close'   => esc_html__( 'Search Close Icon', 'sitka' ),
    ),
    'default'     => array(
        'bg'    => 'rgba(255,255,255, 0.95)',
        'text'   => '#000000',
		'close'   => '#000000',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'bg',
		  'element'       => '#sitka-search-overlay.open',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'text',
		  'element'       => '#sitka-search-overlay input[type="text"], #sitka-search-overlay ::placeholder',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'close',
		  'element'       => '#sitka-search-overlay .close',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_header_burger',
	'label'       => esc_html__( 'Burger Menu Icon Color', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'default'     => '#000000',
	'choices'     => array(
		'alpha' => true,
	),
	'priority'    => 70,
	'output'    => array(
		array(
			'element'         => '.menu-toggle',
			'property'        => 'color',
		),
	),
	'transport'	  => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_header_burger_label',
	'label'       => esc_html__( 'Burger Menu Label Color', 'sitka' ),
	'section'     => 'sitka_section_colors_header',
	'default'     => '#000000',
	'choices'     => array(
		'alpha' => true,
	),
	'priority'    => 75,
	'output'    => array(
		array(
			'element'         => '.menu-icon span',
			'property'        => 'color',
		),
	),
	'transport'	  => 'auto'
) );