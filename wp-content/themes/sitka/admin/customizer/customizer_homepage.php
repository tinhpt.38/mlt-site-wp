<?php

// Add Section
Kirki::add_section( 'sitka_section_homepage', array(
    'title'          => esc_html__( 'Homepage Settings', 'sitka' ),
    'priority'       => 27,
	'description'	 => esc_html__( 'Use these settings to modify your homepage if it is set to display your latest posts (Settings > Reading > Homepage displays > Latest Posts).', 'sitka' )
) );

/* Featured area */
Kirki::add_section( 'sitka_section_homepage_feat', array(
    'title'          => esc_html__( 'Featured Area Settings', 'sitka' ),
    'priority'       => 5,
	'section'	     => 'sitka_section_homepage'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_feat_enable',
	'label'       => esc_html__( 'Enable Featured Area?', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '0',
	'priority'    => 2,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_home_feat_layout',
	'label'       => esc_html__( 'Featured Area Layout', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => 'classic',
	'priority'    => 8,
	'choices'     => array(
		'classic'   => get_template_directory_uri() . '/admin/admin-img/classic-slider.png',
		'fullscreen'   => get_template_directory_uri() . '/admin/admin-img/fullscreen.png',
		'split-fullscreen'   => get_template_directory_uri() . '/admin/admin-img/fullscreen-split.png',
		'carousel'   => get_template_directory_uri() . '/admin/admin-img/carousel.png',
		'carousel-center'   => get_template_directory_uri() . '/admin/admin-img/carousel-center.png',
		'split-slider'   => get_template_directory_uri() . '/admin/admin-img/split-slider.png',
		'static-classic'   => get_template_directory_uri() . '/admin/admin-img/classic-slider.png',
		'static-fullscreen'   => get_template_directory_uri() . '/admin/admin-img/fullscreen.png',
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_home_feat_slider_width',
	'label'       => esc_html__( 'Slider Width', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => 'full-width',
	'priority'    => 8,
	'choices'     => array(
		'full-width'   => esc_html__( 'Full-width', 'sitka' ),
		'content-width'   => esc_html__( 'Content width', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('classic','carousel','static-classic'),
            'operator' => 'contains'
        ),
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_home_feat_classic_type',
	'label'       => esc_html__( 'Classic Slider Overlay Style', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => 'style1',
	'priority'    => 9,
	'choices'     => array(
		'style1'   => esc_html__( 'Style 1', 'sitka' ),
		'style2'   => esc_html__( 'Style 2', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('classic'),
            'operator' => 'contains'
        ),
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'slider',
	'settings'    => 'sitka_home_feat_carousel_columns',
	'label'       => esc_html__( 'Carousel columns', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => 3,
	'priority'    => 10,
	'choices'     => array(
		'min'  => '2',
		'max'  => '4',
		'step' => '1',
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('carousel'),
            'operator' => 'contains'
        ),
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_feat_carousel_spacing',
	'label'       => esc_html__( 'Add space between carousel items?', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '0',
	'priority'    => 12,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('carousel', 'carousel-center'),
            'operator' => 'contains'
        ),
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_feat_fullscreen_overlay',
	'label'       => esc_html__( 'Extend Slider Over Header Bar', 'sitka' ),
	'description'	 => esc_html__( 'If you enable this setting make sure you have uploaded a white version of your logo via Header & Logo Settings.', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '0',
	'priority'    => 36,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('fullscreen', 'split-fullscreen','static-fullscreen'),
            'operator' => 'contains'
        ),
		array(
			'setting'  => 'sitka_header_layout',
			'operator' => '!==',
			'value'    => 'layout3',
		),
		array(
			'setting'  => 'sitka_header_layout',
			'operator' => '!==',
			'value'    => 'layout4',
		),
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_home_feat_content_type',
	'label'       => esc_html__( 'Featured Area Content Type', 'sitka' ),
	'description'	 => esc_html__( 'Select how you want to get the Featured Area content', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => 'latest',
	'priority'    => 38,
	'choices'     => array(
		'latest'   => esc_html__( 'Latest Posts', 'sitka' ),
		'category'   => esc_html__( 'Posts by Categories', 'sitka' ),
		'tag'   => esc_html__( 'Posts by Tags', 'sitka' ),
		'specific'   => esc_html__( 'Specific Posts/Pages', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );


// Category
Kirki::add_field( 'sitka_config', array(
	'type'        => 'select',
	'settings'    => 'sitka_home_feat_content_category',
	'label'       => esc_attr__( 'Select Category/Categories', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => 'option-1',
	'priority'    => 40,
	'multiple'    => 100,
	'choices'     => Kirki_Helper::get_terms( array('taxonomy' => 'category') ),
	'active_callback' => array(
        array(
            'setting'  => 'sitka_home_feat_content_type',
            'value'    => 'category',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );
// Tags
Kirki::add_field( 'sitka_config', array(
	'type'        => 'select',
	'settings'    => 'sitka_home_feat_content_tag',
	'label'       => esc_attr__( 'Select Tag', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => 'option-1',
	'priority'    => 41,
	'multiple'    => 100,
	'choices'     => Kirki_Helper::get_terms( array('taxonomy' => 'post_tag') ),
	'active_callback' => array(
        array(
            'setting'  => 'sitka_home_feat_content_type',
            'value'    => 'tag',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );
// Specific posts
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_feat_content_posts',
	'label'    => esc_html__( 'Enter Post/Page IDs', 'sitka' ),
	'description'	 => esc_html__( 'Separate IDs with a comma.', 'sitka' ),
	'section'  => 'sitka_section_homepage_feat',
	'priority' => 43,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_content_type',
            'value'    => 'specific',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    ),
) );

// Feat amount
Kirki::add_field( 'sitka_config', array(
	'type'        => 'slider',
	'settings'    => 'sitka_home_feat_amount',
	'label'       => esc_html__( 'Number of Featured Items', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => 3,
	'priority'    => 45,
	'choices'     => array(
		'min'  => '1',
		'max'  => '10',
		'step' => '1',
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_feat_exclude',
	'label'       => esc_html__( 'Exclude Featured Posts from Post Feed?', 'sitka' ),
	'description'	 => esc_html__( 'If you enable this setting, the featured posts won\'t show up in the main post feed below', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '0',
	'priority'    => 46,
	'transport' => 'refresh',
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )

) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'custom',
	'settings'    => 'sitka_home_feat_exclude_note',
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '<span class="dashicons dashicons-info"></span> <strong>' . esc_html__( 'Note: Please publish, then manually refresh the page to see the change', 'sitka' ) . '</strong>',
	'priority'    => 48,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_home_feat_overlay',
	'label'       => esc_html__( 'Slider Transparent Overlay', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => 'rgba(0,0,0,0.2)',
	'priority'    => 70,
	'choices'	  => array(
		'alpha'		  => true,
	),
	'output'    => array(
		array(
			'element'         => '.feat-shadow',
			'property'        => 'background',
		),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('classic', 'fullscreen', 'split-fullscreen', 'static-classic', 'static-fullscreen'),
            'operator' => 'contains'
        ),
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    ),
	'transport'	  => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_feat_show_cat',
	'label'       => esc_attr__( 'Display Category', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '1',
	'priority'    => 72,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_feat_show_title',
	'label'       => esc_attr__( 'Display Title', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '1',
	'priority'    => 74,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_feat_show_date',
	'label'       => esc_attr__( 'Display Date', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '1',
	'priority'    => 76,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_feat_show_author_img',
	'label'       => esc_attr__( 'Display Author Avatar', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '1',
	'priority'    => 78,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_feat_show_author_name',
	'label'       => esc_attr__( 'Display Author Name', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'default'     => '1',
	'priority'    => 80,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-classic',
            'operator' => '!='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => 'static-fullscreen',
            'operator' => '!='
        ),
    )
) );

/* Static content */
Kirki::add_field( 'sitka_config', array(
	'type'        => 'repeater',
	'label'       => esc_html__( 'Custom Slides', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'description'	 => esc_html__( 'All fields are optional', 'sitka' ),
	'priority'    => 100,
	'row_label' => array(
		'type'  => 'field',
		'value' => esc_html__( 'Custom Slide', 'sitka' ),
		'field' => 'slide_title',
	),
	'button_label' => esc_html__('Add New Slide', 'sitka' ),
	'settings'     => 'sitka_home_feat_static_slides',
	'fields' => array(
		'slide_image' => array(
			'type'        => 'image',
			'label'       => esc_html__( 'Slide: Image', 'sitka' ),
			'default'     => '',
		),
		'slide_link'  => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Slide: Link', 'sitka' ),
			'default'     => '',
		),
		'slide_title'  => array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Slide: Title', 'sitka' ),
			'default'     => '',
		),
		'slide_subtitle'  => array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Slide: Sub-Title', 'sitka' ),
			'default'     => '',
		),
		'slide_content'  => array(
			'type'        => 'textarea',
			'label'       => esc_html__( 'Slide: Text', 'sitka' ),
			'default'     => '',
		),
		'slide_button_text'  => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Slide: Button Text', 'sitka' ),
			'default'     => '',
		),
		'slide_button_link'  => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Slide: Button Link', 'sitka' ),
			'default'     => '',
		),
		
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('static-classic', 'static-fullscreen'),
            'operator' => 'contains'
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_home_feat_static_classic_width',
	'label'       => esc_html__( 'Static Classic Slider: Content overall max width', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'description' => esc_html__( 'Default: 70%', 'sitka' ),
	'default'     => 70,
	'priority'    => 101,
	'choices'     => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.feat-area.static-slider.classic-slider .post-header',
			'property' => 'max-width',
			'units' => '%'
		),
	),
	'transport' => 'auto',
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('static-classic'),
            'operator' => 'contains'
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_home_feat_static_classic_text_width',
	'label'       => esc_html__( 'Static Classic Slider: Content text max width', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'description' => esc_html__( 'Default: 70%', 'sitka' ),
	'default'     => 70,
	'priority'    => 102,
	'choices'     => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.feat-area.static-slider.classic-slider .static-text',
			'property' => 'max-width',
			'units' => '%'
		),
	),
	'transport' => 'auto',
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('static-classic'),
            'operator' => 'contains'
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_home_feat_static_fullscreen_width',
	'label'       => esc_html__( 'Static Fullscreen Slider: Content overall max width', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'description' => esc_html__( 'Default: 70%', 'sitka' ),
	'default'     => 70,
	'priority'    => 103,
	'choices'     => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.feat-area.static-slider.fullscreen .post-header',
			'property' => 'max-width',
			'units' => '%'
		),
	),
	'transport' => 'auto',
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('static-fullscreen'),
            'operator' => 'contains'
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_home_feat_static_fullscreen_text_width',
	'label'       => esc_html__( 'Static Fullscreen Slider: Content text max width', 'sitka' ),
	'section'     => 'sitka_section_homepage_feat',
	'description' => esc_html__( 'Default: 70%', 'sitka' ),
	'default'     => 70,
	'priority'    => 104,
	'choices'     => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.feat-area.static-slider.fullscreen .static-text',
			'property' => 'max-width',
			'units' => '%'
		),
	),
	'transport' => 'auto',
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_feat_enable',
            'value'    => '1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_feat_layout',
            'value'    => array('static-fullscreen'),
            'operator' => 'contains'
        ),
    )
) );

/* Promo Boxes */
Kirki::add_section( 'sitka_section_homepage_promo', array(
    'title'          => esc_html__( 'Promo Boxes Settings', 'sitka' ),
    'priority'       => 100,
	'section'	     => 'sitka_section_homepage'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_promo_enable',
	'label'       => esc_html__( 'Enable Promo Boxes?', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'default'     => '0',
	'priority'    => 1,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_home_promo_layout',
	'label'       => esc_html__( 'Promo Box Layout', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'default'     => 'grid',
	'priority'    => 2,
	'choices'     => array(
		'grid'   => get_template_directory_uri() . '/admin/admin-img/promo-grid.png',
		'mixed'   => get_template_directory_uri() . '/admin/admin-img/promo-mixed.png',
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'slider',
	'settings'    => 'sitka_home_promo_amount',
	'label'       => esc_html__( 'Number of Promo Boxes', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'default'     => 1,
	'priority'    => 3,
	'choices'     => array(
		'min'  => '1',
		'max'  => '4',
		'step' => '1',
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_layout',
            'value'    => 'grid',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_home_promo_style',
	'label'       => esc_html__( 'Promo Box Style', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'default'     => 'style1',
	'priority'    => 4,
	'choices'     => array(
		'style1'   => esc_html__( 'Style 1', 'sitka' ),
		'style2'   => esc_html__( 'Style 2', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_home_promo_height',
	'label'       => esc_html__( 'Promo Box Height', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'description' => esc_html__( 'Default 240px', 'sitka' ),
	'default'     => 240,
	'choices'     => array(
		'min'  => 0,
		'max'  => 1200,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.promo-wrap.promo-grid',
			'property' => 'grid-auto-rows',
			'units' => 'px'
		),
		array(
			'element' => '.promo-wrap.promo-mixed',
			'property' => 'grid-auto-rows',
			'value_pattern' => '$px $px'
		),
	),
	'transport' => 'auto',
	'priority'    => 5,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )

) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_promo_border',
	'label'       => esc_html__( 'Display Inner Border', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'default'     => '1',
	'priority'    => 6,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );


Kirki::add_field( 'sitka_config', array(
	'type'        => 'image',
	'settings'    => 'sitka_home_promo_image1',
	'label'       => esc_html__( '1st Promo Box Image', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'priority'    => 10,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_promo_title1',
	'label'    => esc_html__( '1st Promo Box Title', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 12,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_promo_subtitle1',
	'label'    => esc_html__( '1st Promo Box Subtitle', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 14,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_style',
            'value'    => 'style1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    ),
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'link',
	'settings' => 'sitka_home_promo_url1',
	'label'    => esc_html__( '1st Promo Box URL', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 16,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'checkbox',
	'settings'    => 'sitka_home_promo_target1',
	'label'       => esc_html__( 'Open link in a new tab?', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'priority' => 18,
	'default'     => false,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'image',
	'settings'    => 'sitka_home_promo_image2',
	'label'       => esc_html__( '2nd Promo Box Image', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'priority'    => 20,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_promo_title2',
	'label'    => esc_html__( '2nd Promo Box Title', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 22,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_promo_subtitle2',
	'label'    => esc_html__( '2nd Promo Box Subtitle', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 24,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_style',
            'value'    => 'style1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    ),
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'link',
	'settings' => 'sitka_home_promo_url2',
	'label'    => esc_html__( '2nd Promo Box URL', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 26,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'checkbox',
	'settings'    => 'sitka_home_promo_target2',
	'label'       => esc_html__( 'Open link in a new tab?', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'priority' => 28,
	'default'     => false,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'image',
	'settings'    => 'sitka_home_promo_image3',
	'label'       => esc_html__( '3rd Promo Box Image', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'priority'    => 30,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_promo_title3',
	'label'    => esc_html__( '3rd Promo Box Title', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 32,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_promo_subtitle3',
	'label'    => esc_html__( '3rd Promo Box Subtitle', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 34,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_style',
            'value'    => 'style1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    ),
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'link',
	'settings' => 'sitka_home_promo_url3',
	'label'    => esc_html__( '3rd Promo Box URL', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 36,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'checkbox',
	'settings'    => 'sitka_home_promo_target3',
	'label'       => esc_html__( 'Open link in a new tab?', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'priority' => 38,
	'default'     => false,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'image',
	'settings'    => 'sitka_home_promo_image4',
	'label'       => esc_html__( '4th Promo Box Image', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'priority'    => 40,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_promo_title4',
	'label'    => esc_html__( '4th Promo Box Title', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 42,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_promo_subtitle4',
	'label'    => esc_html__( '4th Promo Box Subtitle', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 44,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_style',
            'value'    => 'style1',
            'operator' => '=='
        ),
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    ),
) );
Kirki::add_field( 'sitka_config', array(
	'type'     => 'link',
	'settings' => 'sitka_home_promo_url4',
	'label'    => esc_html__( '4rd Promo Box URL', 'sitka' ),
	'section'  => 'sitka_section_homepage_promo',
	'priority' => 46,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'checkbox',
	'settings'    => 'sitka_home_promo_target4',
	'label'       => esc_html__( 'Open link in a new tab?', 'sitka' ),
	'section'     => 'sitka_section_homepage_promo',
	'priority' => 48,
	'default'     => false,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_promo_enable',
            'value'    => '1',
            'operator' => '=='
        ),
    )
) );

/* General homepage settings */
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'sitka_home_sidebar',
	'label'       => esc_html__( 'Homepage Content Layout', 'sitka' ),
	'section'     => 'sitka_section_homepage',
	'default'     => 'isSidebar',
	'priority'    => 1,
	'choices'     => array(
		'isSidebar'   => esc_html__( 'Sidebar', 'sitka' ),
		'isFullwidth isNarrow' => esc_html__( 'Full-width', 'sitka' ),
	),
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_home_layout',
	'label'       => esc_html__( 'Homepage Layout', 'sitka' ),
	'section'     => 'sitka_section_homepage',
	'default'     => 'classic',
	'priority'    => 2,
	'choices'     => array(
		'classic'   => get_template_directory_uri() . '/admin/admin-img/classic.png',
		'classic_grid'   => get_template_directory_uri() . '/admin/admin-img/classic-grid.png',
		'grid'   => get_template_directory_uri() . '/admin/admin-img/grid.png',
		'classic_list'   => get_template_directory_uri() . '/admin/admin-img/classic-list.png',
		'list'   => get_template_directory_uri() . '/admin/admin-img/list.png',
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_home_layout_grid',
	'label'       => esc_html__( 'Select Grid Style', 'sitka' ),
	'section'     => 'sitka_section_homepage',
	'default'     => 'style3',
	'priority'    => 3,
	'choices'     => array(
		'style1'   => esc_html__( 'Grid Style 1', 'sitka' ),
		'style2'   => esc_html__( 'Grid Style 2', 'sitka' ),
		'style3'   => esc_html__( 'Grid Style 3', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_layout',
            'value'    => array('grid', 'classic_grid'),
            'operator' => 'contains'
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_home_layout_list',
	'label'       => esc_html__( 'Select List Style', 'sitka' ),
	'section'     => 'sitka_section_homepage',
	'default'     => 'style1',
	'priority'    => 7,
	'choices'     => array(
		'style1'   => esc_html__( 'List Style 1', 'sitka' ),
		'style2'   => esc_html__( 'List Style 2', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_layout',
            'value'    => array('list', 'classic_list'),
            'operator' => 'contains'
        )
    ),
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_layout_list_alternate',
	'label'       => esc_html__( 'Alternate List Posts?', 'sitka' ),
	'section'     => 'sitka_section_homepage',
	'default'     => '0',
	'priority'    => 8,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_layout',
            'value'    => array('list', 'classic_list'),
            'operator' => 'contains'
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_post_number_toggle',
	'label'       => esc_html__( 'Set a different amount of posts on 1st page?', 'sitka' ),
	'section'     => 'sitka_section_homepage',
	'default'     => '0',
	'priority'    => 27,
	'description'	 => esc_html__( 'Helpful if using "1st Classic + Grid" Homepage Layout to avoid uneven grid rows on page 2, 3, etc. of the post feed/blog. Otherwise, global post count set via Settings > Reading. ).', 'sitka' )
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_home_post_number',
	'label'       => esc_html__( 'Number of Posts on 1st Page', 'sitka' ),
	'section'     => 'sitka_section_homepage',
	'default'     => 6,
	'choices'     => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'priority'    => 29,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_home_post_number_toggle',
            'value'    => '1',
            'operator' => '=='
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_home_blog_heading',
	'label'       => esc_html__( 'Display Post Feed Heading?', 'sitka' ),
	'section'     => 'sitka_section_homepage',
	'default'     => '0',
	'priority'    => 31,
) );

Kirki::add_field( 'sitka_config', array(
	'type'     => 'text',
	'settings' => 'sitka_home_blog_heading_text',
	'label'    => esc_html__( 'Post Feed Heading Text', 'sitka' ),
	'section'  => 'sitka_section_homepage',
	'default'  => esc_html__( 'Latest Articles', 'sitka' ),
	'priority' => 33,
	'active_callback' => array(
        array(
            'setting'  => 'sitka_home_blog_heading',
            'value'    => '1',
            'operator' => '=='
        )
    )
) );