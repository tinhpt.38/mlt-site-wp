<?php

// Add Section
Kirki::add_section( 'sitka_section_archive', array(
    'title'          => esc_html__( 'Archive Settings', 'sitka' ),
    'priority'       => 33,
	'description'	 => esc_html__( 'Use these settings to modify the layout of your category, tag & author pages', 'sitka' )
) );

// Add Settings
Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'sitka_archive_sidebar',
	'label'       => esc_html__( 'Archive Content Layout', 'sitka' ),
	'section'     => 'sitka_section_archive',
	'default'     => 'isSidebar',
	'priority'    => 2,
	'choices'     => array(
		'isSidebar'   => esc_html__( 'Sidebar', 'sitka' ),
		'isFullwidth' => esc_html__( 'Full-width', 'sitka' ),
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_archive_layout',
	'label'       => esc_html__( 'Archive Layout', 'sitka' ),
	'section'     => 'sitka_section_archive',
	'default'     => 'classic',
	'priority'    => 4,
	'choices'     => array(
		'classic'   => get_template_directory_uri() . '/admin/admin-img/classic.png',
		'classic_grid'   => get_template_directory_uri() . '/admin/admin-img/classic-grid.png',
		'grid'   => get_template_directory_uri() . '/admin/admin-img/grid.png',
		'classic_list'   => get_template_directory_uri() . '/admin/admin-img/classic-list.png',
		'list'   => get_template_directory_uri() . '/admin/admin-img/list.png',
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_archive_layout_grid',
	'label'       => esc_html__( 'Select Grid Style', 'sitka' ),
	'section'     => 'sitka_section_archive',
	'default'     => 'style3',
	'priority'    => 8,
	'choices'     => array(
		'style1'   => esc_html__( 'Grid Style 1', 'sitka' ),
		'style2'   => esc_html__( 'Grid Style 2', 'sitka' ),
		'style3'   => esc_html__( 'Grid Style 3', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_archive_layout',
            'value'    => array('grid', 'classic_grid'),
            'operator' => 'contains'
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_archive_layout_list',
	'label'       => esc_html__( 'Select List Style', 'sitka' ),
	'section'     => 'sitka_section_archive',
	'default'     => 'style1',
	'priority'    => 10,
	'choices'     => array(
		'style1'   => esc_html__( 'List Style 1', 'sitka' ),
		'style2'   => esc_html__( 'List Style 2', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_archive_layout',
            'value'    => array('list', 'classic_list'),
            'operator' => 'contains'
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_archive_layout_list_alternate',
	'label'       => esc_html__( 'Alternate List Posts?', 'sitka' ),
	'section'     => 'sitka_section_archive',
	'default'     => '0',
	'priority'    => 12,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_archive_layout',
            'value'    => array('list', 'classic_list'),
            'operator' => 'contains'
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_archivebox_display_browsing',
	'label'       => esc_html__( 'Display Archive Header "Browsing" Text', 'sitka' ),
	'section'     => 'sitka_section_archive',
	'default'     => '1',
	'priority'    => 30,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_archivebox_display_title',
	'label'       => esc_html__( 'Display Archive Header Title', 'sitka' ),
	'section'     => 'sitka_section_archive',
	'default'     => '1',
	'priority'    => 32,
) );

/* Search settings */
Kirki::add_section( 'sitka_section_archive_search', array(
    'title'          => esc_html__( 'Search Settings', 'sitka' ),
    'priority'       => 5,
	'section'	     => 'sitka_section_archive'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio-image',
	'settings'    => 'sitka_search_layout',
	'label'       => esc_html__( 'Search Layout', 'sitka' ),
	'section'     => 'sitka_section_archive_search',
	'default'     => 'classic',
	'priority'    => 4,
	'choices'     => array(
		'classic'   => get_template_directory_uri() . '/admin/admin-img/classic.png',
		'classic_grid'   => get_template_directory_uri() . '/admin/admin-img/classic-grid.png',
		'grid'   => get_template_directory_uri() . '/admin/admin-img/grid.png',
		'classic_list'   => get_template_directory_uri() . '/admin/admin-img/classic-list.png',
		'list'   => get_template_directory_uri() . '/admin/admin-img/list.png',
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_search_layout_grid',
	'label'       => esc_html__( 'Select Grid Style', 'sitka' ),
	'section'     => 'sitka_section_archive_search',
	'default'     => 'style3',
	'priority'    => 8,
	'choices'     => array(
		'style1'   => esc_html__( 'Grid Style 1', 'sitka' ),
		'style2'   => esc_html__( 'Grid Style 2', 'sitka' ),
		'style3'   => esc_html__( 'Grid Style 3', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_search_layout',
            'value'    => array('grid', 'classic_grid'),
            'operator' => 'contains'
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'radio',
	'settings'    => 'sitka_search_layout_list',
	'label'       => esc_html__( 'Select List Style', 'sitka' ),
	'section'     => 'sitka_section_archive_search',
	'default'     => 'style1',
	'priority'    => 10,
	'choices'     => array(
		'style1'   => esc_html__( 'List Style 1', 'sitka' ),
		'style2'   => esc_html__( 'List Style 2', 'sitka' ),
	),
	'active_callback' => array(
		array(
            'setting'  => 'sitka_search_layout',
            'value'    => array('list', 'classic_list'),
            'operator' => 'contains'
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_search_layout_list_alternate',
	'label'       => esc_html__( 'Alternate List Posts?', 'sitka' ),
	'section'     => 'sitka_section_archive_search',
	'default'     => '0',
	'priority'    => 12,
	'active_callback' => array(
		array(
            'setting'  => 'sitka_search_layout',
            'value'    => array('list', 'classic_list'),
            'operator' => 'contains'
        )
    ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_searchbox_display_results',
	'label'       => esc_html__( 'Display Search Header "Search results" Text', 'sitka' ),
	'section'     => 'sitka_section_archive_search',
	'default'     => '1',
	'priority'    => 30,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'toggle',
	'settings'    => 'sitka_searchbox_display_title',
	'label'       => esc_html__( 'Display Search Header Title', 'sitka' ),
	'section'     => 'sitka_section_archive_search',
	'default'     => '1',
	'priority'    => 32,
) );