<?php

// Add Section
Kirki::add_section( 'sitka_section_colors_footer', array(
    'title'          => esc_html__( 'Footer', 'sitka' ),
    'priority'       => 205,
	'panel'			 => 'sitka_panel_colors'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'custom',
	'settings'    => 'panelfix',
	'section'     => 'sitka_section_colors_footer',
	'default'     => '<div class="panelfix"></div>',
	'priority'    => 1,
) );

// Instagram Footer Section
Kirki::add_section( 'sitka_section_colors_footer_instagram', array(
    'title'          => esc_html__( 'Instagram Footer Colors', 'sitka' ),
    'priority'       => 110,
	'section'		 => 'sitka_section_colors_footer'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_footer_insta_bg',
	'label'       => esc_html__( 'Instagram Footer Background', 'sitka' ),
	'section'     => 'sitka_section_colors_footer_instagram',
	'default'     => '#f4f4f4',
	'priority'    => 2,
	'output'    => array(
		array(
			'element'         => '#insta-footer',
			'property'        => 'background-color',
		),
	),
	'transport'	  => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_footer_insta_title',
	'label'       => esc_html__( 'Instagram Footer Title', 'sitka' ),
	'section'     => 'sitka_section_colors_footer_instagram',
	'default'     => array(
		'font-size'      => '30px',
		'letter-spacing' => '4px',
		'color'			 => '#000000',
		'text-transform' => 'uppercase',
	),
	'priority'    => 3,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '.insta-header h5, .insta-header h5 a',
		),
	),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_footer_insta_subtitle',
	'label'       => esc_html__( 'Instagram Footer Subtitle', 'sitka' ),
	'section'     => 'sitka_section_colors_footer_instagram',
	'default'     => array(
		'font-size'      => '12px',
		'letter-spacing' => '2px',
		'color'			 => '#555555',
		'text-transform' => 'uppercase',
	),
	'priority'    => 8,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '.insta-header span',
		),
	),
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_colors_footer_insta_padding_top',
	'label'       => esc_html__( 'Instagram Title Padding Top', 'sitka' ),
	'section'     => 'sitka_section_colors_footer_instagram',
	'description' => esc_html__( 'Default: 60px', 'sitka' ),
	'default'     => 60,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 0,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.insta-header',
			'property' => 'padding-top',
			'units' => 'px'
		),
	),
	'priority'    => 9,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_colors_footer_insta_padding_bottom',
	'label'       => esc_html__( 'Instagram Title Padding Bottom', 'sitka' ),
	'section'     => 'sitka_section_colors_footer_instagram',
	'description' => esc_html__( 'Default: 40px', 'sitka' ),
	'default'     => 40,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 0,
		'max'  => 1000,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element' => '.insta-header',
			'property' => 'padding-bottom',
			'units' => 'px'
		),
	),
	'priority'    => 9,
) );

/*Footer */
Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_footer_bg',
	'label'       => esc_html__( 'Footer Background', 'sitka' ),
	'section'     => 'sitka_section_colors_footer',
	'default'     => '#010101',
	'priority'    => 2,
	'output'    => array(
		array(
			'element'         => '#footer',
			'property'        => 'background-color',
		),
	),
	'transport'	  => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_colors_footer_social',
	'label'       => esc_html__( 'Footer Social Icons', 'sitka' ),
	'section'     => 'sitka_section_colors_footer',
	'default'     => '#010101',
	'priority'    => 4,
	'output'    => array(
		array(
			'element'         => '.footer-social a',
			'property'        => 'background-color',
		),
	),
	'transport'	  => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_footer_social',
    'label'       => esc_html__( 'Footer Social Icons', 'sitka' ),
    'section'     => 'sitka_section_colors_footer',
    'priority'    => 7,
    'choices'     => array(
        'color'    => esc_html__( 'Color', 'sitka' ),
        'hover'   => esc_html__( 'Hover', 'sitka' ),
    ),
    'default'     => array(
        'color'    => '#ffffff',
        'hover'   => '#f6836c',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'color',
		  'element'       => '.footer-social a',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'hover',
		  'element'       => '.footer-social a:hover',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );

Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_footer_text',
    'label'       => esc_html__( 'Footer Copyright Text Color', 'sitka' ),
    'section'     => 'sitka_section_colors_footer',
    'priority'    => 9,
    'choices'     => array(
        'color'    => esc_html__( 'Text Color', 'sitka' ),
        'link'   => esc_html__( 'Link Color', 'sitka' ),
		'link_hover'   => esc_html__( 'Link Hover Color', 'sitka' ),
    ),
    'default'     => array(
        'color'    => '#888888',
        'link'   => '#ffffff',
		'link_hover'   => '#f6836c',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'color',
		  'element'       => '.copy-text, .copy-text p',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'link',
		  'element'       => '.copy-text a',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'link_hover',
		  'element'       => '.copy-text a:hover',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_footer_text_typo',
	'label'       => esc_html__( 'Footer Copyright Text Typography', 'sitka' ),
	'section'     => 'sitka_section_colors_footer',
	'default'     => array(
		'font-size'      => '12px',
		'letter-spacing' => '0',
		'text-transform' => 'none',
	),
	'priority'    => 10,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '.copy-text, .copy-text p',
		),
	),
) );

Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_colors_footer_menu',
    'label'       => esc_html__( 'Footer Menu Colors', 'sitka' ),
    'section'     => 'sitka_section_colors_footer',
    'priority'    => 14,
    'choices'     => array(
        'link'    => esc_html__( 'Menu Color', 'sitka' ),
        'link_hover'   => esc_html__( 'Menu Hover Color', 'sitka' ),
		'separator'   => esc_html__( 'Separator Color', 'sitka' ),
    ),
    'default'     => array(
        'link'    => '#ffffff',
        'link_hover'   => '#f6836c',
		'separator'   => '#555555',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'link',
		  'element'       => '.footer-menu li a',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'link_hover',
		  'element'       => '.footer-menu li a:hover',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'separator',
		  'element'       => '.footer-menu li:after',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'typography',
	'settings'    => 'sitka_colors_footer_menu_text',
	'label'       => esc_html__( 'Footer Menu Typography', 'sitka' ),
	'section'     => 'sitka_section_colors_footer',
	'default'     => array(
		'font-size'      => '12px',
		'letter-spacing' => '2px',
		'text-transform' => 'uppercase',
	),
	'priority'    => 15,
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'		=> '.footer-menu li a',
		),
	),
) );