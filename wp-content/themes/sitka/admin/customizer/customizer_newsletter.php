<?php

// Add Section
Kirki::add_section( 'sitka_section_newsletter', array(
    'title'          => esc_html__( 'Mailchimp Newsletter Settings', 'sitka' ),
    'priority'       => 49,
) );

// Add Settings
Kirki::add_field( 'sitka_config', array(
  'settings' => 'sitka_newsletter_descrip',
  'section'  => 'sitka_section_newsletter',
  'type'     => 'custom',
  'priority'    => 1,
  'default'  => wp_kses( __( 'Please ensure you\'ve installed/activated the "MC4WP: MailChimp for WordPress" plugin. Check out the <a target="_blank" href="https://sitkatheme.com/documentation/#widgets_mailchimp">documentation</a> for configuration instructions.', 'sitka' ), array(
    'a' => array(
      'target' => array(),
      'href' => array(),
    ),
  ) ),
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'image',
	'settings'    => 'sitka_newsletter_image',
	'label'       => esc_html__( 'Newsletter Widget Image', 'sitka' ),
	'section'     => 'sitka_section_newsletter',
	'priority'    => 2,
) );
Kirki::add_field( 'sitka_config', array(
	'type'        => 'number',
	'settings'    => 'sitka_newsletter_image_height',
	'label'       => esc_html__( 'Newsletter Widget Image Height', 'sitka' ),
	'section'     => 'sitka_section_newsletter',
	'default'     => 150,
	'choices'     => array(
		'min'  => 1,
		'max'  => 1000,
		'step' => 1,
	),
	'priority'    => 3,
) );

Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_newsletter_bg_color',
	'label'       => esc_html__( 'Sidebar: Newsletter Widget BG', 'sitka' ),
	'section'     => 'sitka_section_newsletter',
	'default'     => '#ffffff',
	'priority'    => 4,
	'output'    => array(
		array(
			'element'         => '#sp-sidebar .widget.widget_mc4wp_form_widget',
			'property'        => 'background-color',
		),
	),
	'transport'	  => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_newsletter_heading_color',
    'label'       => esc_html__( 'Sidebar: Newsletter Widget Text Color', 'sitka' ),
    'section'     => 'sitka_section_newsletter',
    'priority'    => 6,
    'choices'     => array(
        'title'    => esc_html__( 'Newsletter Title Color', 'sitka' ),
        'text'   => esc_html__( 'Newsletter Text Color', 'sitka' ),
    ),
    'default'     => array(
        'title'    => '#000000',
        'text'   => '#383838',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'title',
		  'element'       => '#sp-sidebar .news-content h4',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'text',
		  'element'       => '#sp-sidebar .news-content p',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_newsletter_sidebar_button_color_bg',
    'label'       => esc_html__( 'Sidebar: Submit Button', 'sitka' ),
    'section'     => 'sitka_section_newsletter',
    'priority'    => 7,
    'choices'     => array(
        'color'    => esc_html__( 'BG Color', 'sitka' ),
        'hover'   => esc_html__( 'BG Hover Color', 'sitka' ),
		'text'    => esc_html__( 'Text Color', 'sitka' ),
        'hover_text'   => esc_html__( 'Text Hover Color', 'sitka' ),
    ),
    'default'     => array(
        'color'    => '#f78a74',
        'hover'   => '#111111',
		'text'   => '#ffffff',
		'hover_text'   => '#ffffff',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'color',
		  'element'       => '#sp-sidebar .form-wrap input[type=submit]',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'hover',
		  'element'       => '#sp-sidebar .form-wrap input[type=submit]:hover',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'text',
		  'element'       => '#sp-sidebar .form-wrap input[type=submit]',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'hover_text',
		  'element'       => '#sp-sidebar .form-wrap input[type=submit]:hover',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );



Kirki::add_field( 'sitka_config', array(
	'type'        => 'color',
	'settings'    => 'sitka_slider_newsletter_bg_color',
	'label'       => esc_html__( 'Under Featured Area: Newsletter Widget BG', 'sitka' ),
	'section'     => 'sitka_section_newsletter',
	'default'     => '#f4f4f4',
	'priority'    => 10,
	'output'    => array(
		array(
			'element'         => '.widget-slider .sitka-newsletter-wrap',
			'property'        => 'background-color',
		),
	),
	'transport'	  => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_slider_newsletter_heading_color',
    'label'       => esc_html__( 'Under Featured Area: Newsletter Widget Text Color', 'sitka' ),
    'section'     => 'sitka_section_newsletter',
    'priority'    => 12,
    'choices'     => array(
        'title'    => esc_html__( 'Newsletter Title Color', 'sitka' ),
        'text'   => esc_html__( 'Newsletter Text Color', 'sitka' ),
    ),
    'default'     => array(
        'title'    => '#000000',
        'text'   => '#383838',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'title',
		  'element'       => '.widget-slider .news-content h4',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'text',
		  'element'       => '.widget-slider .news-content p',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );
Kirki::add_field( 'sitka_config', array(
    'type'        => 'multicolor',
    'settings'    => 'sitka_slider_newsletter_button_color_bg',
    'label'       => esc_html__( 'Under Featured Area: Submit Button', 'sitka' ),
    'section'     => 'sitka_section_newsletter',
    'priority'    => 14,
    'choices'     => array(
        'color'    => esc_html__( 'BG Color', 'sitka' ),
        'hover'   => esc_html__( 'BG Hover Color', 'sitka' ),
		'text'    => esc_html__( 'Text Color', 'sitka' ),
        'hover_text'   => esc_html__( 'Text Hover Color', 'sitka' ),
    ),
    'default'     => array(
        'color'    => '#f78a74',
        'hover'   => '#111111',
		'text'   => '#ffffff',
		'hover_text'   => '#ffffff',
    ),
	'output'    => array(
		array(
		  'choice'		  => 'color',
		  'element'       => '.widget-slider .form-wrap input[type=submit]',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'hover',
		  'element'       => '.widget-slider .form-wrap input[type=submit]:hover',
		  'property'      => 'background-color',
		),
		array(
		  'choice'		  => 'text',
		  'element'       => '.widget-slider .form-wrap input[type=submit]',
		  'property'      => 'color',
		),
		array(
		  'choice'		  => 'hover_text',
		  'element'       => '.widget-slider .form-wrap input[type=submit]:hover',
		  'property'      => 'color',
		),
	),
	'transport' => 'auto'
) );