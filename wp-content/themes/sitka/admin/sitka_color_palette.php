<?php

/*
* Sitka Color Palettes for Gutenberg blocks
*/

function sitka_get_palette() {
	if(get_theme_mod('sitka_colors_palette_default_toggle', true)) {
		$get_palette = get_theme_mod('sitka_colors_palette_default', 'palette1');
	} else {
		$get_palette = 'wordpress-default';
	}
	
	return $get_palette;
	
}

// Set palette colors
function sitka_set_palette() {
	if(sitka_get_palette() == 'palette1') {
		$palette = array(
			array(
				'name' => esc_html__( 'Accent', 'sitka' ),
				'slug' => 'sitka-accent',
				'color'  => '#f78a74',
			),
			array(
				'name' => esc_html__( 'Light Accent', 'sitka' ),
				'slug' => 'sitka-light-accent',
				'color'  => '#ffd6cb',
			),
			array(
				'name' => esc_html__( 'Black', 'sitka' ),
				'slug' => 'sitka-black',
				'color'  => '#000000',
			),
			array(
				'name' => esc_html__( 'Ultra Dark Gray', 'sitka' ),
				'slug' => 'sitka-ultradark-gray',
				'color'  => '#1f2025',
			),
			array(
				'name' => esc_html__( 'Gray', 'sitka' ),
				'slug' => 'sitka-gray',
				'color'  => '#999999',
			),
			array(
				'name' => esc_html__( 'Light Gray', 'sitka' ),
				'slug' => 'sitka-light-gray',
				'color'  => '#b5b5b5',
			),
			array(
				'name' => esc_html__( 'Ultra Light Gray', 'sitka' ),
				'slug' => 'sitka-ultralight-gray',
				'color'  => '#f4f4f4',
			),
			array(
				'name' => esc_html__( 'White', 'sitka' ),
				'slug' => 'sitka-white',
				'color'  => '#ffffff',
			),
		);
	} elseif(sitka_get_palette() == 'palette2') {
		$palette = array(
			array(
				'name' => esc_html__( 'Accent', 'sitka' ),
				'slug' => 'sitka-food-accent',
				'color'  => '#3e555a',
			),
			array(
				'name' => esc_html__( 'Light Accent', 'sitka' ),
				'slug' => 'sitka-food-light-accent',
				'color'  => '#e8f1ef',
			),
			array(
				'name' => esc_html__( 'Black', 'sitka' ),
				'slug' => 'sitka-black',
				'color'  => '#000000',
			),
			array(
				'name' => esc_html__( 'Ultra Dark Gray', 'sitka' ),
				'slug' => 'sitka-ultradark-gray',
				'color'  => '#1f2025',
			),
			array(
				'name' => esc_html__( 'Gray', 'sitka' ),
				'slug' => 'sitka-gray',
				'color'  => '#999999',
			),
			array(
				'name' => esc_html__( 'Light Gray', 'sitka' ),
				'slug' => 'sitka-light-gray',
				'color'  => '#b5b5b5',
			),
			array(
				'name' => esc_html__( 'Ultra Light Gray', 'sitka' ),
				'slug' => 'sitka-ultralight-gray',
				'color'  => '#f4f4f4',
			),
			array(
				'name' => esc_html__( 'White', 'sitka' ),
				'slug' => 'sitka-white',
				'color'  => '#ffffff',
			),
		);
	} elseif(sitka_get_palette() == 'palette3') {
		$palette = array(
			array(
				'name' => esc_html__( 'Accent', 'sitka' ),
				'slug' => 'sitka-classic-accent',
				'color'  => '#d1b099',
			),
			array(
				'name' => esc_html__( 'Light Accent', 'sitka' ),
				'slug' => 'sitka-classic-light-accent',
				'color'  => '#f7eee9',
			),
			array(
				'name' => esc_html__( 'Black', 'sitka' ),
				'slug' => 'sitka-black',
				'color'  => '#000000',
			),
			array(
				'name' => esc_html__( 'Ultra Dark Gray', 'sitka' ),
				'slug' => 'sitka-ultradark-gray',
				'color'  => '#1f2025',
			),
			array(
				'name' => esc_html__( 'Gray', 'sitka' ),
				'slug' => 'sitka-gray',
				'color'  => '#999999',
			),
			array(
				'name' => esc_html__( 'Light Gray', 'sitka' ),
				'slug' => 'sitka-light-gray',
				'color'  => '#b5b5b5',
			),
			array(
				'name' => esc_html__( 'Ultra Light Gray', 'sitka' ),
				'slug' => 'sitka-ultralight-gray',
				'color'  => '#f4f4f4',
			),
			array(
				'name' => esc_html__( 'White', 'sitka' ),
				'slug' => 'sitka-white',
				'color'  => '#ffffff',
			),
		);
	} elseif(sitka_get_palette() == 'palette4') {
		$palette = array(
			array(
				'name' => esc_html__( 'Accent', 'sitka' ),
				'slug' => 'sitka-travel-accent',
				'color'  => '#53c3b5',
			),
			array(
				'name' => esc_html__( 'Black', 'sitka' ),
				'slug' => 'sitka-black',
				'color'  => '#000000',
			),
			array(
				'name' => esc_html__( 'Ultra Dark Gray', 'sitka' ),
				'slug' => 'sitka-ultradark-gray',
				'color'  => '#1f2025',
			),
			array(
				'name' => esc_html__( 'Gray', 'sitka' ),
				'slug' => 'sitka-gray',
				'color'  => '#999999',
			),
			array(
				'name' => esc_html__( 'Light Gray', 'sitka' ),
				'slug' => 'sitka-light-gray',
				'color'  => '#b5b5b5',
			),
			array(
				'name' => esc_html__( 'Ultra Light Gray', 'sitka' ),
				'slug' => 'sitka-ultralight-gray',
				'color'  => '#f4f4f4',
			),
			array(
				'name' => esc_html__( 'White', 'sitka' ),
				'slug' => 'sitka-white',
				'color'  => '#ffffff',
			),
		);
	} elseif(sitka_get_palette() == 'wordpress-default') {
		$palette = 'wordpress-default';
	}
	
	// Get custom colors
	if(get_theme_mod('sitka_colors_palette_custom')) {
		$custom_colors = get_theme_mod( 'sitka_colors_palette_custom' );
		
		foreach($custom_colors as $custom_color) {
			
			if($custom_color['color_code'] && $custom_color['color_slug']) {
				$palette[] = array(
					'name' => $custom_color['color_name'],
					'slug' => 'custom-' . $custom_color['color_slug'],
					'color' => $custom_color['color_code'],
				);
			}
		}
	}
	
	return $palette;
	
}

// Add palette theme support and CSS classes
if(sitka_get_palette() != 'wordpress-default') {
add_theme_support( 'editor-color-palette', sitka_set_palette() );
function sitka_palette_css() {
	
	$css = '';
	foreach(sitka_set_palette() as $color) {
	
		if(substr( $color['slug'], 0, 7 ) === "custom-") {
			$css .= '.has-text-color.has-'. $color['slug'] .'-color { color:'. $color['color'] .';}';
			$css .= '.has-'. $color['slug'] .'-background-color { background-color:'. $color['color'] .';}';
		}
		
	}
	if( !empty( $css ) ) {
        wp_add_inline_style( 'sitka-style', $css );
    }
	
}
add_action( 'wp_enqueue_scripts', 'sitka_palette_css' );
}
?>