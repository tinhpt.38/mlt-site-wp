<?php
function sitka_welcome_menu() {
	add_menu_page(
		esc_html__( 'Sitka Welcome', 'sitka' ),
		esc_html__( 'Sitka Theme', 'sitka' ),
		'manage_options',
		'sitka_welcome',
		'sitka_welcome_page_contents',
		'dashicons-sos',
		3
	);
}
add_action( 'admin_menu', 'sitka_welcome_menu' );

function sitka_welcome_style($hook) {
	if( $hook != 'toplevel_page_sitka_welcome' ) {
		 return;
	}
	wp_enqueue_style( 'sitka_custom_wp_admin_css', get_template_directory_uri() . '/admin/sitka-welcome.css', false, '1.0.0' );
}
add_action( 'admin_enqueue_scripts', 'sitka_welcome_style' );

/* Redirect on theme activation */
add_action( 'admin_init', 'sitka_theme_activation_redirect' );
function sitka_theme_activation_redirect() {
    global $pagenow;
	if (is_admin() && isset($_GET['activated']) && $pagenow == "themes.php") {
        wp_safe_redirect( admin_url( "admin.php?page=sitka_welcome" ) );
    }
}

function sitka_welcome_page_contents() {
	$sitka_theme = wp_get_theme();
	?>
	<div class="sitka-welcome">
		<h1>
			<?php esc_html_e( 'Welcome to Sitka', 'sitka' ); ?> <small><?php echo 'v' . $sitka_theme->version; ?></small>
		</h1>
		<p class="welcome-text"><?php esc_html_e( 'Thanks for entrusting us with your site! We hope you\'ll be thrilled with your new theme. Sitka is continually growing and expanding so keep an eye out for updates that include new features, layouts, and more!', 'sitka' ); ?></p>
		
		<hr>
		
		<h2 class="welcome-heading"><?php esc_html_e( 'Getting Started:', 'sitka' ); ?></h2>
		
		<div class="welcome-wrap">
			
			<div class="welcome-column">
				<h2><?php esc_html_e( '1. Install Plugins', 'sitka' ); ?></h2>
				<p><?php esc_html_e( 'Install & activate Sitka\'s required and recommended plugins to ensure it\'s functioning properly.', 'sitka' ); ?></p>
				<p><strong><?php esc_html_e( 'Go to Appearance > Install Plugins', 'sitka' ); ?></strong></p>
			</div>
			
			<div class="welcome-column">
				<h2><?php esc_html_e( '2. Import Demo (optional)', 'sitka' ); ?></h2>
				<p><?php esc_html_e( 'Import one of Sitka\'s many pre-built demos to get your site looking flashy in a snap!', 'sitka' ); ?></p>
				<p><strong><?php esc_html_e( 'Go to Appearance > Import Demo Data', 'sitka' ); ?></strong></p>
			</div>
			
			<div class="welcome-column">
				<h2><?php esc_html_e( '3. Start Customizing', 'sitka' ); ?></h2>
				<p><?php esc_html_e( 'Now you\'re ready to begin customizing the appearance of your site. Let\'s get started.', 'sitka' ); ?></p>
				<a href="<?php echo esc_url( admin_url( 'customize.php' ) ); ?>" class="button button-primary"><?php esc_html_e( 'Start Customizing', 'sitka' ); ?></a>
			</div>
			
		</div>
		
		<h2 class="welcome-heading"><?php esc_html_e( 'Need Help?', 'sitka' ); ?></h2>
		
		<div class="welcome-wrap support">
			
			<div class="welcome-column">
				<h2><?php esc_html_e( 'Documentation', 'sitka' ); ?></h2>
				<p><?php esc_html_e( 'Please be sure to reference Sitka\'s documentation article for important instructions on setting up and customizing your theme.', 'sitka' ); ?></p>
				<a href="https://sitkatheme.com/documentation/" target="_blank" class="button button-primary"><?php esc_html_e( 'Read Documentation', 'sitka' ); ?></a>
			</div>
			
			<div class="welcome-column">
				<h2><?php esc_html_e( 'Support Ticket', 'sitka' ); ?></h2>
				<p><?php esc_html_e( 'Got questions? We\'re here to help! Head on over to our support site and open up a new ticket.', 'sitka' ); ?></p>
				<a href="https://solopine.ticksy.com" target="_blank" class="button button-primary"><?php esc_html_e( 'Open a Ticket', 'sitka' ); ?></a>
			</div>
			
		</div>
		
	</div>
	<?php
}