<?php
/*
* Post Layout: Video Style 1
*/
?>
	
<div class="sp-container layout-fullwidth-image layout-video-style3 <?php echo esc_attr(sitka_get_content_layout_flex()); ?>">

<div class="video-bg-header">
	<div class="sp-container">
		<?php sitka_get_featured_video(); ?>
	</div>
</div>

<div id="sp-content">

	<div id="sp-main" class="<?php echo esc_attr(sitka_get_content_layout_flex()); ?>">
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			<?php sitka_get_post_header(); ?>
			
			<?php get_template_part('parts/post/post-content'); ?>
		
		<?php endwhile; ?>
		<?php endif; ?>
	
	</div>
	
<?php if(sitka_get_content_layout() == 'isSidebar') { get_sidebar(); } ?>