<?php 
/*
* Post Layout: Style 5
*/
?>
	
	<div class="sp-container layout-style5">
		
		<div class="bg-color-header">
			<div class="sp-container">
				<?php sitka_get_post_header(); ?>
			</div>
		</div>
		
		<div class="bg-color-img">
			<?php
				$get_post_format = sitka_get_post_layout();
				if($get_post_format['format'] == 'video') {
					sitka_get_featured_video();
				} elseif($get_post_format['format'] == 'gallery') {
					sitka_get_featured_gallery();
				} else {
					sitka_get_featured_image();
				}
			?>
		</div>
		
		<div id="sp-content">
		
			<div id="sp-main" class="<?php echo esc_attr(sitka_get_content_layout_flex()); ?>">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<?php get_template_part('parts/post/post-content'); ?>
				
				<?php endwhile; ?>
				<?php endif; ?>
			
			</div>
	
<?php if(sitka_get_content_layout() == 'isSidebar') { get_sidebar(); } ?>