<?php 

$orig_post = $post;
global $post;

$categories = get_the_category($post->ID);

if ($categories) {

	$category_ids = array();

	foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
	
	$args = array(
		'category__in'     => $category_ids,
		'post__not_in'     => array($post->ID),
		'posts_per_page'   => 3,
		'ignore_sticky_posts' => 1,
		'orderby' => 'rand'
	);

	$sitka_related_query = new wp_query( $args );
	if( $sitka_related_query->have_posts() ) { ?>
	
	<div class="related-wrap">
			
		<div class="block-heading-wrap">
			<h4 class="block-heading"><?php esc_html_e( 'Further Reading...', 'sitka' ); ?></h4>
		</div>
			
		<div class="related-posts">
		
			<?php while( $sitka_related_query->have_posts() ) {
			$sitka_related_query->the_post();?>
			
			<div class="related-item <?php if(!has_post_thumbnail()) : ?>no-image<?php endif; ?>">
				<?php if(has_post_thumbnail()) : ?><a class="related-link" href="<?php the_permalink(); ?>"></a><?php endif; ?>
				<?php if(has_post_thumbnail()) : ?>
					<?php the_post_thumbnail('sitka-grid-post-thumb'); ?>
				<?php endif; ?>
				<div class="related-overlay">
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<?php if(get_theme_mod('sitka_post_show_related_date', true)) : ?><span class="sp-date"><?php the_time( get_option('date_format') ); ?></span><?php endif; ?>
				</div>
				
			</div>

		<?php
		}
		echo '</div></div>';
	}
}
$post = $orig_post;
wp_reset_postdata();

?>