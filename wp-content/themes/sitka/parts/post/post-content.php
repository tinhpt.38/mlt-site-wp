<?php
	/* Post Meta */
	sitka_get_post_meta();
?>

<div class="post-entry">
	
	<?php sitka_get_post_teaser('content'); ?>
	
	<?php 
		if(is_single()) {
			the_content('', false);
		} else {
			$get_summary_type = get_theme_mod('sitka_excerpt_classic_type', 'excerpt');
			if($get_summary_type == 'readmore') {
				the_content('', false);
			} else {
				$get_excerpt_length = get_theme_mod('sitka_excerpt_classic_length', 40);
				echo sitka_excerpt($get_excerpt_length);
			}
		}
	?>
	
	<?php if(!is_single()) : ?>
	<div class="read-more-wrapper">
		<a href="<?php the_permalink(); ?>" class="read-more">
			<span class="more-text"><?php esc_html_e( 'Continue Reading', 'sitka' ); ?></span>
			<span class="more-line"></span>
		</a>
	</div>
	<?php endif; ?>
	
	<?php wp_link_pages(); ?>
	
	<?php if(is_single() && has_tag()) : ?>
		<div class="post-tags">
			<?php the_tags("",""); ?>
		</div>
	<?php endif; ?>
	
</div>

<?php 
	
	/* Post Footer Share */
	if(function_exists('sitka_get_social_share') && sitka_show_post_footer_share() == 'display') {
		sitka_get_social_share('footer');
	} elseif(function_exists('sitka_get_social_share') && sitka_show_post_footer_share() == 'single_display') {
		if(is_single()) {
			sitka_get_social_share('footer');
		}
	}

	/* Author box */
	if(sitka_post_show_author_box() == 'show' && is_single()) {
	get_template_part('parts/post/author-box');
	}

	/* Related Posts */
	if(sitka_post_show_related() == 'show' && is_single()) {
	get_template_part('parts/post/related-posts');
	}
	
	/* Comments */
	if ( is_single() && comments_open() ) {
		comments_template( '', true );
	}
	
	/* Post Pagination */
	if(sitka_post_show_pagination() == 'show' && is_single()) {
	get_template_part('parts/post/post-pagination');
	}
	
?>