<?php if(get_the_author_meta('description')) : ?>
<div class="about-author">
	<?php echo get_avatar( get_the_author_meta('email'), '110' ); ?>
	<span class="about-heading"><?php esc_html_e( 'About Author', 'sitka' ); ?></span>
	<h4><?php the_author_posts_link(); ?></h4>
	<p><?php the_author_meta('description'); ?></p>
	<div class="author-links">
		<?php if(function_exists('sitka_core_get_author_icons')) { sitka_core_get_author_icons(); } ?>
	</div>
</div>
<?php endif; ?>