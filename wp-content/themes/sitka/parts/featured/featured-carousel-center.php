<?php
/*
* Featured Area: Classic Slider Layout
*/
$feat_args = sitka_get_featured_content();
/* Get carousel spacing */
if(is_page() && class_exists('ACF')) {
	$carousel_spacing = get_field('sitka_acf_page_feat_carousel_spacing');
	$carousel_spacing = isset($carousel_spacing) ? $carousel_spacing : false;
} elseif(is_home()) {
	$carousel_spacing = get_theme_mod('sitka_home_feat_carousel_spacing', false);
} else {
	$carousel_spacing = false;
}
?>
<div class="sp-container">
<div id="featured-area" class="feat-area sitka-carousel carousel-slider-center feat-arrows <?php if($carousel_spacing) { echo "item-spacing"; } ?>">

	<?php
		
		if(is_home()) {
			$get_cat = get_theme_mod( 'sitka_home_feat_show_cat', true );
			$get_title = get_theme_mod( 'sitka_home_feat_show_title', true );
			$get_date = get_theme_mod( 'sitka_home_feat_show_date', true );
			$get_author_avatar = get_theme_mod( 'sitka_home_feat_show_author_img', true );
			$get_author_name = get_theme_mod( 'sitka_home_feat_show_author_name', true );
		} elseif(is_page() && class_exists('ACF')) {
			$get_cat = get_field( 'sitka_acf_page_feat_show_cat' );
			$get_title = get_field( 'sitka_acf_page_feat_show_title' );
			$get_date = get_field( 'sitka_acf_page_feat_show_date' );
			$get_author_avatar = get_field( 'sitka_acf_page_feat_show_author_img' );
			$get_author_name = get_field( 'sitka_acf_page_feat_show_author_name' );
		}
	
		$feat_query = new WP_Query( $feat_args );
		if ($feat_query->have_posts()) : while ($feat_query->have_posts()) : $feat_query->the_post();
		
		// Get featured image
		$feat_image = sitka_get_feat_image('sitka-fullwidth');
		
		// Get featured title
		$feat_title = sitka_get_feat_title();
	?>
	<div class="feat-item-wrapper">
		<div class="feat-item" style="background-image:url(<?php echo esc_url($feat_image); ?>);">
			<a href="<?php the_permalink(); ?>" class="feat-link"></a>
			<div class="feat-inner">
				
				<div class="feat-overlay">
					
					<div class="post-header">
						
						<?php if($get_cat == true) : ?>
						<div class="post-cats">
							<?php the_category(' <span>/</span> '); ?>
						</div>
						<?php endif; ?>
						<?php if($get_title) : ?>
							<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php echo esc_html($feat_title); ?></a></h2>
						<?php endif; ?>
						<?php if($get_date) : ?>
						<div class="date-wrap">
							<span class="sp-date"><?php the_time( get_option('date_format') ); ?></span>
						</div>
						<?php endif; ?>

					</div>
				</div>
				
			</div>
			
			<div class="feat-shadow"></div>
			
		</div>
	</div>
	<?php endwhile; wp_reset_postdata(); endif; ?>
	
</div>
</div>