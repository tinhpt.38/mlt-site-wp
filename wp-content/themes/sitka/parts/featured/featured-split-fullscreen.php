<?php
/*
* Featured Area: Split Fullscreen Slider Layout
*/
$feat_args = sitka_get_featured_content();
?>
<div class="feat-area fullscreen fullscreen-split">
	
	<?php
	
		if(is_home()) {
			$get_cat = get_theme_mod( 'sitka_home_feat_show_cat', true );
			$get_title = get_theme_mod( 'sitka_home_feat_show_title', true );
			$get_date = get_theme_mod( 'sitka_home_feat_show_date', true );
			$get_author_avatar = get_theme_mod( 'sitka_home_feat_show_author_img', true );
			$get_author_name = get_theme_mod( 'sitka_home_feat_show_author_name', true );
		} elseif(is_page() && class_exists('ACF')) {
			$get_cat = get_field( 'sitka_acf_page_feat_show_cat' );
			$get_title = get_field( 'sitka_acf_page_feat_show_title' );
			$get_date = get_field( 'sitka_acf_page_feat_show_date' );
			$get_author_avatar = get_field( 'sitka_acf_page_feat_show_author_img' );
			$get_author_name = get_field( 'sitka_acf_page_feat_show_author_name' );
		}
	
		$feat_query = new WP_Query( $feat_args );
		if ($feat_query->have_posts()) : while ($feat_query->have_posts()) : $feat_query->the_post();
		
		// Get featured image
		$feat_image = sitka_get_feat_image('sitka-fullscreen');
		
		// Get featured title
		$feat_title = sitka_get_feat_title();
		
	?>
	<div class="feat-split-wrapper">
		<div class="feat-item" style="background-image:url(<?php echo esc_url($feat_image); ?>);">
			<div class="feat-inner">
				<a href="<?php the_permalink(); ?>" class="feat-link"></a>
				<div class="feat-overlay">
					
					<div class="post-header">
						<?php if($get_cat == true) : ?>
						<div class="post-cats">
							<?php the_category(' <span>/</span> '); ?>
						</div>
						<?php endif; ?>
						<?php if($get_title) : ?>
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php echo esc_html($feat_title); ?></a></h2>
						<?php endif; ?>
						<?php if($get_date) : ?>
						<div class="date-wrap">
							<span class="sp-date"><?php the_time( get_option('date_format') ); ?></span>
						</div>
						<?php endif; ?>
					</div>
					
				</div>
				<?php if($get_author_avatar == false && $get_author_name == false) : else : ?>
				<div class="feat-author">
					<?php if($get_author_avatar) : ?>
					<?php echo get_avatar( get_the_author_meta('email'), '70' ); ?>
					<?php endif; ?>
					<?php if($get_author_name) : ?>
					<span class="by"><?php esc_html_e( 'by', 'sitka' ); ?></span> <?php the_author_posts_link(); ?>
					<?php endif; ?>
				</div>
				<?php endif; ?>
				<div class="feat-shadow"></div>
			</div>
		</div>
	</div>
	<?php endwhile; wp_reset_postdata(); endif; ?>

</div>