<?php
/*
* Featured Area: Static Fullscreen Slider Layout
*/

/* Get static slides */
if(is_page() && class_exists('ACF')) {
	$static_content = get_field('sitka_acf_page_feat_static_slides');
} else {
	$static_content = get_theme_mod('sitka_home_feat_static_slides');
}

?>
<div class="feat-area fullscreen static-slider fullscreen-one">
	
	<?php foreach($static_content as $feat_item) : ?>
	<div class="feat-item" <?php if($feat_item['slide_image']) : ?>style="background-image:url(<?php echo esc_url(wp_get_attachment_image_src( $feat_item['slide_image'], 'sitka-fullscreen' )[0]); ?>);"<?php endif; ?>>
		<div class="feat-inner">
			<?php if($feat_item['slide_link']) : ?><a href="<?php echo esc_url($feat_item['slide_link']); ?>" class="feat-link"></a><?php endif; ?>
			<div class="feat-overlay">
				<div class="sp-container">
				<div class="post-header">
					<?php if($feat_item['slide_subtitle']) : ?><p class="static-subtitle"><?php echo $feat_item['slide_subtitle']; ?></p><?php endif; ?>
					<?php if($feat_item['slide_title']) : ?><h2 class="static-title"><?php echo $feat_item['slide_title']; ?></h2><?php endif; ?>
					<?php if($feat_item['slide_content']) : ?><p class="static-text"><?php echo $feat_item['slide_content']; ?></p><?php endif; ?>
					<?php if($feat_item['slide_button_text']) : ?><a class="static-button" href="<?php echo esc_url($feat_item['slide_button_link']); ?>"><?php echo esc_html($feat_item['slide_button_text']); ?></a><?php endif; ?>
				</div>
				</div>
			</div>
			<div class="feat-shadow"></div>
		</div>
	</div>
	<?php endforeach; ?>
	
</div>