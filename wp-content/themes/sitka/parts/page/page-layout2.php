<?php 

/*
* Classic Page Template
*/

get_header(); 
?>
	
	<div class="sp-container page-style2 layout-fullwidth-image-overlay">
		
		<?php $get_thumb_url = get_the_post_thumbnail_url(get_the_ID(), 'sitka-fullscreen'); ?>
		<div class="fullwidth-image" style="background-image:url(<?php echo esc_url($get_thumb_url); ?>)">
			<div class="sp-container">
				<?php sitka_get_page_header(); ?>
				<div class="fullwidth-image-shadow"></div>
			</div>
		</div>
		
		<div id="sp-content">
			
			<?php
				if (class_exists('ACF')) {
					if(get_field('sitka_acf_page_promo_enable')) {
						get_template_part('parts/promo/promo');
					}
				}
			?>
			
			<?php
				if (class_exists('ACF')) {
					if(get_field('sitka_acf_page_show_widget') == 'show' && is_active_sidebar('sidebar-3')) { ?>
						<div class="widget-slider">
							<?php dynamic_sidebar( 'sidebar-3' ); ?>
						</div>
					<?php }
				}
			?>
			
			<div id="sp-main" class="<?php echo sitka_get_page_content_layout(); ?>">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<?php get_template_part('parts/page/page-content'); ?>
				
				<?php endwhile; ?>
				<?php endif; ?>
				
				<?php get_template_part('parts/page/page', 'blog'); ?>
			
			</div>
	
<?php if(sitka_get_page_content_layout() == 'isSidebar') { get_sidebar(); } ?>
<?php get_footer(); ?>