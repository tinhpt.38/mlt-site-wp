<?php 

/*
* Classic Page Template
*/

get_header(); 
?>
	
	<?php
	if (class_exists('ACF')) {
		if(get_field('sitka_acf_page_featured_enable') && !is_paged()) {
			get_template_part('parts/featured/featured', get_field('sitka_acf_page_feat_layout'));
		}
	}
	?>
	
	<div class="sp-container">
	
		<div id="sp-content">
			
			<?php
				if (class_exists('ACF')) {
					if(get_field('sitka_acf_page_promo_enable') && !is_paged()) {
						get_template_part('parts/promo/promo');
					}
				}
			?>
			
			<?php
				if (class_exists('ACF')) {
					if(get_field('sitka_acf_page_show_widget') == 'show' && is_active_sidebar('sidebar-3') && !is_paged()) { ?>
						<div class="widget-slider">
							<?php dynamic_sidebar( 'sidebar-3' ); ?>
						</div>
					<?php }
				}
			?>
			
			<div id="sp-main" class="<?php echo sitka_get_page_content_layout(); ?> page-style1">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<?php get_template_part('content', 'page'); ?>
				
				<?php endwhile; ?>
				<?php endif; ?>
				
				<?php get_template_part('parts/page/page', 'blog'); ?>
				
			</div>
	
<?php if(sitka_get_page_content_layout() == 'isSidebar') { get_sidebar(); } ?>
<?php get_footer(); ?>