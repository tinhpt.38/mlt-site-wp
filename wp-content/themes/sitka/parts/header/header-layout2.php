<header id="header" class="layout2 <?php echo esc_attr(sitka_need_white_logo()); ?>">
	
	<?php sitka_get_burger_menu(); ?>

	<div id="logo">
		<?php 
			/* Get logo */
			sitka_get_logo();

			/* Get white logo if needed */
			if(sitka_need_white_logo() == 'header-white') {
				sitka_get_logo_white();
			}
		?>
	</div>
	
	<div class="top-misc">
		<?php if(function_exists('sitka_get_social_icons') && get_theme_mod('sitka_header_show_social', true)) { sitka_get_social_icons('header'); } ?>
		<?php sitka_get_woo_cart(); ?>
		<?php sitka_get_search(); ?>
	</div>
	
</header>