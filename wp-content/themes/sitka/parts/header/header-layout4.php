<?php
/*
* Get menu width
*/
$menu_width = get_theme_mod('sitka_header_layout4_menu_width', 'fullwidth');
?>
<header id="header" class="layout4 menu-bar-layout <?php echo esc_attr(sitka_header_check_featured()); ?>">
	<div class="sp-container">
		<div id="logo">
			<?php if(function_exists('sitka_get_social_icons') && get_theme_mod('sitka_header_show_social', true)) { sitka_get_social_icons('header'); } ?>
			<?php sitka_get_logo(); ?>
			<div class="top-misc-layout4">
				<?php sitka_get_woo_cart(); ?>
				<?php sitka_get_search(); ?>
			</div>
		</div>
	</div>
</header>

<?php if($menu_width == 'content-width') : ?><div class="sp-container"><?php endif; ?>
<div id="top-bar" class="layout4">
	<div class="sp-container">
		<nav id="nav-wrapper">
			<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'main-menu', 'menu_class' => 'menu' ) ); ?>
		</nav>
		
		
	</div>
</div>
<?php if($menu_width == 'content-width') : ?></div><?php endif; ?>