<div id="top-bar" class="layout3">
	<div class="sp-container">
		<nav id="nav-wrapper">
			<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'main-menu', 'menu_class' => 'menu' ) ); ?>
		</nav>
		
		<div class="top-misc">
			<?php if(function_exists('sitka_get_social_icons') && get_theme_mod('sitka_header_show_social', true)) { sitka_get_social_icons('header'); } ?>
			<?php sitka_get_woo_cart(); ?>
			<?php sitka_get_search(); ?>
		</div>
	</div>
</div>

<header id="header" class="layout3 menu-bar-layout <?php echo esc_attr(sitka_header_check_featured()); ?>">
	
	<div id="logo">
		<?php sitka_get_logo(); ?>
	</div>

</header>