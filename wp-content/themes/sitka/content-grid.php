<article id="post-<?php the_ID(); ?>" class="grid-item <?php if(!has_post_thumbnail()) : ?>no-image<?php endif; ?>">
<div class="grid-inner">

	<?php if(has_post_thumbnail()) : ?>
	<div class="post-img">
		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('sitka-grid-post-thumb'); ?></a>
	</div>
	<?php endif; ?>
	
	<div class="post-header">
		
		<?php if(get_theme_mod('sitka_post_grid_show_cat', true)) : ?>
		<div class="post-cats">
			<?php the_category(' <span>/</span> '); ?>
		</div>
		<?php endif; ?>
		
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		
		<?php
		if (sitka_get_post_style() != 'grid-style2') {
			$get_excerpt_length = get_theme_mod('sitka_home_grid_excerpt_length', 18);
			echo sitka_excerpt($get_excerpt_length);
		}
		?>
		
		<?php if(get_theme_mod('sitka_post_grid_show_date', true)) : ?>
		<span class="sp-date updated published"><?php the_time( get_option('date_format') ); ?></span>
		<?php endif; ?>
		
	</div>
</div>	
</article>