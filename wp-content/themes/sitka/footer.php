	
		<!-- END SP-CONTENT -->
		</div>
	
	<!-- END SP-CONTAINER -->
	</div>
	
	<?php if(is_single()) { echo'</article>'; } ?>
	
<!-- END INNER WRAPPER -->
</div>

<?php if(get_theme_mod('sitka_core_footer_insta_enable')) : ?>
<div id="insta-footer">
	<div class="insta-header">
		<?php if(get_theme_mod('sitka_core_footer_insta_subtitle', esc_html__('Find me on Instagram', 'sitka'))) : ?><span><?php echo esc_html(get_theme_mod('sitka_core_footer_insta_subtitle', esc_html__('Find me on Instagram', 'sitka'))); ?></span><?php endif; ?>
		<?php if(get_theme_mod('sitka_core_footer_insta_title', esc_html__('Solo Pine', 'sitka'))) : ?>
			<h5><?php if(get_theme_mod('sitka_core_social_ic_ig')) : ?><a target="_blank" href="<?php echo esc_url(get_theme_mod('sitka_core_social_ic_ig')); ?>"><?php endif; ?><?php echo esc_html(get_theme_mod('sitka_core_footer_insta_title', esc_html__('Solo Pine', 'sitka'))); ?><?php if(get_theme_mod('sitka_core_social_ic_ig')) : ?></a><?php endif; ?></h5>
		<?php endif; ?>
	</div>
	<div class="insta-images">
		<?php echo do_shortcode('[instagram-feed num='.get_theme_mod('sitka_core_footer_insta_number',8).' cols='.get_theme_mod('sitka_core_footer_insta_number',8).' imagepadding=0 showheader=false showbutton=false showfollow=false disablemobile=true]'); ?>
	</div>
</div>
<?php endif; ?>

<footer id="footer">
	
	<?php if(get_theme_mod('sitka_footer_logo_enable', true)) : ?>
		
	<div id="footer-logo">
		<img src="<?php echo esc_url(get_theme_mod('sitka_footer_logo', get_template_directory_uri() . '/img/footer-logo.png')); ?>" alt="<?php esc_attr(bloginfo( 'name' )); ?>" />
	</div>
		
	<?php endif; ?>
	
	<div id="footer-copy" class="<?php if(!get_theme_mod('sitka_footer_logo_enable', true)) : ?>no-logo<?php endif; ?> <?php if(!has_nav_menu( 'footer-menu' )) : ?>no-menu<?php endif; ?>">
		
		<div class="copy-text"><?php echo wp_kses_post(get_theme_mod('sitka_footer_copyright_text', esc_html__('(C) 2019 - Solo Pine. All Rights Reserved.', 'sitka'))); ?></div>
	
		<?php
		if ( has_nav_menu( 'footer-menu' ) ) {
			wp_nav_menu( array( 'container' => false, 'theme_location' => 'footer-menu', 'menu_class' => 'footer-menu' ) ); 
		}
		?>
		
		<?php
			/* Footer Social Icons */
			if(function_exists('sitka_get_social_icons')) {
				sitka_get_social_icons('footer');
			}
		?>
		
	</div>

</footer>

<?php wp_footer(); ?>

</body>
</html>