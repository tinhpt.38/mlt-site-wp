<form role="search" method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="search-wrapper">
		<input type="text" placeholder="<?php esc_attr_e('Search the site...', 'sitka'); ?>" name="s" class="s" />
		<button class="search-button"><i class="fa fa-search search-icon"></i></button>
	</div>
</form>