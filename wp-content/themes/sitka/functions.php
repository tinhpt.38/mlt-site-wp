<?php
//////////////////////////////////////////////////////////////////
// Content width
//////////////////////////////////////////////////////////////////
if ( ! isset( $content_width ) ) { $content_width = 1140; }

//////////////////////////////////////////////////////////////////
// Theme set up
//////////////////////////////////////////////////////////////////
add_action( 'after_setup_theme', 'sitka_theme_setup' );

if ( !function_exists('sitka_theme_setup') ) {

	function sitka_theme_setup() {
	
		// Register navigation menu
		register_nav_menus(
			array(
				'main-menu' => esc_html__('Primary Menu', 'sitka'),
				'footer-menu' => esc_html__('Footer Menu', 'sitka'),
			)
		);
		
		// Localization support
		load_theme_textdomain('sitka', get_template_directory() . '/lang');
		
		// Post formats
		add_theme_support( 'post-formats', array( 'gallery', 'video' ) );
		
		// Post Thumbnails
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'sitka-fullwidth', 1140, 0, true );
		add_image_size( 'sitka-fullscreen', 1920, 0, true );
		add_image_size( 'sitka-widget-small-thumb', 80, 80, true );
		add_image_size( 'sitka-widget-big-thumb', 300, 200, true );
		add_image_size( 'sitka-grid-post-thumb', 600, 460, true );
		
		// Title tag
		add_theme_support( 'title-tag' );
		
		// Feed Links
		add_theme_support( 'automatic-feed-links' );
		
		// WooCommerce Support
		add_theme_support( 'woocommerce', array(
			'thumbnail_image_width' => 600,
			'single_image_width' => 600,
		) );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
		
		// Gutenberg
		add_theme_support( 'align-wide' );
		add_theme_support( 'editor-styles' );
		add_editor_style( 'editor-style.css' );
		if ( !class_exists( 'Kirki' ) ) {
		add_editor_style( sitka_fonts_url() );
		}
		
		add_theme_support( 'responsive-embeds' );
	
	}
}

//////////////////////////////////////////////////////////////////
// Register & enqueue styles/scripts
//////////////////////////////////////////////////////////////////
//Get Google Fonts
if ( !class_exists( 'Kirki' ) ) {
function sitka_fonts_url() {
	$font_url = '';

	if ( 'off' !== _x( 'on', 'Google font: on or off', 'sitka' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Prata:400&subset=latin|Lora:400,400i,700,700i&subset=latin-ext|Poppins:300,400,500,600,700&subset=latin-ext' ), "https://fonts.googleapis.com/css" );
	}
	return $font_url;
}
}
// Enqueue Scripts
if ( !function_exists('sitka_load_scripts') ) {
function sitka_load_scripts() {

	// Register scripts and styles
	wp_enqueue_style('sitka-style', get_stylesheet_directory_uri() . '/style.css', array(), '1.2.2');
	wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css');
	
	// Javascript
	wp_enqueue_script('sidr', get_template_directory_uri() . '/js/jquery.sidr.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('slick', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array( 'jquery' ), '', true);
	wp_enqueue_script('fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), '', true);
	wp_enqueue_script('sitka-scripts', get_template_directory_uri() . '/js/sitka-scripts.js', array( 'jquery' ), '1.2.2', true);
	
	// Fonts
	if ( !class_exists( 'Kirki' ) ) {
	wp_enqueue_style( 'sitka_fonts', sitka_fonts_url(), array(), '1.0.0' );
	}
	
	if (is_singular() && get_option('thread_comments'))	wp_enqueue_script('comment-reply');
}
}
add_action( 'wp_enqueue_scripts','sitka_load_scripts' );

//////////////////////////////////////////////////////////////////
// Inclues
//////////////////////////////////////////////////////////////////

// TGM
include( get_template_directory() . '/sitka-tgm.php');

// Customizer
require get_template_directory() . '/admin/customizer/sitka_customizer_setup.php';

// Includes
include( get_template_directory() . '/inc/functions-header.php');
include( get_template_directory() . '/inc/functions-post.php');
include( get_template_directory() . '/inc/functions-featured.php');
include( get_template_directory() . '/inc/functions-page.php');
include( get_template_directory() . '/inc/sitka_css.php');
include( get_template_directory() . '/admin/sitka_color_palette.php');
include( get_template_directory() . '/admin/welcome.php');

//////////////////////////////////////////////////////////////////
// Advanced Custom Fields
//////////////////////////////////////////////////////////////////
// Include ACF fields
include( get_template_directory() . '/admin/sitka_acf_fields.php');

// Hide ACF from WordPress menu
if(!get_theme_mod('sitka_misc_acf', false)) {
add_filter('acf/settings/show_admin', '__return_false');
}

add_filter('acf/settings/show_updates', '__return_false');

// Add custom ACF CSS to edit pages
if ( !function_exists('sitka_custom_acf_css') ) {
function sitka_custom_acf_css() {
    wp_enqueue_style( 'sitka-acf-css', get_template_directory_uri() . '/admin/sitka-acf-style.css', false, '1.0.0' );
}
}
add_action( 'admin_enqueue_scripts', 'sitka_custom_acf_css' );

//////////////////////////////////////////////////////////////////
// Register widgets
//////////////////////////////////////////////////////////////////
add_action( 'widgets_init', 'sitka_widgets_init' );
if ( !function_exists('sitka_widgets_init') ) {
function sitka_widgets_init() {
	register_sidebar(array(
		'name' => esc_html__('Sidebar', 'sitka'),
		'id' => 'sidebar-1',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'description' => esc_html__('Widgets here will appear vertically to the right of your content.', 'sitka'),
	));
	register_sidebar(array(
		'name' => esc_html__('Widget Under Featured Area', 'sitka'),
		'id' => 'sidebar-3',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'description' => esc_html__('Widgets here will appear under Featured Area if activated.', 'sitka'),
	));
	if ( class_exists( 'WooCommerce' ) ) {
	register_sidebar(array(
		'name' => esc_html__('WooCommerce Sidebar', 'sitka'),
		'id' => 'sidebar-5',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'description' => esc_html__('Widgets here will appear on your WooCommerce shop and product pages.', 'sitka'),
	));
	}
}
}

//////////////////////////////////////////////////////////////////
// One Click Demo Import
//////////////////////////////////////////////////////////////////
if ( !function_exists('sitka_ocdi_import_files') ) {
function sitka_ocdi_import_files() {
  return array(
  
	array(
      'import_file_name'             => esc_html__('Main (Full content)', 'sitka'),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'demos/main/sitka-main-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demos/main/sitka-main-widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/main/sitka-main-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/main-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import the full demo content which includes posts, pages, widgets, and menus. This is not recommended if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/main/',
    ),
	array(
      'import_file_name'             => esc_html__('Main (Settings only)', 'sitka'),
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/main/sitka-main-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/main-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import only the demo Customizer settings. This is ideal if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/main/',
    ),
	array(
      'import_file_name'             => esc_html__('Classic (Full content)', 'sitka'),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'demos/classic/sitka-classic-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demos/classic/sitka-classic-widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/classic/sitka-classic-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/classic-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import the full demo content which includes posts, pages, widgets, and menus. This is not recommended if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/classic/',
    ),
	array(
      'import_file_name'             => esc_html__('Classic (Settings only)', 'sitka'),
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/classic/sitka-classic-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/classic-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import only the demo Customizer settings. This is ideal if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/classic/',
    ),
	array(
      'import_file_name'             => esc_html__('Minimal (Full content)', 'sitka'),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'demos/minimal/sitka-minimal-content.xml',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/minimal/sitka-minimal-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/minimal-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import the full demo content which includes posts, pages, widgets, and menus. This is not recommended if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/minimal/',
    ),
	array(
      'import_file_name'             => esc_html__('Minimal (Settings only)', 'sitka'),
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/minimal/sitka-minimal-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/minimal-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import only the demo Customizer settings. This is ideal if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/minimal/',
    ),
	array(
      'import_file_name'             => esc_html__('Travel (Full content)', 'sitka'),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'demos/travel/sitka-travel-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demos/travel/sitka-travel-widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/travel/sitka-travel-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/travel-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import the full demo content which includes posts, pages, widgets, and menus. This is not recommended if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/travel/',
    ),
	array(
      'import_file_name'             => esc_html__('Travel (Settings only)', 'sitka'),
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/travel/sitka-travel-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/travel-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import only the demo Customizer settings. This is ideal if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/travel/',
    ),
	array(
      'import_file_name'             => esc_html__('Food (Full content)', 'sitka'),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'demos/food/sitka-food-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demos/food/sitka-food-widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/food/sitka-food-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/food-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import the full demo content which includes posts, pages, widgets, and menus. This is not recommended if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/food/',
    ),
	array(
      'import_file_name'             => esc_html__('Food (Settings only)', 'sitka'),
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/food/sitka-food-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/food-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import only the demo Customizer settings. This is ideal if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/food/',
    ),
	array(
      'import_file_name'             => esc_html__('Lifestyle (Full content)', 'sitka'),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'demos/lifestyle/sitka-lifestyle-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demos/lifestyle/sitka-lifestyle-widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/lifestyle/sitka-lifestyle-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/lifestyle-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import the full demo content which includes posts, pages, widgets, and menus. This is not recommended if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/lifestyle/',
    ),
	array(
      'import_file_name'             => esc_html__('Lifestyle (Settings only)', 'sitka'),
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/lifestyle/sitka-lifestyle-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/lifestyle-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import only the demo Customizer settings. This is ideal if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/lifestyle/',
    ),
	array(
      'import_file_name'             => esc_html__('Lifestyle 2 (Full content)', 'sitka'),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'demos/lifestyle2/sitka-lifestyle2-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demos/lifestyle2/sitka-lifestyle2-widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/lifestyle2/sitka-lifestyle2-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/lifestyle2-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import the full demo content which includes posts, pages, widgets, and menus. This is not recommended if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/lifestyle2/',
    ),
	array(
      'import_file_name'             => esc_html__('Lifestyle 2 (Settings only)', 'sitka'),
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/lifestyle2/sitka-lifestyle2-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/lifestyle2-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import only the demo Customizer settings. This is ideal if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/lifestyle2/',
    ),
	array(
      'import_file_name'             => esc_html__('Interior Design (Full content)', 'sitka'),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'demos/interior/sitka-interior-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demos/interior/sitka-interior-widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/interior/sitka-interior-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/interior-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import the full demo content which includes posts, pages, widgets, and menus. This is not recommended if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/interior/',
    ),
	array(
      'import_file_name'             => esc_html__('Interior Design (Settings only)', 'sitka'),
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demos/interior/sitka-interior-customizer.dat',
    
      'import_preview_image_url'     => 'https://sitkatheme.com/demo-thumb/interior-demo-large.jpg',
      'import_notice'                => esc_html__( 'Please note you are about to import only the demo Customizer settings. This is ideal if you have existing content on your site.', 'sitka' ),
      'preview_url'                  => 'https://sitkatheme.com/interior/',
    ),
  );
}
}
add_filter( 'pt-ocdi/import_files', 'sitka_ocdi_import_files' );

// Disable plugin branding
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

if ( !function_exists('sitka_demo_confirmation_dialog_options') ) {
function sitka_demo_confirmation_dialog_options ( $options ) {
    return array_merge( $options, array(
        'width'       => 400,
    ) );
}
}
add_filter( 'pt-ocdi/confirmation_dialog_options', 'sitka_demo_confirmation_dialog_options', 10, 1 );

// Assign menu
if ( !function_exists('sitka_ocdi_after_import') ) {
function sitka_ocdi_after_import( $selected_import ) {
	$main_menu = get_term_by( 'name', 'main-demo main', 'nav_menu' );
	$footer_menu = get_term_by( 'name', 'main-demo footer', 'nav_menu' );
	set_theme_mod( 'nav_menu_locations', array(
			'main-menu' => $main_menu->term_id,
			'footer-menu' => $footer_menu->term_id,
		)
	);
}
}
add_action( 'pt-ocdi/after_import', 'sitka_ocdi_after_import' );

if ( !function_exists('sitka_demo_import_intro_text') ) {
function sitka_demo_import_intro_text( $default_text ) { ?>
	<div class="demo-wrap">
	<h4><?php esc_html_e( 'Each demo offers two import options', 'sitka' ); ?>:</h4>
	<ul>
	<li><strong><?php esc_html_e( 'Full Content Import', 'sitka' ); ?></strong>: <?php echo wp_kses( __( 'Imports ALL CONTENT from the demo such as posts, pages, widgets, etc. as well as Customizer settings. (Recommended if you <strong>have no</strong> existing content).', 'sitka' ), array('strong' => array('class' => array())) ); ?></li>
	<li><strong><?php esc_html_e( 'Settings Only Import', 'sitka' ); ?></strong>: <?php echo wp_kses( __( 'Imports only the demo\'s Customizer settings which includes layouts, colors, widgets, etc. (Recommended if you <strong>have</strong> existing content)..', 'sitka' ), array('strong' => array('class' => array())) ); ?></li>
	</ul>
	
	<div class="note-item"><span class="demo-note important"><?php esc_html_e( 'Important', 'sitka' ); ?>:</span> <?php esc_html_e( 'Make sure to install all required plugins via Appearance > Install Plugins, before you start importing a demo.', 'sitka' ); ?></div>
	
	<hr>
	<div class="note-item"><span class="demo-note"><?php esc_html_e( 'Note', 'sitka' ); ?>:</span> <?php esc_html_e( 'It can take several minutes to complete the demo import.', 'sitka' ); ?></div>
	<div class="note-item"><span class="demo-note"><?php esc_html_e( 'Note', 'sitka' ); ?>:</span> <?php echo wp_kses( __( 'If you import one demo and wish to switch to another demo, you will first need to reset your Customizer settings prior to importing the second demo. Install the plugin <a href="https://wordpress.org/plugins/customizer-reset-by-wpzoom/" target="_blank">Customizer Reset</a>. Then, go to Appearance > Customize > Reset.', 'sitka' ), array('a' => array('target' => array(),'href' => array())) ); ?></div>
	<div class="demo-more"><?php esc_html_e( 'For more info check out our', 'sitka' ); ?> <a href="http://sitkatheme.com/documentation/#installation_demo" target="_blank"><?php esc_html_e( 'documentation', 'sitka' ); ?></a>.</div>
	</div>
    <?php
}
}
add_filter( 'pt-ocdi/plugin_intro_text', 'sitka_demo_import_intro_text' );

//////////////////////////////////////////////////////////////////
// COMMENTS LAYOUT
//////////////////////////////////////////////////////////////////
if ( !function_exists('sitka_comments') ) {
function sitka_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
		
		<div class="thecomment">
			
			<div class="comment-header">
				<div class="author-img">
					<?php echo get_avatar($comment,$args['avatar_size']); ?>
				</div>
				<div class="comment-author">
					<h6 class="author"><?php echo get_comment_author_link(); ?></h6>
					<span class="date"><?php printf(esc_html__('%1$s at %2$s', 'sitka'), get_comment_date(),  get_comment_time()) ?></span>
				</div>
			</div>
			
			<div class="comment-text">
				<?php if ($comment->comment_approved == '0') : ?>
					<em><i class="icon-info-sign"></i> <?php esc_html_e('Comment awaiting approval', 'sitka'); ?></em>
					<br />
				<?php endif; ?>
				<?php comment_text(); ?>
				<span class="reply">
					<i class="fa fa-reply"></i> <?php comment_reply_link(array_merge( $args, array('reply_text' => esc_html__('Reply', 'sitka'), 'depth' => $depth, 'max_depth' => $args['max_depth'])), $comment->comment_ID); ?>
					<?php edit_comment_link(esc_html__('Edit', 'sitka')); ?>
				</span>
			</div>
					
		</div>
		
	</li>

	<?php 
}
}

//////////////////////////////////////////////////////////////////
// Exclude Featured Posts from Post feed
//////////////////////////////////////////////////////////////////
// Create new query var for page blog template
if ( !function_exists('sitka_query_vars') ) {
function sitka_query_vars( $query_vars ){
    $query_vars[] = 'sitka_page_blog';
    return $query_vars;
}
}
add_filter( 'query_vars', 'sitka_query_vars' );

if(get_theme_mod('sitka_home_feat_enable', false) == true && get_theme_mod('sitka_home_feat_exclude', false) == true) {
	
	function sitka_get_feat_ids() {
	
		// Determine featured posts
		$feat_content_type = get_theme_mod('sitka_home_feat_content_type', 'latest');
		$feat_amount = get_theme_mod('sitka_home_feat_amount', '3');
		
		if($feat_content_type == 'latest') {
			$exclude_args = array(
				'posts_per_page' => $feat_amount,
			);
		} elseif($feat_content_type == 'category') {
			$exclude_args = array(
				'posts_per_page' => $feat_amount,
				'cat' => get_theme_mod('sitka_home_feat_content_category'),
			);
		} elseif($feat_content_type == 'tag') {
			$exclude_args = array(
				'posts_per_page' => $feat_amount,
				'tag__in' => get_theme_mod('sitka_home_feat_content_tag'),
			);
		} elseif($feat_content_type == 'specific') {
			
			$featured_ids = explode(',', get_theme_mod('sitka_home_feat_content_posts'));
		
			$exclude_args = array(
				'posts_per_page' => $feat_amount,
				'post_type' => array('post', 'page'),
				'post__in' => $featured_ids,
			);
		}
		// Get the post ID's and put them in a array
		$getpost = get_posts($exclude_args);
		foreach($getpost as $post) {
			$exclude_posts[] = $post->ID;
		}
		
		return $exclude_posts;
		
	}
	
	//Modify main query to exclude posts
	function sitka_exclude_feat_posts($query) {
		if ( $query->is_home() && $query->is_main_query() ) {
			$query->query_vars['post__not_in'] = sitka_get_feat_ids();
		}		
	}
	add_action( 'pre_get_posts', 'sitka_exclude_feat_posts' );
	
}

//////////////////////////////////////////////////////////////////
// Set different post count on 1st page
//////////////////////////////////////////////////////////////////
if(get_theme_mod('sitka_home_post_number_toggle', false) == true) {

	function sitka_post_feed_offset( $query ) {
		$ppp = get_option( 'posts_per_page' );
		$first_page_ppp = get_theme_mod('sitka_home_post_number', 6);
		$paged = $query->query_vars[ 'paged' ];

		if( $query->is_home() && $query->is_main_query() ) {
			if( !is_paged() ) {

				$query->set( 'posts_per_page', $first_page_ppp );

			} else {
			
				$paged_offset = $first_page_ppp + ( ($paged - 2) * $ppp );
				$query->set( 'offset', $paged_offset );

			}
		}
	}
	add_action( 'pre_get_posts', 'sitka_post_feed_offset' );
	
	function sitka_offset_pagination( $found_posts, $query ) {
		$ppp = get_option( 'posts_per_page' );
		$first_page_ppp = get_theme_mod('sitka_home_post_number', 6);

		if( $query->is_home() && $query->is_main_query() ) {
			if( !is_paged() ) {

				return( $first_page_ppp + ( $found_posts - $first_page_ppp ) * $first_page_ppp / $ppp );

			} else {

				return( $found_posts - ($first_page_ppp - $ppp) );
			}
		}
		return $found_posts;
	}
	add_filter( 'found_posts', 'sitka_offset_pagination', 10, 2 ); 
	
}

//////////////////////////////////////////////////////////////////
// Pagination
//////////////////////////////////////////////////////////////////
if ( !function_exists('sitka_pagination') ) {
function sitka_pagination() {
	
	global $wp_query;
	
	$big = 999999999;
	echo '<div class="sitka-pagination">';
	echo paginate_links( array(
		'prev_text'          => esc_html__('Prev', 'sitka'),
		'next_text'          => esc_html__('Next', 'sitka'),
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages
	) );
	echo '</div>';
}
}

//////////////////////////////////////////////////////////////////
// ADD SPANS AROUND COUNT TEXT IN CATEGORY WIDGETS
//////////////////////////////////////////////////////////////////
if ( !function_exists('sitka_cat_count_span') ) {
function sitka_cat_count_span( $links ) {
	$links = str_replace( '</a> (', '</a><span class="sp-post-count">', $links );
	$links = str_replace( ')', '</span>', $links );
	return $links;
}
}
add_filter( 'wp_list_categories', 'sitka_cat_count_span' );

if ( !function_exists('sitka_archive_count_span') ) {
function sitka_archive_count_span( $links ) {
	$links = str_replace( '</a>&nbsp;(', '</a><span class="sp-post-count">', $links );
	$links = str_replace( ')', '</span>', $links );
	return $links;
}
}
add_filter( 'get_archives_link', 'sitka_archive_count_span' );

//////////////////////////////////////////////////////////////////
// REMOVE DEFAULT WORDPRESS GALLERY CSS
//////////////////////////////////////////////////////////////////
add_filter( 'use_default_gallery_style', '__return_false' );

//////////////////////////////////////////////////////////////////
// HEX to RGBA
//////////////////////////////////////////////////////////////////
if ( !function_exists('sitka_hex2rgba') ) {
function sitka_hex2rgba($color, $opacity = false) {
 
	$default = 'rgb(0,0,0)';
 
	if(empty($color))
          return $default; 
 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
		
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        $rgb =  array_map('hexdec', $hex);
 
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).',0)';
        }
 
        return $output;
}
}

//////////////////////////////////////////////////////////////////
// WooCommerce functions
//////////////////////////////////////////////////////////////////
if ( class_exists( 'WooCommerce' ) ) {
	
	add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
		return array(
			'width' => 280,
			'height' => 280,
			'crop' => 0,
		);
	} );
	
}