<?php 

/*
* 404 Error page
*/

get_header(); 
?>
	
	<div class="sp-container">
	
		<div id="sp-content">
			
			<div id="sp-main" class="isFullwidth">
				
				<div id="header-404">
					
					<div class="title-404"><?php esc_html_e( 'Page not found', 'sitka' ); ?></div>
					<h1><?php esc_html_e( '404', 'sitka' ); ?></h1>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Back to home', 'sitka' ); ?></a>
					
					
					
				</div>
				
			</div>
	

<?php get_footer(); ?>