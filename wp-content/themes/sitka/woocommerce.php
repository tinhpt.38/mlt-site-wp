<?php get_header(); ?>
	
	<div class="sp-container">
	
		<div id="sp-content">
			
			<div id="sp-main" class="<?php echo esc_attr(get_theme_mod('sitka_woo_layout', 'isFullwidth')); ?>">
				
				<?php woocommerce_content(); ?>
			
			</div>

<?php if(get_theme_mod('sitka_woo_layout') == 'isSidebar') : ?><?php get_sidebar('woocommerce'); ?><?php endif; ?>
<?php get_footer(); ?>