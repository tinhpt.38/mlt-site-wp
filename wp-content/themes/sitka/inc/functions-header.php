<?php
/*
* Get logo
*/
if ( !function_exists('sitka_get_logo') ) {
function sitka_get_logo() {

	$logo_type = get_theme_mod('sitka_header_logo_type', 'logo_image');
	$logo = get_theme_mod('sitka_header_logo_image', esc_url(get_template_directory_uri()) . '/img/logo.png');
	
	if($logo_type == 'logo_text') : ?>
		<?php if(is_home()) : ?>
		<h1><a class="text-logo normal-text" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html(get_theme_mod('sitka_header_logo_text_name', esc_html__('Sitka', 'sitka'))); ?></a></h1>
		<?php else : ?>
		<a class="text-logo normal-text" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html(get_theme_mod('sitka_header_logo_text_name', esc_html__('Sitka', 'sitka'))); ?></a>
		<?php endif; ?>
	<?php else : ?>
	
		<?php if(!$logo) : ?>
			<a class="normal-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/img/logo.png" alt="<?php esc_attr(bloginfo( 'name' )); ?>" /></a>
		<?php else : ?>
			<a class="normal-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo( 'name' )); ?>" /></a>
			<?php if(is_home()) : ?><h1 class="home-h1"><?php esc_attr(bloginfo( 'name' )); ?></h1><?php endif; ?>
		<?php endif; ?>
		
	<?php endif;
	
}
}

/*
* Get white logo
*/
if ( !function_exists('sitka_get_logo_white') ) {
function sitka_get_logo_white() {

	$logo_type = get_theme_mod('sitka_header_logo_type', 'logo_image');
	$logo = get_theme_mod('sitka_header_logo_image_white', esc_url(get_template_directory_uri()) . '/img/logo-white.png');
	
	if($logo_type == 'logo_text') : ?>
		
	<?php else : ?>
		
		<?php if(!$logo) : ?>
			<a class="white-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/img/logo-white.png" alt="<?php esc_attr(bloginfo( 'name' )); ?>" /></a>
		<?php else : ?>
			<a class="white-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr(bloginfo( 'name' )); ?>" /></a>
		<?php endif; ?>
		
	<?php endif;
	
}
}

/*
* Check if white logo is needed
*/
if ( !function_exists('sitka_need_white_logo') ) {
function sitka_need_white_logo() {
	
	if (!class_exists('ACF')) {
		if(get_theme_mod('sitka_home_feat_fullscreen_overlay', false) == true && get_theme_mod('sitka_home_feat_layout', 'none') == 'fullscreen' && get_theme_mod('sitka_home_feat_enable', false) == true && is_home()) {
			return 'header-white';
		}
	} else {
		if(get_theme_mod('sitka_home_feat_fullscreen_overlay', false) == true && get_theme_mod('sitka_home_feat_layout', 'none') == 'fullscreen' && is_home() && !is_paged() && get_theme_mod('sitka_home_feat_enable', false) == true || 
		   get_field('sitka_acf_page_feat_fullscreen_overlay') == true && get_field('sitka_acf_page_feat_layout') == 'fullscreen' && is_page() && !is_paged() && sitka_get_page_layout() == 'layout1' && get_field('sitka_acf_page_featured_enable') || 
		   get_theme_mod('sitka_home_feat_fullscreen_overlay', false) == true && get_theme_mod('sitka_home_feat_layout', 'none') == 'split-fullscreen' && is_home() && !is_paged() && get_theme_mod('sitka_home_feat_enable', false) == true ||
		   get_field('sitka_acf_page_feat_fullscreen_overlay') == true && get_field('sitka_acf_page_feat_layout') == 'split-fullscreen' && is_page() && !is_paged() && sitka_get_page_layout() == 'layout1' && get_field('sitka_acf_page_featured_enable') ||
		   get_theme_mod('sitka_home_feat_fullscreen_overlay', false) == true && get_theme_mod('sitka_home_feat_layout', 'none') == 'static-fullscreen' && is_home() && !is_paged() && get_theme_mod('sitka_home_feat_enable', false) == true ||
		   get_field('sitka_acf_page_feat_fullscreen_overlay') == true && get_field('sitka_acf_page_feat_layout') == 'static-fullscreen' && is_page() && !is_paged() && sitka_get_page_layout() == 'layout1' && get_field('sitka_acf_page_featured_enable')) {
			return 'header-white';
		}
	}
	
}
}

/*
* Get Burger-Menu
*/
if ( !function_exists('sitka_get_burger_menu') ) {
function sitka_get_burger_menu() { ?>
	
	<span class="menu-icon">
		<button class="menu-toggle"></button>
		<?php if(get_theme_mod('sitka_header_show_menu_label', true)) : ?><span><?php echo esc_html(get_theme_mod('sitka_header_menu_label', esc_html__('Menu', 'sitka'))); ?></span><?php endif; ?>
	</span>
	
	<div id="mobile-navigation">
		<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'main-menu', 'menu_class' => 'menu' ) ); ?>
	</div>
	
	<div id="sitka-mobile-header">
		
		<a href="#" id="close-mobile-menu"></a>
		
		<?php 
		if(get_theme_mod('sitka_mobile_menu_show_logo', true)) {
			sitka_get_logo();
		}
		?>
		
	</div>
	
	<div id="sitka-mobile-footer">
		<?php
		if(get_theme_mod('sitka_mobile_menu_show_social', true)) {
			if(function_exists('sitka_get_social_icons')) { sitka_get_social_icons('header'); } 
		}
		
		if(get_theme_mod('sitka_mobile_menu_show_search', true)) {
			sitka_get_search(); 
		}
		
		sitka_get_woo_cart();
		?>
	</div>
	
<?php }
}

/*
* Get Mobile Header
*/
if ( !function_exists('sitka_get_mobile_header') ) {
function sitka_get_mobile_header() { ?>
	
	<div id="mobile-header-wrapper">
		<header id="mobile-menu" class="mobile-header">
			<?php
			
				sitka_get_burger_menu();
				sitka_get_logo();
				
			?>
			
			<div class="top-misc">
				<?php sitka_get_search(); ?>
			</div>
			
		</header>
	</div>
	
<?php }
}

/*
* Get header search
*/
if ( !function_exists('sitka_get_search') ) {
function sitka_get_search() { ?>
	<?php if(get_theme_mod('sitka_header_show_search', true)) : ?>
	<div class="header-search-wrap <?php if(!class_exists('WooCommerce')) : ?>no-cart<?php endif; ?>">
		<a href="#search" class="toggle-search-box">
			<i class="fa fa-search"></i>
		</a>
	</div>
	<?php endif; ?>
<?php }
}

/*
* Get header search overlay
*/
if ( !function_exists('sitka_search_overlay') ) {
function sitka_search_overlay(){
?>
<div id="sitka-search-overlay">
	<button type="button" class="close">&times;</button>
	<form role="search" class="form-search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" >
		<input spellcheck="false" autocomplete="off" type="text" value="" name="s" placeholder="<?php esc_attr_e('Search the site...', 'sitka'); ?>" />
	</form>
</div>
<?php
}
}
if(is_admin()) { } else { add_action('wp_footer','sitka_search_overlay'); }

/*
* Get Woocommerce shop cart
*/
if ( !function_exists('sitka_get_woo_cart') ) {
function sitka_get_woo_cart() {
if ( class_exists( 'WooCommerce' ) ) {
if(get_theme_mod('sitka_woo_cart', true)) { ?>
<div id="sp-shopping-cart">
<?php $count = WC()->cart->cart_contents_count; ?>
	<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'sitka' ); ?>"><?php if ( $count >= 0 ) echo '<span class="sp-count">' . esc_html($count) . '</span>'; ?></a>
</div>
<?php
}
}
}
}

/*
* Check featured area
*/
if ( !function_exists('sitka_header_check_featured') ) {
function sitka_header_check_featured() {
	
	if(is_home() && get_theme_mod('sitka_home_feat_enable', false)) {
		$featured_area = 'has-slider';
	} elseif(class_exists('ACF') && is_page() && get_field('sitka_acf_page_featured_enable')) {
		$featured_area = 'has-slider';
	} else {
		$featured_area = 'no-slider';
	}
	
	return $featured_area;
	
}
}

/*
* Add body class
*/
add_filter( 'body_class', 'sitka_header_class_body' );
if ( !function_exists('sitka_header_class_body') ) {
function sitka_header_class_body( $classes ) {

	$classes[] = 'is-header-' . get_theme_mod('sitka_header_layout', 'layout1');
	
	return $classes;
 
}
}