<?php
if ( !function_exists('sitka_head_css') ) {
function sitka_head_css() {
    ?>
    <style type="text/css">
		
		<?php if(sitka_need_white_logo() == 'header-white') : ?>
			.sticky-wrapper:not(.sticky) #header { background:none; box-shadow:none; }
			@media screen and (min-width:1200px) {
			#inner-wrapper .feat-area.fullscreen .feat-item { height:100vh; }
			}
			<?php if(get_theme_mod('sitka_header_layout', 'layout1') == 'layout1') : ?>
				.feat-area.fullscreen { margin-top:-<?php echo get_theme_mod('sitka_header_layout1_height', '90'); ?>px; }
			<?php elseif(get_theme_mod('sitka_header_layout', 'layout1') == 'layout2') : ?>
				.feat-area.fullscreen { margin-top:-<?php echo get_theme_mod('sitka_header_layout2_height', '110'); ?>px; }
			<?php endif; ?>
		<?php endif; ?>
		
		<?php if(get_theme_mod('sitka_home_feat_fullscreen_overlay', false) == true && get_theme_mod('sitka_home_feat_layout', 'classic') == 'fullscreen' && is_home()) : ?>
			<?php if(get_theme_mod('sitka_header_layout', 'layout1') == 'layout1') : ?>
				.feat-link { top:<?php echo get_theme_mod('sitka_header_layout1_height', '90'); ?>px; }
			<?php elseif(get_theme_mod('sitka_header_layout', 'layout1') == 'layout2') : ?>
				.feat-link { top:<?php echo get_theme_mod('sitka_header_layout2_height', '110'); ?>px; }
			<?php endif; ?>
		<?php endif; ?>
		
		<?php if(is_page() && class_exists('ACF') && get_field('sitka_acf_page_promo_enable')) : ?>
			.promo-wrap.promo-grid { grid-auto-rows:<?php echo get_field('sitka_acf_page_promo_height'); ?>px; }
			.promo-wrap.promo-mixed { grid-auto-rows:<?php echo get_field('sitka_acf_page_promo_height'); ?>px <?php echo get_field('sitka_acf_page_promo_height'); ?>px; }
			<?php if(get_field('sitka_acf_page_promo_border') == false) : ?>.promo-overlay {border:none;}<?php endif; ?>
			
		<?php elseif(is_home()) : ?>
			<?php if(get_theme_mod('sitka_home_promo_border', true) == false) : ?>.promo-overlay {border:none;}<?php endif; ?>
		<?php endif; ?>
		
		<?php if(is_page() && class_exists('ACF')) : ?>
			<?php if(get_field('sitka_acf_page_featured_enable')) : ?>
			<?php
				$bg_trans = get_field('sitka_acf_page_feat_overlay_trans');
				$bg_trans = isset($bg_trans) ? $bg_trans : 0.2;
			
				$bg_color = get_field('sitka_acf_page_feat_overlay_color');
			?>
			#inner-wrapper .feat-area:not(.sitka-carousel) .feat-shadow { background:<?php echo sitka_hex2rgba($bg_color, $bg_trans); ?>;}
			<?php endif; ?>
		<?php endif; ?>
		
		<?php if(is_page() && class_exists('ACF')) : ?>
			<?php if(get_field('sitka_acf_page_featured_enable')) : ?>
				<?php if(get_field('sitka_acf_page_feat_layout') == 'static-classic') : ?>
					.feat-area.static-slider.classic-slider .post-header { max-width:<?php echo get_field('sitka_acf_page_feat_static_classic_width'); ?>%; }
					.feat-area.static-slider.classic-slider .static-text { max-width:<?php echo get_field('sitka_acf_page_feat_static_classic_text_width'); ?>%; }
				<?php elseif(get_field('sitka_acf_page_feat_layout') == 'static-fullscreen') : ?>
					.feat-area.static-slider.fullscreen .post-header { max-width:<?php echo get_field('sitka_acf_page_feat_static_fullscreen_width'); ?>%; }
					.feat-area.static-slider.fullscreen .static-text { max-width:<?php echo get_field('sitka_acf_page_feat_static_fullscreen_text_width'); ?>%; }
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
		
		<?php if(get_theme_mod('sitka_newsletter_image')) : ?>
		.news-img { background-image:url(<?php echo get_theme_mod('sitka_newsletter_image'); ?>); height:<?php echo get_theme_mod('sitka_newsletter_image_height', 150); ?>px; }
		<?php endif; ?>
		
    </style>
    <?php
}
}
add_action( 'wp_head', 'sitka_head_css', 999999 );

?>