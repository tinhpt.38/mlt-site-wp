<?php
/**
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme Sitka for publication on ThemeForest
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

require_once get_template_directory() . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'sitka_register_required_plugins' );

function sitka_register_required_plugins() {

	$plugins = array(
		array(
			'name'     				=> 'Sitka Core',
			'slug'     				=> 'sitka-core',
			'source'   				=> get_stylesheet_directory() . '/plugins/sitka-core.zip',
			'required' 				=> true,
			'version' 				=> '1.2.1',
		),
		array(
			'name'      => 'Kirki',
			'slug'      => 'kirki',
			'required'  => true,
		),
		array(
			'name'     				=> 'Advanced Custom Fields PRO',
			'slug'     				=> 'advanced-custom-fields-pro',
			'source'   				=> get_stylesheet_directory() . '/plugins/advanced-custom-fields-pro.zip',
			'required' 				=> true,
			'version' 				=> '5.8.9',
		),
		array(
			'name'     				=> 'Sitka Blocks',
			'slug'     				=> 'sitka-blocks',
			'source'   				=> get_stylesheet_directory() . '/plugins/sitka-blocks.zip',
			'required' 				=> false,
			'version' 				=> '1.2.2',
		),
		array(
			'name'     				=> 'Envato Market',
			'slug'     				=> 'envato-market',
			'source'   				=> get_stylesheet_directory() . '/plugins/envato-market.zip',
			'required' 				=> false,
			'version' 				=> '2.0.3',
		),
		array(
			'name'      => 'One Click Demo Import',
			'slug'      => 'one-click-demo-import',
			'required'  => false,
		),
		array(
			'name'      => 'Smash Balloon Instagram Feed',
			'slug'      => 'instagram-feed',
			'required'  => false,
		),
		array(
			'name'      => 'Mailchimp for WordPress',
			'slug'      => 'mailchimp-for-wp',
			'required'  => false,
		),
		array(
			'name'      => 'Contact Form 7',
			'slug'      => 'contact-form-7',
			'required'  => false,
		),

	);

	$config = array(
		'id'           => 'sitka',
		'default_path' => '',
		'menu'         => 'tgmpa-install-plugins',
		'parent_slug'  => 'themes.php',
		'capability'   => 'edit_theme_options',
		'has_notices'  => true,
		'dismissable'  => true,
		'dismiss_msg'  => '',
		'is_automatic' => true,
		'message'      => '',
	);

	tgmpa( $plugins, $config );
}
